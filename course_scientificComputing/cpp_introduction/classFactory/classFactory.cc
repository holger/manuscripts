#include <iostream>     // std::cout
#include <cstdlib>      // for exit()

#include "classFactory.h"

ParticleTypeFactory particleTypeFactory;

bool ParticleTypeFactory::registerParticleType(std::string particleTypeID, CreateParticleTypeCallback createFunction)
{
  std::cout << "ParticleTypeFactory::registerParticleType: registering particleTypeID = " << particleTypeID << std::endl;
  return callbackMap_.insert(CallbackMap::value_type(particleTypeID, createFunction)).second;
}

ParticleBase* ParticleTypeFactory::createParticleType(std::string particleTypeID)
{
  CallbackMap::const_iterator it = callbackMap_.find(particleTypeID);

  if(it == callbackMap_.end()) {
    std::cerr << "unknown particleTypeID = " << particleTypeID << std::endl;
    exit(1);
  }
  
  return (it->second)();
}
