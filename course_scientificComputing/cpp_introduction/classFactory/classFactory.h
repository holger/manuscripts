#ifndef classFactory_h
#define classFactory_h

#include <map>
#include <string>

#include "particleBase.h"

class ParticleTypeFactory
{
public:
  typedef ParticleBase* (*CreateParticleTypeCallback)();

 private:
  typedef std::map<std::string, CreateParticleTypeCallback> CallbackMap;
  
 public:
  // stores a string a the corresponding particle creation function in the map
  bool registerParticleType(std::string particleTypeID, CreateParticleTypeCallback createFunction); // or
  // bool registerParticleType(std::string particleTypeID, ParticleBase* (*createFunction)());

  // function to create a particle
  ParticleBase* createParticleType(std::string ParticleTypeID);
  
private:

  // the heart of the particle creation mecanism: A map storing a pair
  // of a call-back function for the creation of a particle and a
  // string.
  CallbackMap callbackMap_;
};

#endif
