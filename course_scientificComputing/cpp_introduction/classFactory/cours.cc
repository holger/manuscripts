// g++ -std=gnu++11 -o cours cours.cc classFactory.cc particleTypeFactoryModule.cc

#include <iostream>
#include <list>

#include "particleTypes.h"
#include "classFactory.h"

extern ParticleTypeFactory particleTypeFactory;

// The goal is to create objects dynamically from strings. You would
// like to write something like ParticleBase* particle = new
// "ParticleInertial"; And the string "ParticleInertial" you might get
// from outside (parameter file).

// Here is how it works:
int main(int argc, char** argv)
{
  std::cout << "ParticleFactory starts ...\n";

  ParticleBase* particle1 = particleTypeFactory.createParticleType("ParticleInertial");
  ParticleBase* particle2 = particleTypeFactory.createParticleType("ParticleIon");
  
  std::list<ParticleBase*> particleList;
  particleList.push_back(particle1);
  particleList.push_back(particle2);
  
  for(ParticleBase* pb : particleList)
    std::cout << "here is " << pb->name() << std::endl;
}
