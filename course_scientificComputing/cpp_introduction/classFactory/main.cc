#include <iostream>
#include <list>

#include "particleTypes.h"
#include "classFactory.h"

extern ParticleTypeFactory particleTypeFactory;

int main(int argc, char** argv)
{
  std::cout << "\nparticleFactory starts ...\n";

  ParticleBase* particle1 = particleTypeFactory.createParticleType("ParticleInertial");
  ParticleBase* particle2 = particleTypeFactory.createParticleType("ParticleIon");
  
  std::list<ParticleBase*> particleList;
  particleList.push_back(particle1);
  particleList.push_back(particle2);
  
  for(ParticleBase* pb : particleList)
    std::cout << "here is " << pb->name() << std::endl;
}
