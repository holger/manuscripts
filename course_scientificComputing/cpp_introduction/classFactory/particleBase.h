#ifndef particleBase_h
#define particleBase_h

class ParticleBase
{
public:
  virtual std::string name() = 0;
};

#endif
