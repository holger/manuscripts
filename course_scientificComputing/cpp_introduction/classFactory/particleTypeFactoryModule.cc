#include "classFactory.h"
#include "particleBase.h"
#include "particleTypes.h"

extern ParticleTypeFactory particleTypeFactory;

// defining call back function for the creation of a inertial particle
ParticleBase* createParticleInertial() { return new ParticleInertial; }
// global variable leading to execution of the registration of the inertial particle at compile time
const bool registeredParticleInertial = particleTypeFactory.registerParticleType("ParticleInertial", createParticleInertial);

// --------------------------------

// defining call back function for the creation of a charged particle
ParticleBase* createParticleIon() { return new ParticleIon; }
// global variable leading to execution of the registration of the charged particle at compile time
const bool registeredParticleIon = particleTypeFactory.registerParticleType("ParticleIon", createParticleIon);
