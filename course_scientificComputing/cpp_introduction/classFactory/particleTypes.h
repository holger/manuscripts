#ifndef particleTypes_h
#define particleTypes_h

#include <iostream>

#include "particleBase.h"

// classes defining two different particles types

class ParticleInertial : public ParticleBase
{
public:
  double mass;

  ParticleInertial() { std::cout << "ParticleInertial::ParticleInertial()\n";}

  std::string name() { return "ParticleInertial";}
};

class ParticleIon : public ParticleBase
{
public:
  double mass;

  double charge;

  ParticleIon() { std::cout << "ParticleIon::ParticleIon()\n";}

  std::string name() { return "ParticleIon";}
};

#endif
