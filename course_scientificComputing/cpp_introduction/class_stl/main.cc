#include <iostream>
#include <vector>
#include <list>

// Brief introduction to C++ classes and the STL (Standard template
// library)

// C++ is an object oriented programming language.  An object contains
// members (data) and member functions (functions). The object is a
// realisation of a class at runtime. Let us see this for a 1D particle:

class Particle 
{
public:

  // members:
  double x;   // position
  double v;   // velocity
  double F;   // force
  double qc;  // charge

  // Constructors:
  // default constructor (Particle particle;)
  Particle() {
    std::cerr << "Particle::Particle()\n";
    x = 0;
    v = 1.0;
    F = 0;
    qc = 0;
  }
  
  // constructor with arguments (Particle particle(1,1))
  Particle(double qq, double xx) {
    std::cerr << "Particle::Particle(double qq, double xx)\n";
    x = xx;
    v = 1.0;
    F = 0;
    qc = qq;
  }

  void move_x(double dt) {
    std::cerr << "Particle::move_x(double dt)\n";
    x += dt*v;
  }
  
  void move_v(double dt) {
    std::cerr << "Particle::move_v(double dt)\n";
    v += dt*F;
  }
};

// Let us see this at work

int main(int argc, char** argv)
{
  // create particle:
  Particle particle;
  
  // set a force
  particle.F = 1;
  
  particle.move_x(0.1);
  particle.move_v(0.1);

  std::cerr << "Particle : " << particle.x << "  " << particle.v << std::endl;

  // how can we store n particles?
  int n = 10;
  // 1) Simple pointer

  Particle* particlePointer = new Particle[n];
  
  for(int i = 0; i < n; i++) {
    particlePointer[i].F = 1;
    particlePointer[i].move_x(0.1);
    particlePointer[i].move_v(0.1);
  }

  // 2) STL container 

  // STL is a library (collection) of data types and algorithms. There
  // are especially lists and vectors:

  // create stl vector of particles
  std::vector<Particle> particleVector(n);
  
  // you can use it in the same way as the simple pointer above
  for(int i = 0; i < n; i++) {
    particlePointer[i].F = 1;
    particlePointer[i].move_x(0.1);
    particlePointer[i].move_v(0.1);
  }
  // but also by iterators
  for(std::vector<Particle>::iterator it = particleVector.begin(); it != particleVector.end(); it++) {
    it->F = 1;
    it->move_x(0.1);
    it->move_v(0.1);
  }
  // or if you like
  for(std::vector<Particle>::iterator it = particleVector.begin(); it != particleVector.end(); it++) {
    Particle& p = *it; // Reference to particle
    p.F = 1;
    p.move_x(0.1);
    p.move_v(0.1);
  }

  // create double-linked list of Particles
  std::list<Particle> particleList;
  
  // access only by iterators
  for(std::list<Particle>::iterator it = particleList.begin(); it != particleList.end(); it++) {
    Particle& p = *it; // Reference to particle
    p.F = 1;
    p.move_x(0.1);
    p.move_v(0.1);
  }

  // Think about adding and removing particles!
}
