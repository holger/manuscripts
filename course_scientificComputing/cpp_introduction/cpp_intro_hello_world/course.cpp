#include <iostream>

// Brief and practical introduction to object oriented programming in
// C++

// C++ is a general-purpose programming language.
// It has

// imperative      = statements (e.g. assignment: float a = 5; goto 10;) functions or procedures
// object-oriented = objects contain data and methods manipulating the data
// generic         = abstractions to avoid code duplication (e.g. templates -> types to be specified later)
// low-level       = assembler (e.g. direct manipulation of registers and memory)

// programming features.

// Examples:

// imperative
int leaves;
int roots;

leaves = -1;

struct Tree // (C)
{
  int leaves;
  int roots;
};

Tree tree;
tree.leaves = -1;

// object-oriented (c++)
class Tree
{
public:

  void grow_leaves() {
    ++leaves_;
  }
  void grow_roots() {
    ++roots_;
  }

  int get_leaves() const {
    return leaves_;
  }

  int& get_leaves() {
    return leaves_;
  }
  
private:
  int leaves_;
  int roots_;
};

Tree tree;
tree.grow_leaves();
tree.get_leaves() = 5;

void writeTreeOnScreen(Tree const& tree)
{
  std::cout << "tree.leaves = " << tree.get_leaves() << std::endl;
  std::cout << "adresse de tree = " << &tree << std::endl;
}

writeTreeOnScreen(tree);

// Invented in 1979 by Bjarne Stroustrup at Bell Labs.

// Some other features:
// Static type system = correct use of types is checked at compile time (less bugs)
// portable           = code runs without modifications on many different architectures
// fast               = performance comparable to assembler
// rich               = many different programming styles are possible -> complicated

// the first c++ programm:

int main()
{
  std::cout << "hello world" << std::endl;
}

// compile this with g++ -o hello_world course.cpp
