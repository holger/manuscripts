#include <iostream>

#include "factorial.hpp"

int main()
{
  std::cout << "4! = " << Factorial<4>::value << std::endl;
  std::cout << "4! = " << Factorial2<4>::value << std::endl;
}
