#ifndef factorial_hpp
#define factorial_hpp

// template computing the factorial at compile time by recursion
template <int N>
struct Factorial 
{
    enum { value = N * Factorial<N - 1>::value };
};

// complete specialisation that terminates the recursion process
template <>
struct Factorial<0> 
{
    enum { value = 1 };
};

template <int N>
struct Factorial2
{
  static const int value = N * Factorial<N - 1>::value;
};

template <>
struct Factorial2<0> 
{
  static const int value = 1;
};


#endif
