#include "forest.hpp"

int main()
{
  Forest forest(10);

  for(int y = 0; y < 100; y++)
    forest.yearCycle();
  
  const std::vector<Tree>& trees = forest.get_trees();

  for(int t=0; t<trees.size(); t++)
    std::cout << trees[t].get_roots() << "\t" << trees[t].get_leaves() << std::endl;
  
}
