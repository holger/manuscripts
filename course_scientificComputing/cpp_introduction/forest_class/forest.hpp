#ifndef forest_hpp
#define forest_hpp

#include <vector>
#include "../tree_class/tree.hpp"

class Forest
{
public:
  Forest(int treeNumber) {
    trees_.resize(treeNumber);
  }
  
  void yearCycle()
  {
    for(int t=0;t<trees_.size();t++)
      trees_[t].grow();
  }

  std::vector<Tree> const& get_trees() {
    return trees_;
  }

private:
  
  std::vector<Tree> trees_;
};

#endif
