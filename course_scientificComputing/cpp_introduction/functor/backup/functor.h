#ifndef functor_own_h
#define functor_own_h

#include <iostream>
#include <string>

// Functor-Klasse fuer keinen Parameter

template<class R>
class Functor0
{
 public:
/*   virtual void operator()()=0;  // call using operator */
/*   virtual void Call()=0;        // call using function */

  virtual R operator()()=0;  // call using operator
  virtual R Call()=0;        // call using function
};

// derived template class
template <class TClass, class R> 
class SpecificFunctor0 : public Functor0<R>
{
private:
  R (TClass::*fpt)();   // pointer to member function
  TClass* pt2Object;                  // pointer to object
  
public:
  
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  SpecificFunctor0(TClass* _pt2Object, R(TClass::*_fpt)())
  { pt2Object = _pt2Object;  fpt=_fpt; };
  
  // override operator "()"
  virtual R operator()()
  { (*pt2Object.*fpt)();};              // execute member function
  
  // override function "Call"
  virtual R Call()
  { (*pt2Object.*fpt)();};             // execute member function
};

// --------------------------------------------------------------------------------

// Functor-Klasse für einen Parameter

template<class R, class PAR1>
class Functor1
{
 public:
/*   virtual void operator()()=0;  // call using operator */
/*   virtual void Call()=0;        // call using function */

  virtual R operator()(PAR1)=0;  // call using operator
  virtual R Call(PAR1)=0;        // call using function
};

// derived template class
template <class TClass, class R, class PAR1> 
class SpecificFunctor1 : public Functor1<R, PAR1>
{
private:
  R (TClass::*fpt)(PAR1);   // pointer to member function
  TClass* pt2Object;                  // pointer to object
  
public:
  
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  SpecificFunctor1(TClass* _pt2Object, R(TClass::*_fpt)(PAR1))
  { pt2Object = _pt2Object;  fpt=_fpt; };
  
  // override operator "()"
  virtual R operator()(PAR1 p1)
  { (*pt2Object.*fpt)(p1);};              // execute member function
  
  // override function "Call"
  virtual R Call(PAR1 p1)
  { (*pt2Object.*fpt)(p1);};             // execute member function
};

// --------------------------------------------------------------------------------

// Functor-Klasse fuer zwei Parameter

template<class R, class PAR1, class PAR2>
class Functor2
{
 public:
/*   virtual void operator()()=0;  // call using operator */
/*   virtual void Call()=0;        // call using function */

  virtual R operator()(PAR1, PAR2)=0;  // call using operator
  virtual R Call(PAR1, PAR2)=0;        // call using function
};

// derived template class
template <class TClass, class R, class PAR1, class PAR2> 
class SpecificFunctor2 : public Functor2<R, PAR1, PAR2>
{
private:
  R (TClass::*fpt)(PAR1, PAR2);   // pointer to member function
  TClass* pt2Object;                  // pointer to object
  
public:
  
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  SpecificFunctor2(TClass* _pt2Object, R(TClass::*_fpt)(PAR1, PAR2))
  { pt2Object = _pt2Object;  fpt=_fpt; };
  
  // override operator "()"
  virtual R operator()(PAR1 p1, PAR2 p2)
  { (*pt2Object.*fpt)(p1,p2);};              // execute member function
  
  // override function "Call"
  virtual R Call(PAR1 p1, PAR2 p2)
  { (*pt2Object.*fpt)(p1,p2);};             // execute member function
};


// --------------------------------------------------------------------------------

// Functor-Klasse fuer drei Parameter

template<class R, class PAR1, class PAR2, class PAR3>
class Functor3
{
 public:
/*   virtual void operator()()=0;  // call using operator */
/*   virtual void Call()=0;        // call using function */

  virtual R operator()(PAR1 ,PAR2 , PAR3)=0;  // call using operator
  virtual R Call(PAR1 ,PAR2, PAR3)=0;        // call using function
};

// derived template class
template <class TClass, class R, class PAR1, class PAR2, class PAR3> 
class SpecificFunctor3 : public Functor3<R, PAR1, PAR2, PAR3>
{
private:
  R (TClass::*fpt)(PAR1 ,PAR2, PAR3);   // pointer to member function
  TClass* pt2Object;                  // pointer to object
  
public:
  
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  SpecificFunctor3(TClass* _pt2Object, R(TClass::*_fpt)(PAR1, PAR2, PAR3))
  { pt2Object = _pt2Object;  fpt=_fpt; };
  
  // override operator "()"
  virtual R operator()(PAR1 p1, PAR2 p2, PAR3 p3)
  { (*pt2Object.*fpt)(p1,p2,p3);};              // execute member function
  
  // override function "Call"
  virtual R Call(PAR1 p1, PAR2 p2, PAR3 p3)
  { (*pt2Object.*fpt)(p1,p2,p3);};             // execute member function
};


// --------------------------------------------------------------------------------

class Test 
{
public:
  Test() : ausgabeF2(this, &Test::ausgabe2), ausgabeF3(this, &Test::ausgabe3) {}
  void ausgabe2(double& p1, double& p2){std::cout << "Test::ausgabe2: \np1 = " << p1 << "\np2 = " << p2 << std::endl;}
  void ausgabe3(double& p1, double& p2, double& p3)
    {std::cout << "Test::ausgabe3: \np1 = " << p1 << "\np2 = " << p2 << "\np3 = " << p3 << std::endl;}
  SpecificFunctor2<Test,void, double&, double&> ausgabeF2;
  SpecificFunctor3<Test,void, double&, double&, double&> ausgabeF3;
}; 


#endif
