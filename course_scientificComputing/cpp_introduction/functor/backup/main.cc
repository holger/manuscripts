#include "functor.h"

#include <iostream>
#include <string>
#include <list>

class Particle1
{
public:
  Particle1() {};
  std::string name() {std::cerr << "Particle1::name()\n"; std::string tmp = "particle1";}
};

class Particle3
{
public:
  Particle3() {};
  std::string name(int a) {std::cerr << "Particle3::name()\n"; std::string tmp = "particle3";}
};


class Particle2
{
public:
  std::string name() {return "particle2";}
};


int main(int argc, char** argv)
{
  std::cerr << "functor test starts ...\n";

  Particle1 particle1;
  Particle2 particle2;

  Functor0<std::string>* functor1;
  functor1 = new SpecificFunctor0<Particle1,std::string>(&particle1, &Particle1::name);

  Functor0<std::string>* functor2;
  functor2 = new SpecificFunctor0<Particle2,std::string>(&particle2, &Particle2::name);
  
  SpecificFunctor0<Particle1,std::string> functor3(&particle1, &Particle1::name);
  
  //std::list<Functor0<std::string>* > functorList;
  // functorList.push_back(functor1);
  // functorList.push_back(functor2);

  std::string name = functor3.Call();

  // std::cerr << name << std::endl;
  
  // std::cerr << "functor test ends ...\n";
}
