//g++ -o cours cours.cc

#include <iostream>
#include <string>
#include <list>
#include <functional>
#include <cstdlib>

#include "functorExamples.h"

// Call-back strategies

// The problem: Imagine that you have several classes of particles

class Particle1
{
public:
  Particle1() {name = "Particle1";};
  std::string get_name(void) { return name;}
  
  std::string name;
};

class Particle2
{
public:
  Particle2() {name = "Particle2";};
  std::string get_name(void) {return name;}
  
  std::string name;
  
  double charge;
};


// and in your code you have many objects of such particle classes.
// Now you want to store the function get_name() of each object in a
// list (or vector) to easily execute it:

int main(int argc, char** argv)
{
  Particle1 p1,p2;
  Particle2 p3;

  // Putting all of them directly into one list is not possible
  // because they have different types (Particle1 and Particle2):
  // std::list<???> particleList;

  // One possibility are C++ Functors: Objects that behave as
  // functions.  
  // Let us look into functorExamples.h and not to be
  // shocked we recall pointer to functions:

  // c-pointer to function standardFunction:
  std::string (*fPointer)() = standardFunction;
  std::cerr << (*fPointer)() << std::endl; // or just
  std::cerr << fPointer() << std::endl; 
  
  // now a pointer to a member function (Particle1::get_name):
  std::string (Particle1::*pointer)() = &Particle1::get_name; 

  // this is first a general object independent pointer. Now we point
  // it on our particle1:
  std::cerr << "here is " << (p1.*pointer)() << std::endl;
							      
  // This helps to understand the functor Particle1Functor:
 
  Particle1Functor particle1Functor(&p1,&Particle1::get_name);
  std::cout << "here is " << particle1Functor() << std::endl;

  // A functor is just the same as a member-function pointer? No! It
  // is an object: Can store data, can be copied, ...

  // What about a functor for class Particle2?
  StringFunctor<Particle2> stringFunctor3(&p3,&Particle2::get_name);
  StringFunctor<Particle1> stringFunctor1(&p1,&Particle1::get_name);
  StringFunctor<Particle1> stringFunctor2(&p2,&Particle1::get_name);
  std::cout << "here is " << stringFunctor1() << std::endl;
  std::cout << "here is " << stringFunctor2() << std::endl;
  std::cout << "here is " << stringFunctor3() << std::endl;

  // But again: How to put them now into one list?
  SpecificStringFunctor0<Particle1> functor1(&p1,&Particle1::get_name);
  SpecificStringFunctor0<Particle1> functor2(&p1,&Particle1::get_name);
  SpecificStringFunctor0<Particle2> functor3(&p3,&Particle2::get_name);

  // here comes the list:
  
  std::list<StringFunctorBase0*> particleGetNameList;
  particleGetNameList.push_back(&functor1);
  particleGetNameList.push_back(&functor2);
  particleGetNameList.push_back(&functor3);
  
  // let us see the result

  std::cerr << "We have all these types of particles\n";
  for(std::list<StringFunctorBase0*>::const_iterator it = particleGetNameList.begin(); it != particleGetNameList.end(); it++) {
    std::cout << (**it)() << std::endl;
  }

  // C++11: std::functions: object-oriented function wrapper
  std::function<std::string(Particle1&)> functor11 = &Particle1::get_name;

  std::cout << functor11(p1) << std::endl;

  // Done? Nearly: This was all for member-function without arguments.
  // Checkout functor.h for a Functors of 0,1,2,3 arguments.

  // Exercise: Understand the implementation of Functor1 and SpecificFunctor1.
  // write a code that uses Functor1

} 
