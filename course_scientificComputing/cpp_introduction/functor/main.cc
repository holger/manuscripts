#include "functor.h"

#include <iostream>
#include <string>
#include <list>

class Particle1
{
public:
  Particle1() {name = "Particle1";};
  std::string& get_name() { return name;}
  
  std::string name;
};

class Particle2
{
public:
  Particle2() {name = "Particle2";};
  std::string& get_name() {return name;}
  
  std::string name;
  
  double charge;
};

std::string standardFunction()
{
  return "standardFunction";
}

int main(int argc, char** argv)
{
  std::cerr << "functor test starts ...\n";

  Particle1 particle1;
  Particle2 particle2;

  Functor0<std::string&>* functor1;
  functor1 = new SpecificFunctor0<Particle1,std::string&>(&particle1, &Particle1::get_name);

  Functor0<std::string&>* functor2;
  functor2 = new SpecificFunctor0<Particle2,std::string&>(&particle2, &Particle2::get_name);
  
  SpecificFunctor0<Particle1,std::string&> functor3(&particle1, &Particle1::get_name);
  
  std::list<Functor0<std::string&>* > functorList;
  functorList.push_back(functor1);
  functorList.push_back(functor2);

  for(std::list<Functor0<std::string&>* >::const_iterator it = functorList.begin(); it != functorList.end(); it++) {
    Functor0<std::string&>* functorP = *it;
    std::cerr << (*functorP)() << std::endl;

    std::cerr << (**it)() << std::endl;
  }
  
  std::string& (Particle1::*pointer)() = &Particle1::get_name;
  std::cerr << (particle1.*pointer)() << std::endl;

  std::string (*fPointer)() = standardFunction;

  std::cerr << fPointer() << std::endl;
  std::cerr << (*fPointer)() << std::endl;

  

  std::cerr << "functor test ends ...\n";
}
