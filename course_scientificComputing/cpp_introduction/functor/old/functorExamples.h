#ifndef functorExamples_h
#define functorExamples_h

// Simple Functor for the Particle1 class case:

class Particle1; // forward declaration of Particle1 class

// Functor for the get_name function of the Particle1 class:
class Particle1Functor
{
private:
  std::string& (Particle1::*fpt)();      // pointer to member function
  Particle1* pt2Object;                  // pointer to object
  
public:
  
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  Particle1Functor(Particle1* _pt2Object, std::string&(Particle1::*_fpt)())
  { pt2Object = _pt2Object;  fpt=_fpt; };
  
  // override operator "()"
  std::string& operator()()
  { (*pt2Object.*fpt)();};              // execute member function
  
  // override function "Call"
  std::string& Call()
  { (*pt2Object.*fpt)();};              // execute member function
};

// standard function -----------------------------------------------

std::string standardFunction()
{
  return "standard function";
}

// TEMPLATED Functor: -----------------------------------------------

// Functor for std::string& returning member-functions without
// parameters:

template <class TClass> 
class StringFunctor
{
private:
  std::string& (TClass::*fpt)();   // pointer to member function
  TClass* pt2Object;                  // pointer to object
  
public:
  
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  StringFunctor(TClass* _pt2Object, std::string&(TClass::*_fpt)())
  { pt2Object = _pt2Object;  fpt=_fpt; };
  
  // override operator "()"
  std::string& operator()()
  { (*pt2Object.*fpt)();};              // execute member function
  
  // override function "Call"
  std::string& Call()
  { (*pt2Object.*fpt)();};             // execute member function
};


// TEMPLATED BASE Functor: -----------------------------------------------

// Functor-Class for zero parameters

class StringFunctor0
{
 public:
  virtual std::string& operator()()=0;  // call using operator
  virtual std::string& Call()=0;        // call using function
};

// derived template class
template <class TClass> 
class SpecificStringFunctor0 : public StringFunctor0
{
private:
  std::string& (TClass::*fpt)();   // pointer to member function
  TClass* pt2Object;                  // pointer to object
  
public:
  
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  SpecificStringFunctor0(TClass* _pt2Object, std::string&(TClass::*_fpt)())
  { pt2Object = _pt2Object;  fpt=_fpt; };
  
  // override operator "()"
  virtual std::string& operator()()
  { (*pt2Object.*fpt)();};              // execute member function
  
  // override function "Call"
  virtual std::string& Call()
  { (*pt2Object.*fpt)();};             // execute member function
};


#endif
