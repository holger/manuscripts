#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>

#include "/home/holger/include/BasisKlassen/toString.h"
#include "toString.hpp"

int main(int argc, char** argv)
{
  std::cout << "writing data for gnuplot starts...\n";

  if(argc < 2) {
    std::cerr << "ERROR: argc = " << argc << " < 2\n";
    std::cerr << "usage: course <n>\n";
    exit(1);
  }
  
  int nPoints = atoi(argv[1]);

  std::ofstream out("testPoints_2D.txt");
  
  for(int i=0; i <nPoints; i++) {
    out << double(rand())/RAND_MAX << "\t" << double(rand())/RAND_MAX << std::endl;
  }

  out.close();

  out.open("testPoints_3D.txt");

  for(int i=0; i <nPoints; i++) {
    out << double(rand())/RAND_MAX << "\t" << double(rand())/RAND_MAX << "\t" << double(rand())/RAND_MAX << std::endl;
  }

  out.close();

  std::string filename;
  std::stringstream sstream;
  sstream << "testPoints_2D_" << nPoints << ".txt";
  filename = sstream.str();
  out.open(filename.c_str());
  for(int i=0; i <nPoints; i++) {
    out << double(rand())/RAND_MAX << "\t" << double(rand())/RAND_MAX << std::endl;
  }

  out.close();

  //  filename = BK::toString("testPointss_2D_",nPoints);
  filename = toString("testPointss_2D_",nPoints);
  std::cout << "filename = " << filename << std::endl;
  
}
