#ifndef toString_hpp
#define toString_hpp

template<typename T>
std::string typeToString(T number)
{
  std::stringstream sstream;
  sstream << number;
  return sstream.str();  
}

template<typename First>
void addToStream(std::ostringstream& sstream, First first)
{
  sstream << first;
}

template<typename First, typename ... Rest>
void addToStream(std::ostringstream& sstream, First first, Rest ... rest)
{
  sstream << first;
  addToStream(sstream, rest...);
}

template<typename ... Types>
std::string toString(Types ... types)
{
  std::ostringstream sstream;
  addToStream(sstream, types...);
  return sstream.str();
}


#endif
