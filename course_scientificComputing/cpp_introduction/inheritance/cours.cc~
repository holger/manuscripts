#include "inheritance.h"

int main(int argc, char** argv)
{
  std::cerr << "Inheritance test starts\n";
  
  // create base class object
  Base base;

  // create derived class object
  Derived derived;

  base.number = 10;
  derived.counter = 10;
  
  // Now let us access these object by a pointer:

  Base* baseP = &base;
  baseP->number = 20;
  
  // we are allowed to use a base class pointer to a derived class
  // object. We put the pointer on the derived object
  baseP = &derived;
  
  // We cannot access members of the derived class with a base pointer
  // baseP->counter = 11; // ERROR!  But we can cast the base pointer
  // to a derived pointer. Caution! It is not checked whether baseP
  // points to a complete type (Derived): with baseP = &base; the
  // following lines do compile but are dangerous code!
  static_cast<Derived*>(baseP)->counter = 11;
  std::cout << static_cast<Derived*>(baseP)->counter << std::endl;

  // and if we print the names we get:
  baseP = &base;
  std::cerr << baseP->name() << std::endl;
  baseP = &derived;
  std::cerr << baseP->name() << std::endl;

  // we always call the base class funtion name. Again we would have
  // to make a cast!
}
