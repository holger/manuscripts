#ifndef inheritance_h
#define inheritance_h

#include <iostream>

class Base
{
public:
  Base() { std::cerr << "Base::Base()\n"; }
  
  int number;
  
  std::string name() { return "I am the Base class"; }
};

class Derived : public Base
{
public:
  Derived() { std::cerr << "Derived::Derived()\n"; }
  
  double counter;
  
  std::string name() { return "I am the Derived class"; }
};

#endif
