// g++ -O3 -o cours cours.cc

#include <iostream>
#include <list>
#include <vector>

#include "vector.h"

// Here we are going to test the performance of different containers
// to remove elements. The context is that the particles from the PIC
// simulation that leave the numerical domain are removed from the
// container. We are going to compare the performance of a stl list,
// stl vector and custom designed structure (in vector.h)

int main(int argc, char** argv)
{
  std::cerr << "starting the deletion test ...\n";
  
  // number of elements
  int nParticles = 1e6;
  // number of elements to remove
  int eraseNumber = nParticles/100;

  {
    // First, let us try the stl list
    std::list<double> list;
    for(int i=0;i<nParticles;i++)
      list.push_back(i);

    // starting timer
    double startTime = clock();
    // passing through the list and erasing particles ...
    std::list<double>::iterator it = list.begin();
    int counter = 0;
    while(it != list.end()) {
      it = list.erase(it);
      advance(it,nParticles/eraseNumber-1);
      ++counter;
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl list : " << elapsedTime << "  " << counter << std::endl;
  }

  {  
    // Now the stl vector. Its performance is worse than that of a stl
    // list because when removing an element, all other following
    // elements are shifted (copied). A stl vector is continuous in memory!!!
    std::vector<double> array(nParticles);

    double startTime = clock();
    std::vector<double>::iterator it = array.begin();
    int counter = 0;
    for(int i=0;i<nParticles;i+=nParticles/eraseNumber) {
      array.erase(it+i-counter);
      ++counter;
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl vector : " << elapsedTime << "  " << counter << std::endl;
  }

  {  
    // Here comes our own vector
    Vector array(nParticles);

    double startTime = clock();
    int counter = 0;
    for(int i=0;i<nParticles;i+=nParticles/eraseNumber) {
      array.erase(i-counter);
      ++counter;
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "own vector : " << elapsedTime << "  " << counter << std::endl;
  }
}


// starting the deletion test ...
// stl list : 0.003981  1000
// stl vector : 0.424553  1000
// own vector : 0.000897  1000
