// g++ -o main main.cc

#include <iostream>
#include <list>
#include <vector>

class Vector
{
public:
  Vector(int n) { n_ = n; size_ = n; data_ = new double[n];};
  
  ~Vector() { delete [] data_;}

  double operator()(int i) const {return data_[i];}
  double& operator()(int i) {return data_[i];}
  
  int size() {return size_;}
  
  void erase(int i) {data_[i] = data_[size_-1]; --size_; }
  
private:
  double* data_;
  int n_;
  int size_;
};

int main(int argc, char** argv)
{
  std::cerr << "starting the deletion test ...\n";

  int nParticles = 1e6;
  int eraseNumber = nParticles/1000;

  {  
    std::list<double> list;
    for(int i=0;i<nParticles;i++)
      list.push_back(i);

    double startTime = clock();
    std::list<double>::iterator it = list.begin();
    int counter = 0;
    while(it != list.end()) {
      it = list.erase(it);
      advance(it,nParticles/eraseNumber-1);
      ++counter;
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl list : " << elapsedTime << "  " << counter << std::endl;
  }

  {  
    std::vector<double> array(nParticles);

    double startTime = clock();
    std::vector<double>::iterator it = array.begin();
    int counter = 0;
    for(int i=0;i<nParticles;i+=nParticles/eraseNumber) {
      array.erase(it+i-counter);
      ++counter;
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl vector : " << elapsedTime << "  " << counter << std::endl;
  }

  {  
    Vector array(nParticles);

    double startTime = clock();
    int counter = 0;
    for(int i=0;i<nParticles;i+=nParticles/eraseNumber) {
      array.erase(i-counter);
      ++counter;
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "own vector : " << elapsedTime << "  " << counter << std::endl;
  }

  

}
