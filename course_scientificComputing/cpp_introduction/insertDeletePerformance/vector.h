#ifndef vector_h
#define vector_h

// A test class for storing and erasing particles
class Vector
{
public:
  Vector(int n) { n_ = n; size_ = n; data_ = new double[n];};
  
  ~Vector() { delete [] data_;}

  double operator()(int i) const {return data_[i];}
  double& operator()(int i) {return data_[i];}
  
  int size() {return size_;}
  
  // as the order of the particles is not important, we can remove an
  // element by copying the last element of the container to the
  // position of the removed element. This reduces the effectiv size
  // of the array by one.
  void erase(int i) {data_[i] = data_[size_-1]; --size_; }
  
private:
  // pointer holding the data
  double* data_;
  // allocated size of vector
  int n_;
  // effectively used size of vector
  int size_;
};

#endif
