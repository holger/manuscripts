#include <iostream>
#include <cmath>
#include <vector>
#include <cstring>

int main(int argc, char** argv)
{
  std::cout << "jacobi 1d test with stl vector starts ...\n";
  
  // number of grid points
  int mx = 1000;

  // creation of stl vectors
  std::vector<double> phiNew(mx);
  std::vector<double> phi(mx);
  
  // initial condition
  for(int i=0;i<mx;i++) 
    phi[i] = 0;
  
  // boundary condition
  phi[0] = 0;
  phi[mx-1] = 1;
  phiNew[0] = 0;
  phiNew[mx-1] = 1;
  
  double difference = 1;
  int counter = 0;
  while(difference > 1e-12) {

    // one Jacobi iteration
    for(int i=1;i<mx-1;i++) 
      phiNew[i] = 0.5*(phi[i+1] + phi[i-1]);

    // computation of the absolut value of phi-phiNew
    difference = 0;
    for(int i=0;i<mx;i++) 
      if(fabs(phi[i] - phiNew[i]) > difference)
	difference = fabs(phi[i] - phiNew[i]);
    
    // phi = phiNew
    std::memcpy(&phi[0],&phiNew[0],mx*sizeof(double));

    counter++;
  }
  
  // output
  for(int i=0;i<mx;i++)
    std::cerr << i << "\t" << phi[i] << std::endl;

  std::cout << "counter = " << counter << std::endl;
}
