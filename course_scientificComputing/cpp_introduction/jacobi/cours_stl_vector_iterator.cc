#include <iostream>
#include <cmath>
#include <vector>

#include <algorithm>

int main(int argc, char** argv)
{
  std::cout << "jacobi 1d test with iterator on stl vector starts ...\n";

  // number of grid points
  int mx = 1000;

    // creation of stl vectors
  std::vector<double> phiNew(mx);
  std::vector<double> phi(phiNew.size());
  
  // boundary conditions
  phiNew.front() = 0;
  phiNew.back() = 1;
  phi.front() = 0;
  phi.back() = 1;
  
  double difference = 1;
  while(difference > 1e-12) {
    
    // one Jacobi iteration
    std::vector<double>::iterator itNew = phiNew.begin() + 1;
    std::vector<double>::iterator itLeft = phi.begin();
    std::vector<double>::iterator itRight = phi.begin() + 2;
    
    while(itNew != phiNew.end() - 1)
      *(itNew++) = 0.5*(*(itLeft++) + *(itRight++));

    // computation of the absolut value of phi-phiNew    
    difference = 0;

    std::vector<double> phiDiff(phiNew.size());
    
    std::vector<double>::iterator itDiff = phiDiff.begin();
    std::vector<double>::iterator itPhiNew = phiNew.begin();
    std::vector<double>::iterator itPhi = phi.begin();
    
    while(itDiff != phiDiff.end())
      *(itDiff++) = fabs(*(itPhi++) - *(itPhiNew++));
    
    difference = *std::max_element(phiDiff.begin(),phiDiff.end());
    
    // phi = phiNew
    std::copy(phiNew.begin(),phiNew.end(),phi.begin());
  }
  
  // output
  for(int i=0;i<mx;i++)
    std::cerr << i << "\t" << phi[i] << std::endl;
}
