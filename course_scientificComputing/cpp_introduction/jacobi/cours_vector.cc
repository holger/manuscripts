#include <iostream>
#include <cmath>
#include <cstring>
#include "../vector/vector.h"
#include "poisson.h"

int main(int argc, char** argv)
{
  std::cout << "jacobi 1d test with own vector class starts ...\n";
  
  // number of grid points
  int mx = 1000;

  // creation of vectors
  Vector<double> phi(mx);
  Vector<double> rho(mx);
  
  // initial conditions and right hand side
  phi = 0.;
  rho = 0.;
  
  // number of iterations
  int counter;
  counter = poisson(phi,rho,0,1);
  
  // output
  for(int i=0;i<mx;i++)
    std::cerr << i << "\t" << phi[i] << std::endl;

  std::cout << "counter = " << counter << std::endl;
}
