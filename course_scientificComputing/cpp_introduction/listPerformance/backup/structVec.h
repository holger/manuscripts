#ifndef structVec_h
#define structVec_h

#include <vector>
#include <array>

class StructVec
{
public:
  
  StructVec(int size) {
    pos.resize(size);
    vel.resize(size);

    for(int i=0;i<size;i++) {
      for(int j=0;j<3;j++) {
	pos[i][j] = i;
	vel[i][j] = i;
      }
    }
  }

  std::vector<std::array<float,3> > pos;
  std::vector<std::array<float,3> > vel;

  float x(int p, int c) const { return pos[p][c]; }
  float v(int p, int c) const { return vel[p][c]; }
  float& x(int p, int c) { return pos[p][c]; }
  float& v(int p, int c) { return vel[p][c]; }
};


  
#endif
