juqueen (xlc++ -O3) compute node: n=1e6; loops = 100;

stl list of Particle<3>: 0.59
stl list of Particle<3> with separated loops: 1.74
stl list of Particle<36> : 4.55
vector of Particle<3>: 0.31
vector of Particle<36>: 2.98
structure of stl vectors<3> : 0.24
structure of stl vectors<36> : 0.26
structure of stl vectors lin <36> : 0.25
structure of 1D stl vectors<36> : 0.24
structure of own vector : 2.1
map of stl vector : 0.48

deletion: 1e6 / 100
stl list : 0.01  10000
stl vector : 4.2  10000
own vector : 0  10000

---------------------------------------------

licallo (icc -O3): n=1e6; loops = 100

stl list of Particle<3>: 0.68
stl list of Particle<3> with separated loops: 2.02
stl list of Particle<36> : 3.65
vector of Particle<3>: 0.33
vector of Particle<36>: 1.76
structure of stl vectors<3> : 0.27
structure of stl vectors<36> : 0.27
structure of stl vectors lin <36> : 0.29
structure of 1D stl vectors<36> : 0.28
structure of own vector : 0.28
map of stl vector : 0.46

stl list : 0.01  10000
stl vector : 1.97  10000
own vector : 0  10000

-------------------------------------------------

licallo (g++ -O3): n=1e6; loops = 100

stl list of Particle<3>: 0.67
stl list of Particle<3> with separated loops: 2.02
stl list of Particle<36> : 3.4
vector of Particle<3>: 0.34
vector of Particle<36>: 1.74
structure of stl vectors<3> : 0.47
structure of stl vectors<36> : 0.46
structure of stl vectors lin <36> : 0.43
structure of 1D stl vectors<36> : 0.32
structure of own vector : 0.29
map of stl vector : 0.57

stl list : 0.01  10000
stl vector : 3.48  10000
own vector : 0  10000


-------------------------------------------------

laptop (g++ -O3): n=1e6; loops = 100

stl list of Particle<3>: 1.07165
stl list of Particle<3> with separated loops: 3.2478
stl list of Particle<36> : 4.03192
vector of Particle<3>: 0.519521
vector of Particle<36>: 2.66155
structure of stl vectors<3> : 0.400364
structure of stl vectors<36> : 0.393886
structure of stl vectors lin <36> : 0.369425
structure of 1D stl vectors<36> : 0.400392
structure of own vector : 0.406235
map of stl vector : 0.801756

stl list : 0.003817  10000
stl vector : 4.28876  10000
own vector : 0.001075  10000
