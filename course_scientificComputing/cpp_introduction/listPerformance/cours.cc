//g++ -O3 -DNDEBUG -std=gnu++11 -o cours cours.cc -I/home/holger/include

#include <boost/simd/sdk/simd/pack.hpp>
#include <boost/simd/include/functions/sum.hpp>
#include <boost/simd/include/functions/load.hpp>
#include <boost/simd/include/functions/aligned_load.hpp>
#include <boost/simd/include/functions/plus.hpp>
#include <boost/simd/include/functions/multiplies.hpp>

#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>
#include <cstdlib>
#include <list>
#include <fstream>
#include <vector>

#include "particle.h"
#include "structVec.h"
#include "particleStorage.h"
#include "particleMap.h"

#include "/home/holger/include/BasisKlassen/particleArray.h"
#include "/home/holger/include/BasisKlassen/particleArrayAttributes.h"



// Until now our particle were objects containing its position,
// velocity and possibly other properties.  These particles were
// stored in a stl list. We can suppose that a big part (maybe the
// major part if we have a lot of particles) of the computing time
// will be spent for the particle integration. The question is thus:

// What is the fastest way to store and manipulate the particles?

// Let us try different possibilities and compare their performance.

// The general operation will be to add to the position of a particle
// its velocity: particle.position += particle.velo. Let us do this in
// three dimensions for many particles

int main(int argc, char** argv)
{
  std::cerr << "Computing time measurement for different particle storage types...\n";

  if(argc < 2) {
    std::cerr << "usage: ./cours <particle number>\n";
    exit(1);
  }

  // get the number of particles from the command line
  const int particleNumber = atoi(argv[1]);

  // always perform the same number(nCount) of operations
  int nCount = 1e7;
  int loopNumber = 100;//nCount/particleNumber;

  // Let us start with a list of simple particles (see Particle.h)
  // similar to the ones we used in our PIC simulation

  // ------------------------------------------------------
//   {
//     // 3D!
//     const int particleSize = 3;
//     std::list<Particle<particleSize> > particleList;
//     // adding particles to the list
//     for(int i=0;i<particleNumber;i++) 
//       particleList.push_back(Particle<particleSize>());
    
//     // Now let us do the pos+vel operation and measure the time:
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++)
//       for(std::list<Particle<particleSize> >::iterator it=particleList.begin();it!=particleList.end();it++) {
// 	it->pos[0] += it->vel[0];
// 	it->pos[1] += it->vel[1];
// 	it->pos[2] += it->vel[2];
//       }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "stl list of Particle<3>: " << elapsedTime << std::endl;

//     // We write the result to a file:
//     std::ofstream out("timer_list_particle3.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   }
//   // ------------------------------------------------------ 

//   {
//     // Another computing possibility would be to split the main loop
//     // in three loops over one component each. It gives off-course the
//     // same physical result. BUT, does it take the same computing
//     // time??? Check it out!
    
//     const int particleSize = 3;
//     std::list<Particle<particleSize> > particleList;
//     for(int i=0;i<particleNumber;i++) 
//       particleList.push_back(Particle<particleSize>());
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
//       for(std::list<Particle<particleSize> >::iterator it=particleList.begin();it!=particleList.end();it++) 
// 	it->pos[0] += it->vel[0];
    
//       for(std::list<Particle<particleSize> >::iterator it=particleList.begin();it!=particleList.end();it++)
// 	it->pos[1] += it->vel[1];
      
//       for(std::list<Particle<particleSize> >::iterator it=particleList.begin();it!=particleList.end();it++) 
// 	it->pos[2] += it->vel[2];
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "stl list of Particle<3> with separated loops: " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_list_particle3_sepLoop.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------ 

//   {
//     // Until now our particle our particle had just three spatial and
//     // three velocity components. What if our particle has more
//     // properties such as a charge. Let us check the performance for
//     // big-memory particles of type Particle<36>:
    
//     const int particleSize = 36;
//     std::list<Particle<particleSize> > particleList;
//     for(int i=0;i<particleNumber;i++) 
//       particleList.push_back(Particle<particleSize>());
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) 
//       for(std::list<Particle<particleSize> >::iterator it=particleList.begin();it!=particleList.end();it++) {
// 	it->pos[0] += it->vel[0];
// 	it->pos[1] += it->vel[1];
// 	it->pos[2] += it->vel[2];
//       }
    

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "stl list of Particle<36> : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_list_particle36.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------
//   {
//     // So far we used stl lists. What if we use a stl vector
//     // instead. The functionality is off-course not the same! BUT,
//     // what about the computing time?. 
    
//     //    std::cerr << "stl vector of Particle<3> ...\n";
    
//     const int particleSize = 3;
//     std::vector<Particle<particleSize> > particleVector(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
//       for(std::vector<Particle<particleSize> >::iterator it=particleVector.begin();it!=particleVector.end();it++) {
//       	it->pos[0] += it->vel[0];
//       	it->pos[1] += it->vel[1];
//       	it->pos[2] += it->vel[2];
//       }
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "vector of Particle<3>: " << elapsedTime << std::endl;

//     // We write the result to a file:
//     std::ofstream out("timer_vector_particle3.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   }
//   // ------------------------------------------------------ 

//   {
//     // We now do the big-memory particle!
    
//     //    std::cerr << "stl vector of Particle<36> ...\n";
    
//     const int particleSize = 36;
//     std::vector<Particle<particleSize> > particleVector(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
//       for(std::vector<Particle<particleSize> >::iterator it=particleVector.begin();it!=particleVector.end();it++) {
//       	it->pos[0] += it->vel[0];
//       	it->pos[1] += it->vel[1];
//       	it->pos[2] += it->vel[2];
//       }
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "vector of Particle<36>: " << elapsedTime << std::endl;

//     // We write the result to a file:
//     std::ofstream out("timer_vector_particle36.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   }
//   // ------------------------------------------------------ 

//   // ------------------------------------------------------
  
//   {
//     // Until now we considered our particle as an object and stored it
//     // in a stl container (list or vector) = vector of structure. What
//     // could we do else?  Store the position of all particles in a
//     // vector and their velocities in a separate vector = structure of
//     // vector.

//     StructVecTemplate<3> structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
// 	structVec.x(i,0) += structVec.v(i,0);
// 	structVec.x(i,1) += structVec.v(i,1);
// 	structVec.x(i,2) += structVec.v(i,2);
//       }
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of stl vectors<3> : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector3.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   }
  
//   // ------------------------------------------------------
  
//   {
//     // What if the particle is a big-memory particle?

//     StructVecTemplate<36> structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
// 	structVec.x(i,0) += structVec.v(i,0);
// 	structVec.x(i,1) += structVec.v(i,1);
// 	structVec.x(i,2) += structVec.v(i,2);
//       }
      
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of stl vectors<36> : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector36.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   }

//   // ------------------------------------------------------
  
//   {
//     // What if the particle is a big-memory particle?

//     StructVecTemplateLin<36> structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) 
// 	structVec.x(i,0) += structVec.v(i,0);
//       for(int i=0;i<particleNumber;i++) 
// 	structVec.x(i,0) += structVec.v(i,0);
//       for(int i=0;i<particleNumber;i++) 
// 	structVec.x(i,0) += structVec.v(i,0);
      
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of stl vectors lin <36> : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVectorLin36.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   }

//   // ------------------------------------------------------
  
//   {
//     // What if the particle is a big-memory particle?

//     StructVec1DTemplate<36> structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
// 	structVec.x(i,0) += structVec.v(i,0);
// 	structVec.x(i,1) += structVec.v(i,1);
// 	structVec.x(i,2) += structVec.v(i,2);
//       }
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of 1D stl vectors<36> : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOf1DStlVector36.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   }


// // ------------------------------------------------------

//   {
//     typedef BK_TYPELIST_5(Number,Position,Velocity,Acceleration,Diameter) ParticleAttributeList;
    
//     ParticleArray<ParticleAttributeList> particleArray(particleNumber);

//     // for(int i=0;i<particleNumber;i++) {
//     //   particleArray.number(i) = i;
//     // }
//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
//         particleArray.pos(i,0) += particleArray.vel(i,0);
// 	particleArray.pos(i,1) += particleArray.vel(i,1);
// 	particleArray.pos(i,2) += particleArray.vel(i,2);
//       }      
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of own vector : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfOwnVector.txt",std::ofstream::app);
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
//     out.close();
//   } 

//   float numberFloat;
//   int numberInt = int(numberFloat);


//   {
//     // Let us use a stl::map. We can add dynamically properities to
//     // our particle and access the data with the corresponding names:
//     // A map consists of pairs: <key, data>
//     std::map<std::string, std::vector<float> > particleArray;

//     // registering stl-vectors for the properties position and velocity:
//     particleArray["posX"].resize(particleNumber);
//     particleArray["posY"].resize(particleNumber);
//     particleArray["posZ"].resize(particleNumber);
//     particleArray["velX"].resize(particleNumber);
//     particleArray["velY"].resize(particleNumber);
//     particleArray["velZ"].resize(particleNumber);
    

//     double startTime = clock();
    
//     // for(int loop=0;loop<loopNumber;loop++) {
//     //   for(int i=0;i<particleNumber;i++) 
//     //     particleArray["posX"][i] += particleArray["velX"][i];
//     //   for(int i=0;i<particleNumber;i++) 
//     //     particleArray["posY"][i] += particleArray["velY"][i];
//     //   for(int i=0;i<particleNumber;i++) 
//     //     particleArray["posZ"][i] += particleArray["velZ"][i];
//     // }
//     // This computation is very slow. The reason is that the call
//     // particleArray["posX"] searches the vector each time in the map.

//     // Speeding up by creating references to the stl-vectors. With
//     // this we loose all the flexibility of the map. Not better than
//     // to allocate directly the vectors for posX,... (see below)

//     std::vector<float>& posX = particleArray["posX"];
//     std::vector<float>& posY = particleArray["posY"];
//     std::vector<float>& posZ = particleArray["posZ"];
//     std::vector<float>& velX = particleArray["velX"];
//     std::vector<float>& velY = particleArray["velY"];
//     std::vector<float>& velZ = particleArray["velZ"];

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) 
//         posX[i] += velX[i];
//       for(int i=0;i<particleNumber;i++) 
//         posY[i] += velY[i];
//       for(int i=0;i<particleNumber;i++) 
//         posZ[i] += velZ[i];
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "map of stl vector : " << elapsedTime << std::endl;

//   }

  {
    // simplest way: allocate 1d arrays
    float* x1 = new float[particleNumber];
    float* v1 = new float[particleNumber];
    float* x2 = new float[particleNumber];
    float* v2 = new float[particleNumber];
    float* x3 = new float[particleNumber];
    float* v3 = new float[particleNumber];
    
    // initialize
    for(int i=0;i<particleNumber;i++) {
      x1[i] = i;
      v1[i] = i;
      x2[i] = i;
      v2[i] = i;
      x3[i] = i;
      v3[i] = i;
    }
    
   double startTime = clock();
   
   for(int loop=0;loop<loopNumber;loop++) {
     for(int i=0;i<particleNumber;i++) 
       x1[i] += v1[i];
     for(int i=0;i<particleNumber;i++) 
       x2[i] += v2[i];
     for(int i=0;i<particleNumber;i++) 
       x3[i] += v3[i];
   }

   double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
   std::cerr << "1d arrays : " << elapsedTime << std::endl;

   delete [] x1,x2,x3,v1,v2,v3;
  }

  {
    // Computation with SIMD (single instruction - multiple
    // data). Each modern processor possesses an instruction set for
    // processing vectors with the same command such as 2.*v; The
    // compiler is not always capable to recognise the possibility of
    // "vectorizing" computation loops (for() v[i] = 2*v[i]). This can
    // be done by hand using specific SIMD instruction set (such as
    // sse of INTEL). However, these commands are low-level and not
    // portable from one processor family to another (intel, amd, IBM,
    // ...). There are attempts to build portable libraries using
    // SIMD such as BOOST.SIMD (http://www.numscale.com/boost-simd)
    // Let us see this at work:
    
    // abbreviations
    using boost::simd::sum;
    using boost::simd::pack;
    using boost::simd::load;
    using boost::simd::aligned_load;
    
    typedef pack<float> type;
    
    // allocate 1d arrays for x1,x2,x3 and v1,v2,v3
    float* x1 = new float[particleNumber];
    float* v1 = new float[particleNumber];
    float* x2 = new float[particleNumber];
    float* v2 = new float[particleNumber];
    float* x3 = new float[particleNumber];
    float* v3 = new float[particleNumber];
    
    // initialization of arrays
    for(int i=0;i<particleNumber;i++) {
      x1[i] = i;
      v1[i] = i;
      x2[i] = i;
      v2[i] = i;
      x3[i] = i;
      v3[i] = i;
    }
    
    double startTime = clock();
    // Let's consider that (last1-first1) is divisible by the size of the pack.
    for(int loop=0;loop<loopNumber;loop++) {
      
      // define pointers to first and last elements to treat
      float* first1 = &x1[0];
      float* last1 = &x1[0]+particleNumber;
      float* first2 = &v1[0];
      
      while(first1 != last1) {
	// Load current values from the datasets into pack
	pack<float> x1 = load< type >(first1);
	pack<float> x2 = load< type >(first2);
	
	// Computation with simd-overloaded operator +=
	x1 += x2;
	
	// Advance to the next SIMD vector
	first1 += type::static_size;
	first2 += type::static_size;
      }
      
      first1 = &x2[0];
      last1 = &x2[0]+particleNumber;
      first2 = &v2[0];
      
      while(first1 != last1) {
	pack<float> x1 = load< type >(first1);
	pack<float> x2 = load< type >(first2);
	
	x1 += x2;
	
	first1 += type::static_size;
	first2 += type::static_size;
      }
      
      first1 = &x3[0];
      last1 = &x3[0]+particleNumber;
      first2 = &v3[0];
      
      while(first1 != last1) {
	pack<float> x1 = load< type >(first1);
	pack<float> x2 = load< type >(first2);
	
	x1 += x2;
	
	first1 += type::static_size;
	first2 += type::static_size;
      }
    }
    double elapsedTime = (clock() -  startTime)/double(CLOCKS_PER_SEC);
    std::cerr << "simd on arrays : " << std::setprecision(8) << elapsedTime << std::endl;

    delete [] x1,x2,x3,v1,v2,v3;
  }


// ------------------------------------------------------
  
{

  BK::ParticleArray<std::tuple<BK::Id,BK::Position,BK::Velocity,BK::Acceleration,BK::Diameter>> particleArray(particleNumber);

  for(int i=0;i<particleNumber;i++) {
    particleArray.posX(i) = i;
    particleArray.posY(i) = i;
    particleArray.posZ(i) = i;
    particleArray.velX(i) = i;
    particleArray.velY(i) = i;
    particleArray.velZ(i) = i;
  }
    
  double startTime = clock();
  
  for(int loop=0;loop<loopNumber;loop++) {
    for(int i=0;i<particleNumber;i++)
      particleArray.pos(i,0) += particleArray.vel(i,0);
    for(int i=0;i<particleNumber;i++) 
      particleArray.pos(i,1) += particleArray.vel(i,1);
    for(int i=0;i<particleNumber;i++) 
      particleArray.pos(i,2) += particleArray.vel(i,2);
  }

  double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
  
  std::cerr << "ParticleArray pos(i,coord) : " << elapsedTime << std::endl;
  
  std::ofstream out("timer_particleArray.txt",std::ofstream::app);
  out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
  out.close();
}

{
  BK::ParticleArray<std::tuple<BK::Id,BK::Position,BK::Velocity,BK::Acceleration,BK::Diameter>> particleArray(particleNumber);

  for(int i=0;i<particleNumber;i++) {
    particleArray.posX(i) = i;
    particleArray.posY(i) = i;
    particleArray.posZ(i) = i;
    particleArray.velX(i) = i;
    particleArray.velY(i) = i;
    particleArray.velZ(i) = i;
  }
    
  double startTime = clock();
  
  for(int loop=0;loop<loopNumber;loop++) {
    for(int i=0;i<particleNumber;i++) 
      particleArray.posX(i) += particleArray.velX(i);
    for(int i=0;i<particleNumber;i++) 
      particleArray.posY(i) += particleArray.velY(i);
    for(int i=0;i<particleNumber;i++) 
      particleArray.posZ(i) += particleArray.velZ(i);
  }

  double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
  
  std::cerr << "ParticleArray posX(i) : " << elapsedTime << std::endl;
  
  std::ofstream out("timer_particleArray.txt",std::ofstream::app);
  out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
  out.close();
}

{
  BK::ParticleArray<std::tuple<BK::Id,BK::Position,BK::Velocity,BK::Acceleration,BK::Diameter>> particleArray(particleNumber);

  for(int i=0;i<particleNumber;i++) {
    particleArray.posX(i) = i;
    particleArray.posY(i) = i;
    particleArray.posZ(i) = i;
    particleArray.velX(i) = i;
    particleArray.velY(i) = i;
    particleArray.velZ(i) = i;
  }

  BK::ListVector<float>& posX = particleArray.posX();
  BK::ListVector<float>& posY = particleArray.posY();
  BK::ListVector<float>& posZ = particleArray.posZ();
  BK::ListVector<float>& velX = particleArray.velX();
  BK::ListVector<float>& velY = particleArray.velY();
  BK::ListVector<float>& velZ = particleArray.velZ();

  double startTime = clock();

  for(int loop=0;loop<loopNumber;loop++) {
    for(int i=0;i<particleNumber;i++) 
      posX[i] += velX[i];
    for(int i=0;i<particleNumber;i++) 
      posY[i] += velY[i];
    for(int i=0;i<particleNumber;i++) 
      posZ[i] += velZ[i];
  }

  double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
  
  std::cerr << "ParticleArray posX : " << elapsedTime << std::endl;
  
  std::ofstream out("timer_particleArray.txt",std::ofstream::app);
  out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
  out.close();
}

}  


//   // ------------------------------------------------------
//   {
//     std::cerr << "structure of stl lists ...\n";
    
//     StructList structList(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
//       std::list<float>::iterator itV0 = structList.velo0.begin();
//       for(std::list<float>::iterator it=structList.space0.begin();it!=structList.space0.end();it++) 
// 	*it += *(itV0++);
    
//       std::list<float>::iterator itV1 = structList.velo1.begin();
//       for(std::list<float>::iterator it=structList.space1.begin();it!=structList.space1.end();it++) 
// 	*it += *(itV1++);
      
//       std::list<float>::iterator itV2 = structList.velo2.begin();
//       for(std::list<float>::iterator it=structList.space2.begin();it!=structList.space2.end();it++) 
// 	*it += *(itV2++);
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of stl lists : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlList.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------

//  // ------------------------------------------------------
//   {
//     std::cerr << "structure2 of stl lists ...\n";
    
//     StructList2 structList(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
      
      
//       auto itV = structList.velo.begin();
//       for(auto it=structList.space.begin();it!=structList.space.end();it++) 
// 	for(int i=0;i<3;i++)
// 	  (*it)[i] += (*itV++)[i];
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure2 of stl lists : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlList2.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------

// // ------------------------------------------------------
//   {
//     std::cerr << "structure3 of stl lists ...\n";
    
//     StructList3 structList(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
//       for(auto v : structList.value) 
// 	for(int i=0;i<3;i++)
// 	  v[i] += v[i+3];
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure3 of stl lists : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlList3.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------
  
//   {
//     std::cerr << "structure of stl vectors ...\n";

//     StructVec structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
// 	structVec.space0[i] += structVec.velo0[i];
// 	structVec.space1[i] += structVec.velo1[i];
// 	structVec.space2[i] += structVec.velo2[i];
//       }
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of stl vectors : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------

//  // ------------------------------------------------------
  
//   {
//     std::cerr << "structure of stl vectors separate loops ...\n";

//     StructVec structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
      
//       // std::vector<float>::const_iterator cIt=structVec.velo0.begin();
//       // for(std::vector<float>::iterator it=structVec.space0.begin(); it!=structVec.space0.end();it++)
//       // 	*it += *(cIt++);
//       // cIt=structVec.velo1.begin();
//       // for(std::vector<float>::iterator it=structVec.space1.begin(); it!=structVec.space1.end();it++)
//       // 	*it += *(cIt++);
//       // cIt=structVec.velo2.begin();
//       // for(std::vector<float>::iterator it=structVec.space2.begin(); it!=structVec.space2.end();it++)
//       // 	*it += *(cIt++);
      
      
//       for(int i=0;i<particleNumber;i++) 
//       	structVec.space0[i] += structVec.velo0[i];

//       for(int i=0;i<particleNumber;i++) 
//       	structVec.space1[i] += structVec.velo1[i];
      
//       for(int i=0;i<particleNumber;i++) 
//       	structVec.space2[i] += structVec.velo2[i];
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure of stl vectors separate loops: " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector_sepLoop.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------

//  // ------------------------------------------------------
  
//   {
//     std::cerr << "structure2 of stl vectors ...\n";

//     StructVec2 structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
// 	structVec.space[i][0] += structVec.velo[i][0];
// 	structVec.space[i][1] += structVec.velo[i][1];
// 	structVec.space[i][2] += structVec.velo[i][2];
//       }
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure2 of stl vectors : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector2.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------

// // ------------------------------------------------------
  
//   {
//     std::cerr << "structure2 func of stl vectors ...\n";

//     StructVec2 structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
// 	structVec.x(i,0) += structVec.v(i,0);
// 	structVec.x(i,1) += structVec.v(i,1);
// 	structVec.x(i,2) += structVec.v(i,2);
//       }
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure2 func of stl vectors : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector2_func.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------

// // ------------------------------------------------------
  
//   {
//     std::cerr << "structure3 of stl vectors ...\n";

//     StructVec3 structVec(particleNumber);

//     double startTime = clock();

//     for(int loop=0;loop<loopNumber;loop++) {
//       for(int i=0;i<particleNumber;i++) {
// 	structVec.value[i][0] += structVec.value[i][3];
// 	structVec.value[i][1] += structVec.value[i][4];
// 	structVec.value[i][2] += structVec.value[i][5];
//       }
//     }

//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure3 of stl vectors : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector3.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------

//  // ------------------------------------------------------
  
//   {
//     std::cerr << "structure2 of stl vectors separate loop ...\n";

//     StructVec2 structVec(particleNumber);

//     double startTime = clock();

//    for(int loop=0;loop<loopNumber;loop++) {
      
//       for(int i=0;i<particleNumber;i++) 
// 	structVec.space[i][0] += structVec.velo[i][0];

//       for(int i=0;i<particleNumber;i++) 
// 	structVec.space[i][1] += structVec.velo[i][1];
      
//       for(int i=0;i<particleNumber;i++) 
// 	structVec.space[i][2] += structVec.velo[i][2];
//     }
   
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "structure2 of stl vectors separate loop: " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_structOfStlVector2_sepLoop.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------
//   // ------------------------------------------------------
//   {
//     std::cerr << "stl vector of FlatParticle<3> ...\n";
    
//     const int particleSize = 3;
//     std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++)
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) {
// 	it->value1[0] += it->value2[0];
// 	it->value1[1] += it->value2[1];
// 	it->value1[2] += it->value2[2];
//       }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "stl vector of FlatParticle<3> : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_vectorflatParticle3.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }
  
//   // ------------------------------------------------------

//   // ------------------------------------------------------
//   {
//     std::cerr << "stl vector of FlatParticle<36> ...\n";
    
//     const int particleSize = 36;
//     std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++)
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) {
// 	it->value1[0] += it->value2[0];
// 	it->value1[1] += it->value2[1];
// 	it->value1[2] += it->value2[2];
//       }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "stl vector of FlatParticle<36> : " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_vectorflatParticle36.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }
  
//   // ------------------------------------------------------
//   {
//     std::cerr << "stl vector of FlatParticle<3> separate loops ...\n";
    
//     const int particleSize = 3;
//     std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
// 	it->value1[0] += it->value2[0];
      
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
// 	it->value1[1] += it->value2[1];
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
// 	it->value1[2] += it->value2[2];
//     }
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "stl vector of FlatParticle<3> separate loops: " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_vectorflatParticle3_sepLoop.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }

//   // ------------------------------------------------------
//   {
//     std::cerr << "stl vector of FlatParticle<3> separate loops ...\n";
    
//     const int particleSize = 36;
//     std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
//     double startTime = clock();
    
//     for(int loop=0;loop<loopNumber;loop++) {
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
// 	it->value1[0] += it->value2[0];
      
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
// 	it->value1[1] += it->value2[1];
//       for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
// 	it->value1[2] += it->value2[2];
//     }
    
    
//     double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
//     std::cerr << "stl vector of FlatParticle<36> separate loops: " << elapsedTime << std::endl;
    
//     std::ofstream out("timer_vectorflatParticle36_sepLoop.txt",std::ofstream::app);
    
//     out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
//     out.close();
//   }
 
