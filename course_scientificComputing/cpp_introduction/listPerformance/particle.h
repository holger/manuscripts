#ifndef flatParticle_h
#define flatParticle_h

template<int SIZE>
class Particle
{
public:
  
  Particle()  {
    for(int i=0;i<SIZE;i++) {
      pos[i] = i;
      vel[i] = i;
    }
  }

  float pos[SIZE];
  float vel[SIZE];
};



#endif
