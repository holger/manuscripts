#ifndef particleMap_h
#define particleMap_h

#include <map>
#include <vector>

class ParticleMap
{
 public:
  std::map<std::string, std::vector<double> > particleArray;
};

#endif
