#ifndef flatParticle_h
#define flatParticle_h

template<int SIZE>
class FlatParticle
{
public:
  
  FlatParticle() : size(SIZE) {
    for(int i=0;i<size;i++) {
      value1[i] = i;
      value2[i] = i;
    }
  }

  float value1[SIZE];
  float value2[SIZE];

  int size;
};



#endif
