#include <iostream>
#include <string>
#include <cstdlib>
#include <list>
#include <fstream>
#include <array>

#include "flatParticle.h"
#include "structList.h"
#include "structList2.h"
#include "structVec.h"
#include "structVec2.h"
#include "structVec3.h"


int main(int argc, char** argv)
{
  std::cerr << "test of different types of lists starts ...\n";

  const int particleNumber = atoi(argv[1]);

  int nCount = 1e7;
  int loopNumber = nCount/particleNumber;

  std::cerr << "nCount = " << nCount 
	    << "  particleNumber = " << particleNumber 
	    << "  loopNumber = " << loopNumber << std::endl;
  
  // ------------------------------------------------------
  {
    std::cerr << "stl list of FlatParticle<3> ...\n";
    
    const int particleSize = 3;
    std::list<FlatParticle<particleSize> > flatParticleList;
    for(int i=0;i<particleNumber;i++) 
      flatParticleList.push_back(FlatParticle<particleSize>());
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++)
      for(std::list<FlatParticle<particleSize> >::iterator it=flatParticleList.begin();it!=flatParticleList.end();it++) {
	it->value1[0] += it->value2[0];
	it->value1[1] += it->value2[1];
	it->value1[2] += it->value2[2];
      }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl list of FlatParticle<3> : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_listflatParticle3.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }
  // ------------------------------------------------------ 

  {
    std::cerr << "stl list of FlatParticle<3> separate loops ...\n";
    
    const int particleSize = 3;
    std::list<FlatParticle<particleSize> > flatParticleList;
    for(int i=0;i<particleNumber;i++) 
      flatParticleList.push_back(FlatParticle<particleSize>());
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++) {
      for(std::list<FlatParticle<particleSize> >::iterator it=flatParticleList.begin();it!=flatParticleList.end();it++) 
	it->value1[0] += it->value2[0];
    
      for(std::list<FlatParticle<particleSize> >::iterator it=flatParticleList.begin();it!=flatParticleList.end();it++)
	it->value1[1] += it->value2[1];
      
      for(std::list<FlatParticle<particleSize> >::iterator it=flatParticleList.begin();it!=flatParticleList.end();it++) 
	it->value1[2] += it->value2[2];
    }

    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl list of FlatParticle<3> : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_listflatParticle3_sepLoop.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------
  {
    std::cerr << "stl list of FlatParticle<36> ...\n";
    
    const int particleSize = 36;
    std::list<FlatParticle<particleSize> > flatParticleList;
    for(int i=0;i<particleNumber;i++) 
      flatParticleList.push_back(FlatParticle<particleSize>());
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++)
      for(std::list<FlatParticle<particleSize> >::iterator it=flatParticleList.begin();it!=flatParticleList.end();it++) {
	it->value1[0] += it->value2[0];
	it->value1[1] += it->value2[1];
	it->value1[2] += it->value2[2];
      }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl list of FlatParticle<36> : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_listflatParticle36.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------
  {
    std::cerr << "structure of stl lists ...\n";
    
    StructList structList(particleNumber);
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++) {
      std::list<float>::iterator itV0 = structList.velo0.begin();
      for(std::list<float>::iterator it=structList.space0.begin();it!=structList.space0.end();it++) 
	*it += *(itV0++);
      
      std::list<float>::iterator itV1 = structList.velo1.begin();
      for(std::list<float>::iterator it=structList.space1.begin();it!=structList.space1.end();it++) 
	*it += *(itV1++);
      
      std::list<float>::iterator itV2 = structList.velo2.begin();
      for(std::list<float>::iterator it=structList.space2.begin();it!=structList.space2.end();it++) 
	*it += *(itV2++);
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure of stl lists : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlList.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------

 // ------------------------------------------------------
  {
    std::cerr << "structure2 of stl lists ...\n";
    
    StructList2 structList(particleNumber);
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++) {
      
      
      auto itV = structList.velo.begin();
      for(auto it=structList.space.begin();it!=structList.space.end();it++) 
	for(int i=0;i<3;i++)
	  (*it)[i] += (*itV++)[i];
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure2 of stl lists : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlList2.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------

// ------------------------------------------------------
  {
    std::cerr << "structure3 of stl lists ...\n";
    
    StructList3 structList(particleNumber);
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++) {
      for(auto v : structList.value) 
	for(int i=0;i<3;i++)
	  v[i] += v[i+3];
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure3 of stl lists : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlList3.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------
  
  {
    std::cerr << "structure of stl vectors ...\n";

    StructVec structVec(particleNumber);

    double startTime = clock();

    for(int loop=0;loop<loopNumber;loop++) {
      for(int i=0;i<particleNumber;i++) {
	structVec.space0[i] += structVec.velo0[i];
	structVec.space1[i] += structVec.velo1[i];
	structVec.space2[i] += structVec.velo2[i];
      }
    }

    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure of stl vectors : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlVector.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------

 // ------------------------------------------------------
  
  {
    std::cerr << "structure of stl vectors separate loops ...\n";

    StructVec structVec(particleNumber);

    double startTime = clock();

    for(int loop=0;loop<loopNumber;loop++) {
      
      // std::vector<float>::const_iterator cIt=structVec.velo0.begin();
      // for(std::vector<float>::iterator it=structVec.space0.begin(); it!=structVec.space0.end();it++)
      // 	*it += *(cIt++);
      // cIt=structVec.velo1.begin();
      // for(std::vector<float>::iterator it=structVec.space1.begin(); it!=structVec.space1.end();it++)
      // 	*it += *(cIt++);
      // cIt=structVec.velo2.begin();
      // for(std::vector<float>::iterator it=structVec.space2.begin(); it!=structVec.space2.end();it++)
      // 	*it += *(cIt++);
      
      
      for(int i=0;i<particleNumber;i++) 
      	structVec.space0[i] += structVec.velo0[i];

      for(int i=0;i<particleNumber;i++) 
      	structVec.space1[i] += structVec.velo1[i];
      
      for(int i=0;i<particleNumber;i++) 
      	structVec.space2[i] += structVec.velo2[i];
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure of stl vectors separate loops: " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlVector_sepLoop.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------

 // ------------------------------------------------------
  
  {
    std::cerr << "structure2 of stl vectors ...\n";

    StructVec2 structVec(particleNumber);

    double startTime = clock();

    for(int loop=0;loop<loopNumber;loop++) {
      for(int i=0;i<particleNumber;i++) {
	structVec.space[i][0] += structVec.velo[i][0];
	structVec.space[i][1] += structVec.velo[i][1];
	structVec.space[i][2] += structVec.velo[i][2];
      }
    }

    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure2 of stl vectors : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlVector2.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------

// ------------------------------------------------------
  
  {
    std::cerr << "structure2 func of stl vectors ...\n";

    StructVec2 structVec(particleNumber);

    double startTime = clock();

    for(int loop=0;loop<loopNumber;loop++) {
      for(int i=0;i<particleNumber;i++) {
	structVec.x(i,0) += structVec.v(i,0);
	structVec.x(i,1) += structVec.v(i,1);
	structVec.x(i,2) += structVec.v(i,2);
      }
    }

    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure2 func of stl vectors : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlVector2_func.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------

// ------------------------------------------------------
  
  {
    std::cerr << "structure3 of stl vectors ...\n";

    StructVec3 structVec(particleNumber);

    double startTime = clock();

    for(int loop=0;loop<loopNumber;loop++) {
      for(int i=0;i<particleNumber;i++) {
	structVec.value[i][0] += structVec.value[i][3];
	structVec.value[i][1] += structVec.value[i][4];
	structVec.value[i][2] += structVec.value[i][5];
      }
    }

    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure3 of stl vectors : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlVector3.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------

 // ------------------------------------------------------
  
  {
    std::cerr << "structure2 of stl vectors separate loop ...\n";

    StructVec2 structVec(particleNumber);

    double startTime = clock();

   for(int loop=0;loop<loopNumber;loop++) {
      
      for(int i=0;i<particleNumber;i++) 
	structVec.space[i][0] += structVec.velo[i][0];

      for(int i=0;i<particleNumber;i++) 
	structVec.space[i][1] += structVec.velo[i][1];
      
      for(int i=0;i<particleNumber;i++) 
	structVec.space[i][2] += structVec.velo[i][2];
    }
   
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "structure2 of stl vectors separate loop: " << elapsedTime << std::endl;
    
    std::ofstream out("timer_structOfStlVector2_sepLoop.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------
  // ------------------------------------------------------
  {
    std::cerr << "stl vector of FlatParticle<3> ...\n";
    
    const int particleSize = 3;
    std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++)
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) {
	it->value1[0] += it->value2[0];
	it->value1[1] += it->value2[1];
	it->value1[2] += it->value2[2];
      }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl vector of FlatParticle<3> : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_vectorflatParticle3.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }
  
  // ------------------------------------------------------

  // ------------------------------------------------------
  {
    std::cerr << "stl vector of FlatParticle<36> ...\n";
    
    const int particleSize = 36;
    std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++)
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) {
	it->value1[0] += it->value2[0];
	it->value1[1] += it->value2[1];
	it->value1[2] += it->value2[2];
      }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl vector of FlatParticle<36> : " << elapsedTime << std::endl;
    
    std::ofstream out("timer_vectorflatParticle36.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }
  
  // ------------------------------------------------------
  {
    std::cerr << "stl vector of FlatParticle<3> separate loops ...\n";
    
    const int particleSize = 3;
    std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++) {
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
	it->value1[0] += it->value2[0];
      
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
	it->value1[1] += it->value2[1];
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
	it->value1[2] += it->value2[2];
    }
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl vector of FlatParticle<3> separate loops: " << elapsedTime << std::endl;
    
    std::ofstream out("timer_vectorflatParticle3_sepLoop.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }

  // ------------------------------------------------------
  {
    std::cerr << "stl vector of FlatParticle<3> separate loops ...\n";
    
    const int particleSize = 36;
    std::vector<FlatParticle<particleSize> > flatParticleVec(particleNumber);
    
    double startTime = clock();
    
    for(int loop=0;loop<loopNumber;loop++) {
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
	it->value1[0] += it->value2[0];
      
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
	it->value1[1] += it->value2[1];
      for(std::vector<FlatParticle<particleSize> >::iterator it=flatParticleVec.begin();it!=flatParticleVec.end();it++) 
	it->value1[2] += it->value2[2];
    }
    
    
    double elapsedTime = (clock() -  startTime)/CLOCKS_PER_SEC;
    
    std::cerr << "stl vector of FlatParticle<36> separate loops: " << elapsedTime << std::endl;
    
    std::ofstream out("timer_vectorflatParticle36_sepLoop.txt",std::ofstream::app);
    
    out << particleNumber << "\t" << loopNumber << "\t" << elapsedTime << std::endl;
    
    out.close();
  }
  
}
