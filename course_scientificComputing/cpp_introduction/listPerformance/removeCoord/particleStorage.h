#ifndef particleStorage_h
#define particleStorage_h

#include <vector>
#include "/home/holger/include/BasisKlassen/HierarchyGenerators.h"

class Array3
{
 public:
  float operator[](int i) const {return data[i];}
  float& operator[](int i){return data[i];}
  
 private:
  float data[3];
};

class ParticleStorage
{
 protected:
  std::vector<std::vector<Array3 > > data_;
};

class Position 
{
 public:
  enum Att { attributeNumber = 3 };
  
  template<int attIndex>
  class Access : public virtual ParticleStorage {
  public:
    float pos(int particle, int coord) const { return ParticleStorage::data_[attIndex_][particle][coord];}
    float& pos(int particle, int coord) { return ParticleStorage::data_[attIndex_][particle][coord];}
    
    const int attIndex_;

    Access() : attIndex_(attIndex) {}
  };
};

class Velocity
{
 public:
  enum Att { attributeNumber = 3 };
  
  template<int attIndex>
  class Access : public virtual ParticleStorage {
  public:
    float vel(int particle, int coord) const { return ParticleStorage::data_[attIndex_][particle][coord];}
    float& vel(int particle, int coord) { return ParticleStorage::data_[attIndex_][particle][coord];}
    
    const int attIndex_;

    Access() : attIndex_(attIndex) {}
  };
};

class Acceleration
{
 public:
  enum Att { attributeNumber = 3 };
  
  template<int attIndex>
  class Access : public virtual ParticleStorage {
  public:
    float acc(int particle, int coord) const { return ParticleStorage::data_[attIndex_][particle][coord];}
    float& acc(int particle, int coord) { return ParticleStorage::data_[attIndex_][particle][coord];}
    
    const int attIndex_;

    Access() : attIndex_(attIndex) {}
  };
};

class Diameter
{
 public:
  enum Att { attributeNumber = 1 };
  
  template<int attIndex>
  class Access : public virtual ParticleStorage {
  public:
    float dia(int particle, int coord) const { return this->data_[attIndex_][particle][coord];}
    float& dia(int particle, int coord) { return this->data_[attIndex_][particle][coord];}
    
    const int attIndex_;

    Access() : attIndex_(attIndex) {}
  };
};

template<class TypeList>
class InheritorHolder
{
 public:
  template<class T>
    class Inheritor : public T::template Access<BK::TL::IndexOfAtt<TypeList,T>::value > {};
};

template<class TypeList>
class ParticleArray : public virtual BK::GenScatterHierarchy<TypeList,InheritorHolder<TypeList>::template Inheritor >
{
 public:
  ParticleArray(int particleNumber)
  {
    attributeNumber_ = BK::TL::NumberOfAtt<TypeList>::value;
    this->data_.resize(attributeNumber_);
    for(int i=0;i<attributeNumber_;i++)
      this->data_[i].resize(particleNumber);

  }
  
 private:
  int attributeNumber_;
  int particleNumber_;
};

#endif
