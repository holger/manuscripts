#ifndef structList_h
#define structList_h

#include <list>

class StructList
{
public:
  
  StructList(int size) {
    for(int i=0;i<size;i++) {
      space0.push_back(i);
      space1.push_back(i);
      space2.push_back(i);
      
      velo0.push_back(i);
      velo1.push_back(i);
      velo2.push_back(i);
    }
  }
  
  std::list<float> space0;
  std::list<float> space1;
  std::list<float> space2;

  std::list<float> velo0;
  std::list<float> velo1;
  std::list<float> velo2;
};


#endif
