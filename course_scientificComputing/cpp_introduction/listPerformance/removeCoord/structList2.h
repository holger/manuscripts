#ifndef structList2_h
#define structList2_h

#include <list>

class StructList2
{
public:
  
  StructList2(int size) {
    for(int i=0;i<size;i++) {
      space.push_back(std::array<float,3>());
      velo.push_back(std::array<float,3>());
    }
    
    for(auto s : space) 
      for(int i=0;i<3;i++)
	s[i] = i;
    
    for(auto s : velo) 
      for(int i=0;i<3;i++)
	s[i] = i;
    
  }

  std::list<std::array<float,3> > space;
  std::list<std::array<float,3> > velo;
};


class StructList3
{
public:
  
  StructList3(int size) {
    for(int i=0;i<size;i++) {
      value.push_back(std::array<float,6>());
    }
    
    for(auto s : value) 
      for(int i=0;i<6;i++)
	s[i] = i;
    
  }

  std::list<std::array<float,6> > value;
};


#endif
