#ifndef structVec_h
#define structVec_h

#include <vector>
//#include <array>

class Array
{
 public:
  float operator[](int i) const {return data[i];}
  float& operator[](int i){return data[i];}
  
 private:
  float data[3];
};

class StructVec
{
public:
  
  StructVec(int size) {
    pos.resize(size);
    vel.resize(size);

    for(int i=0;i<size;i++) {
      for(int j=0;j<3;j++) {
    	pos[i][j] = i;
    	vel[i][j] = i;
      }
    }
  }
  
  
  //  std::vector<std::array<float,3> > pos;
  //std::vector<std::array<float,3> > vel;
  std::vector<Array> pos;
  std::vector<Array> vel;

  float x(int p, int c) const { return pos[p][c]; }
  float v(int p, int c) const { return vel[p][c]; }
  float& x(int p, int c) { return pos[p][c]; }
  float& v(int p, int c) { return vel[p][c]; }
};

template<int ATT_NUMBER>
class StructVecTemplate
{
public:
  
  StructVecTemplate(int size) {
    data.resize(ATT_NUMBER);
    
    for(int i=0;i<ATT_NUMBER;i++)
      data[i].resize(size);
    
    for(int k=0;k<ATT_NUMBER;k++)
      for(int i=0;i<size;i++) 
    	for(int j=0;j<3;j++) {
    	  data[k][i][j] = i;
    	  data[k][i][j] = i;
    	}
    
    
  }
  
  std::vector<std::vector<Array > > data;
  // std::vector<std::vector<std::array<float,3> > > data;

  float x(int p, int c) const { return data[0][p][c]; }
  float v(int p, int c) const { return data[1][p][c]; }
  float& x(int p, int c) { return data[0][p][c]; }
  float& v(int p, int c) { return data[1][p][c]; }

};

template<int ATT_NUMBER>
class StructVecTemplateLin
{
public:
  
  StructVecTemplateLin(int size) {
    data.resize(ATT_NUMBER);
    
    for(int i=0;i<ATT_NUMBER;i++)
      data[i].resize(size);
    
    for(int k=0;k<ATT_NUMBER;k++) 
      for(int i=0;i<size;i++) {
	data[k][i] = i;
	data[k][i] = i;
      }
  }
  
  std::vector<std::vector<float> > data;
  // std::vector<std::vector<std::array<float,3> > > data;

  float x(int p, int c) const { return data[c][p]; }
  float v(int p, int c) const { return data[3+c][p]; }
  float& x(int p, int c) { return data[c][p]; }
  float& v(int p, int c) { return data[3+c][p]; }

};

template<int ATT_NUMBER>
class StructVec1DTemplate
{
public:
  
  StructVec1DTemplate(int size) {
    size_ = size;
    dataX.resize(size*3);
    dataV.resize(size*3);
    
    for(int k=0;k<size*3;k++) {
      dataX[k] = k;
      dataV[k] = k;
    }
  }
  
  std::vector<float> dataX;
  std::vector<float> dataV;

  float x(int p, int c) const { return dataX[3*p+c]; }
  float v(int p, int c) const { return dataV[3*p+c]; }
  float& x(int p, int c) { return dataX[3*p+c]; } 
  float& v(int p, int c) { return dataV[3*p+c]; }

 private:
  int size_;
  
};


#endif
