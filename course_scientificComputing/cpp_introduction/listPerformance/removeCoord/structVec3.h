#ifndef structVec3_h
#define structVec3_h

#include <vector>
#include <array>

class StructVec3
{
public:
  
  StructVec3(int size) {
    value.resize(size);

    space3.resize(size);
    space4.resize(size);
    space5.resize(size);
    velo3.resize(size);
    velo4.resize(size);
    velo5.resize(size);

    for(int i=0;i<size;i++) {
      value[i][0] = i;
      value[i][1] = i;
      value[i][2] = i;
      value[i][3] = i;
      value[i][4] = i;
      value[i][5] = i;
    }
  }

  std::vector<std::array<float,6> > value;

  std::vector<float> space3;
  std::vector<float> space4;
  std::vector<float> space5;

  std::vector<float> velo3;
  std::vector<float> velo4;
  std::vector<float> velo5;
};


#endif
