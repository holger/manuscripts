#include <map>
#include <functional>
#include <iostream>

void printSteve()
{
  std::cout << "my name is steve\n";
}

void printBob()
{
  std::cout << "my name is bob\n";
}

class Car
{
public:
  void accelerate() {
    std::cout << "Reaching 50km/h...\n";
  }
};

int main()
{
  std::cout << "call-back test with maps starts ...\n";

  std::map<std::string, int> testMap;

  testMap["steve"] = 1;
  testMap["bob"] = 2;

  std::cout << (*testMap.find("steve")).second << std::endl;
  
  std::function<void(void)> printSteveFunctor(printSteve);
  std::function<void(void)> printBobFunctor(printBob);

  std::map<std::string, std::function<void(void)>> callBackMap;
  callBackMap["steve"] = printSteveFunctor;
  callBackMap["Bob"] = printBobFunctor;

  Car car;
  std::function<void(void)> carFunctor = std::bind(&Car::accelerate, car);
  //  auto carFunctor = std::bind(&Car::accelerate, car);
  //  auto carFunctor = std::mem_fn(&Car::accelerate);

  callBackMap["car"] = carFunctor;
  
  for(auto it : callBackMap)
    it.second();
}
