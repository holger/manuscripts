// mpiCC -o main main.cc

#include <iostream>     
#include <vector> 
#include <cmath>
#include <mpi.h>

// MPI implementation of the derivative of a function
// 
// Each processor has its own memory.
// Each processor reads each line of the code for its own.
// Communication between processes by MPI functions

int main(int argc, char** argv) 
{
  // initialize MPI with command line arguments
  MPI_Init(&argc,&argv);

  int commRank; // id of process
  int commSize; // number of processes
  MPI_Comm_rank(MPI_COMM_WORLD,&commRank);
  MPI_Comm_size(MPI_COMM_WORLD,&commSize);
  
  std::cerr << "commSize = " << commSize << "\t commRank = " << commRank << std::endl;
  
  // global size of vector
  int n=100;
  // local size of vector
  int nLocal = n/commSize;
  
  // allocate vector of LOCAL size
  std::vector<double> vec(nLocal);

  double dx = 2*M_PI/n;
  // left boundary of local physical domain
  double localStart = n/commSize*dx*commRank;

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << commRank << " : localStart = " << localStart << std::endl;
  MPI_Barrier(MPI_COMM_WORLD);
  
  // allocate array for result
  std::vector<double> vecDiff(nLocal);
  // initialization
  for(int i=0;i<nLocal;i++) {
    vec[i] = sin(dx*i+localStart);
    vecDiff[i] = 0;
  }
  
  // compute derivative
  for(int i=0;i<nLocal-1;i++) {
    vecDiff[i] = (vec[i+1]-vec[i])/dx;
  }

  // exchange boundary value (vec[0]) from commRank to commRank-1
  double boundaryVal; // get this value from right process

  MPI_Status status_send;
  MPI_Status status_recv;
  int tag = 1;
  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "dest = " << (commRank-1+commSize)%commSize << "  source = " << commRank << std::endl;;
  MPI_Barrier(MPI_COMM_WORLD);
  
  // non-blocking exchanges:
  
  // 
  MPI_Request request_send;

  // MPI_Isend(buf, count, datatype, dest, tag, comm, request)   
  MPI_Isend(&vec[0], 1, MPI_DOUBLE, (commRank-1+commSize)%commSize,0 , MPI_COMM_WORLD, &request_send);

  MPI_Request request_recv;

  // MPI_Irecv(buf, count, datatype, source, tag, comm, request) 
  MPI_Irecv(&boundaryVal, 1, MPI_DOUBLE, (commRank+1)%commSize,0 , MPI_COMM_WORLD, &request_recv);

  // wait until send and receive completed
  MPI_Wait(&request_send, &status_send);
  MPI_Wait(&request_recv, &status_recv);

  // compute derivative for boundary value
  vecDiff[nLocal-1] = (boundaryVal - vec[nLocal-1])/dx;
  
  // output
  for(int rank=0;rank<commSize;rank++) {
    if(commRank == rank)
      for(int i=0;i<nLocal;i++)
	std::cout << i+n/commSize*commRank << "\t" << vec[i] << "\t" << vecDiff[i] << std::endl;
    
    MPI_Barrier(MPI_COMM_WORLD);
  }

  MPI_Finalize();
  
  return 0;
}
