// g++ -o course course.cpp -fopenmp -std=c++11 -O3

#include <iostream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <cmath>
#include <omp.h>

int main(int argc, char **argv)
{
  std::cerr << "Openmp test starts ...\n";
  
  if(argc < 4) {
    std::cerr << "ERROR: argc = " << argc << " < 3\n";
    std::cerr << "usage: ./course <n> <maxThreadNumber> <loopNumber>\n";
    exit(1);
  }
  
  int n = atoi(argv[1]);
  int maxThreadNumber = atoi(argv[2]);
  int loopNumber = atoi(argv[3]);
  
  std::vector<double> vec(n);
  std::vector<double> vecDiff(n);

  for(int i=0;i<n;i++)
    vec[i] = sin(2*M_PI/n*i);

  std::vector<double> speedUpVec(maxThreadNumber);
  
  for(int threadNumber=1;threadNumber<=maxThreadNumber;threadNumber++) {
  
    omp_set_num_threads(threadNumber);
  
#pragma omp parallel

    threadNumber = omp_get_num_threads();
  
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    for(int loop = 0; loop < loopNumber; loop++) {
#pragma omp parallel for
      for(int i=0; i < n-1; i++)
	vecDiff[i] = vec[i+1] - vec[i];
    }

    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;

    std::cout << "elapsed time with " << threadNumber << " threads : " << elapsed_seconds.count() << "s\n";
    speedUpVec[threadNumber-1] = elapsed_seconds.count();
  }

  for(int i=1;i<maxThreadNumber;i++)
    speedUpVec[i] = speedUpVec[0]/speedUpVec[i];

  for(int i=0;i<maxThreadNumber;i++)
    std::cerr << i << "\t" << speedUpVec[i] << std::endl;
}
