#include <iostream>
#include <cstdlib>

class Particle
{
public:

  static int maxId;

  void accelerate() {
    pos_ += vel_;
    vel_ += 1;
  }
  void decelerate() {
    pos_ += vel_;
    vel_ -= 1;
  }

  int get_id() {
    return id_;
  }

  double get_pos() {
    return pos_;
  }

  double get_vel() {
    return vel_;
  }
  
  void init() {
    id_ = maxId;
    ++maxId;
  }

private:

  int id_;
  double pos_;
  double vel_;
};

int Particle::maxId = 0;

int main()
{
  int n = 10;
  // particle array creation
  Particle* pArr = new Particle[n];

  // initialisation
  for(int p=0;p<n;p++) {
    pArr[p].init();
  }

  // move randomly!
  for(int i=0;i<100;i++) 
    for(int p=0;p<n;p++) {
      if(double(rand())/RAND_MAX >= 0.5)
	pArr[p].accelerate();
      else
	pArr[p].decelerate();
    }
  
  for(int p=0;p<n;p++)
    std::cout << pArr[p].get_id() << "\t" << pArr[p].get_pos() << "\t" << pArr[p].get_vel() << std::endl;
  
  
}
