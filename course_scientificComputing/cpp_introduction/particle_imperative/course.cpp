
#include <iostream>
#include <cstdlib>

// how to implement particles that can move, accelerate and
// decelerate

void accelerate(double& pos, double& vel)
{
  pos += vel;
  vel += 1;
}

void decelerate(double& pos, double& vel)
{
  pos += vel;
  vel -= 1;
}

int main()
{
  // one particle
  int pId = 0;
  double pPos = 0;
  double pVel = 0;

  // Many particles (10)
  int n = 10;
  int* pIdArr = new int[n];
  double* pPosArr = new double[n];
  double* pVelArr = new double[n];

  // initialisation
  for(int p=0;p<n;p++) {
    pIdArr[p] = p;
    pPosArr[p] = 0;
    pVelArr[p] = 0;
  }

  // move randomly!
  for(int i=0;i<100;i++) 
    for(int p=0;p<n;p++) {
      if(double(rand())/RAND_MAX >= 0.5)
	accelerate(pPosArr[p],pVelArr[p]);
      else
	decelerate(pPosArr[p],pVelArr[p]);
    }
  
  for(int p=0;p<n;p++)
    std::cout << pIdArr[p] << "\t" << pPosArr[p] << "\t" << pVelArr[p] << std::endl;

  // safety ? All is possible:

  pIdArr[1] = 0;
  pPosArr[1] = -1;
}
