	.file	"course.cpp"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZnwmPv,"axG",@progbits,_ZnwmPv,comdat
	.weak	_ZnwmPv
	.type	_ZnwmPv, @function
_ZnwmPv:
.LFB429:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE429:
	.size	_ZnwmPv, .-_ZnwmPv
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_,"axG",@progbits,_ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_,comdat
	.align 2
	.weak	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_
	.type	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_, @function
_ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_:
.LFB2084:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2084:
	.size	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_, .-_ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_
	.section	.text._ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv,"axG",@progbits,_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv,comdat
	.align 2
	.weak	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	.type	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv, @function
_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv:
.LFB2088:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2088:
	.size	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv, .-_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	.section	.text._ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv,"axG",@progbits,_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv,comdat
	.align 2
	.weak	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	.type	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv, @function
_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv:
.LFB2090:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2090:
	.size	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv, .-_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	.section	.rodata
	.align 4
	.type	_ZL8tempSize, @object
	.size	_ZL8tempSize, 4
_ZL8tempSize:
	.zero	4
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED2Ev,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED2Ev
	.type	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED2Ev, @function
_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED2Ev:
.LFB2114:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2114:
	.size	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED2Ev, .-_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED2Ev
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED1Ev
	.set	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED1Ev,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED2Ev
	.section	.text._ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_,"axG",@progbits,_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_,comdat
	.weak	_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_
	.type	_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_, @function
_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_:
.LFB2117:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	movq	%rax, %rbx
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	subq	%rax, %rbx
	movq	%rbx, %rax
	movq	%rax, -32(%rbp)
	leaq	-32(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chrono8durationIlSt5ratioILl1ELl1000000000EEEC1IlvEERKT_
	movq	-48(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L11
	call	__stack_chk_fail
.L11:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2117:
	.size	_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_, .-_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_
	.weak	_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIINS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_
	.set	_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIINS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_,_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_
	.section	.text._ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE,"axG",@progbits,_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE,comdat
	.weak	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE
	.type	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE, @function
_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE:
.LFB2116:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	movq	%rax, -16(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono10time_pointINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEEE16time_since_epochEv
	movq	%rax, -32(%rbp)
	leaq	-16(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chronomiIlSt5ratioILl1ELl1000000000EElS2_EENSt11common_typeIJNS_8durationIT_T0_EENS4_IT1_T2_EEEE4typeERKS7_RKSA_
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L14
	call	__stack_chk_fail
.L14:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2116:
	.size	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE, .-_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE
	.weak	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIIT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE
	.set	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIIT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE,_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE
	.section	.text._ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_,"axG",@progbits,_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_,comdat
	.align 2
	.weak	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_
	.type	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_, @function
_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_:
.LFB2123:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movsd	(%rax), %xmm0
	movq	-8(%rbp), %rax
	movsd	%xmm0, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2123:
	.size	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_, .-_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_
	.section	.text._ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE,"axG",@progbits,_ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE,comdat
	.weak	_ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE
	.type	_ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE, @function
_ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE:
.LFB2120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIlSt5ratioILl1ELl1000000000EEE5countEv
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	.LC0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, -16(%rbp)
	leaq	-16(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IdvEERKT_
	movsd	-32(%rbp), %xmm0
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L18
	call	__stack_chk_fail
.L18:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2120:
	.size	_ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE, .-_ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE
	.section	.text._ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE,"axG",@progbits,_ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE,comdat
	.weak	_ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE
	.type	_ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE, @function
_ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE:
.LFB2119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6chrono20__duration_cast_implINS_8durationIdSt5ratioILl1ELl1EEEES2_ILl1ELl1000000000EEdLb1ELb0EE6__castIlS5_EES4_RKNS1_IT_T0_EE
	movq	%xmm0, %rax
	movq	%rax, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2119:
	.size	_ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE, .-_ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE
	.section	.text._ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv,"axG",@progbits,_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv,comdat
	.align 2
	.weak	_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv
	.type	_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv, @function
_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv:
.LFB2124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movsd	(%rax), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2124:
	.size	_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv, .-_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv
	.section	.text._ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE,"axG",@progbits,_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE,comdat
	.align 2
	.weak	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE
	.type	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE, @function
_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE:
.LFB2126:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6chrono13duration_castINS_8durationIdSt5ratioILl1ELl1EEEElS2_ILl1ELl1000000000EEEENSt9enable_ifIXsrNS_13__is_durationIT_EE5valueES8_E4typeERKNS1_IT0_T1_EE
	movq	%xmm0, %rax
	movq	%rax, -40(%rbp)
	movsd	-40(%rbp), %xmm0
	movsd	%xmm0, -16(%rbp)
	leaq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv
	movq	%xmm0, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, (%rax)
	nop
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L24
	call	__stack_chk_fail
.L24:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2126:
	.size	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE, .-_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE
	.section	.rodata
	.align 8
.LC1:
	.string	"Computing time measurements ...\n"
	.align 8
.LC2:
	.string	"usage: ./course <particle number> <loop number>\n"
.LC3:
	.string	"particleNumber = "
.LC4:
	.string	"list : elapsed time: "
.LC5:
	.string	"s\n"
.LC6:
	.string	"vector : elapsed time: "
.LC9:
	.string	"arrays : elapsed time: "
	.text
	.globl	main
	.type	main, @function
main:
.LFB2109:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2109
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -24
	movl	%edi, -308(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$.LC1, %esi
	movl	$_ZSt4cerr, %edi
.LEHB0:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	cmpl	$2, -308(%rbp)
	jg	.L26
	movl	$.LC2, %esi
	movl	$_ZSt4cerr, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$1, %edi
	call	exit
.L26:
	movq	-320(%rbp), %rax
	addq	$8, %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	atoi
	movl	%eax, -264(%rbp)
	movq	-320(%rbp), %rax
	addq	$16, %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	atoi
	movl	%eax, -260(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -240(%rbp)
	movl	$.LC3, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdx
	movl	-264(%rbp), %eax
	movl	%eax, %esi
	movq	%rdx, %rdi
	call	_ZNSolsEi
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
.LEHE0:
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC1Ev
	movl	$0, -296(%rbp)
.L28:
	movl	-296(%rbp), %eax
	cmpl	-264(%rbp), %eax
	jge	.L27
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN10Particle1DILi0EEC1Ev
	leaq	-32(%rbp), %rdx
	leaq	-160(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB1:
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_
	addl	$1, -296(%rbp)
	jmp	.L28
.L27:
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, -256(%rbp)
	movl	$0, -292(%rbp)
.L32:
	movl	-292(%rbp), %eax
	cmpl	-260(%rbp), %eax
	jge	.L29
	leaq	-160(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv
	movq	%rax, -96(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv
	movq	%rax, -64(%rbp)
.L31:
	leaq	-64(%rbp), %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_
	testb	%al, %al
	je	.L30
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movss	(%rax), %xmm1
	movq	-168(%rbp), %rax
	movss	4(%rax), %xmm0
	addss	%xmm1, %xmm0
	movq	-168(%rbp), %rax
	movss	%xmm0, (%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorI10Particle1DILi0EEEppEv
	jmp	.L31
.L30:
	addl	$1, -292(%rbp)
	jmp	.L32
.L29:
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, -240(%rbp)
	leaq	-256(%rbp), %rdx
	leaq	-240(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rdx
	leaq	-224(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv
	movsd	%xmm0, -328(%rbp)
	movl	$.LC4, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movsd	-328(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSolsEd
	movl	$.LC5, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LEHE1:
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC1Ev
	movl	$0, -288(%rbp)
.L34:
	movl	-288(%rbp), %eax
	cmpl	-264(%rbp), %eax
	jge	.L33
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN10Particle1DILi0EEC1Ev
	leaq	-32(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB2:
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_
	addl	$1, -288(%rbp)
	jmp	.L34
.L33:
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, -256(%rbp)
	movl	$0, -284(%rbp)
.L38:
	movl	-284(%rbp), %eax
	cmpl	-260(%rbp), %eax
	jge	.L35
	movl	$0, -280(%rbp)
.L37:
	movl	-280(%rbp), %eax
	cmpl	-264(%rbp), %eax
	jge	.L36
	movl	-280(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm
	movss	4(%rax), %xmm2
	movss	%xmm2, -328(%rbp)
	movl	-280(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm
	movss	(%rax), %xmm0
	addss	-328(%rbp), %xmm0
	movss	%xmm0, (%rax)
	addl	$1, -280(%rbp)
	jmp	.L37
.L36:
	addl	$1, -284(%rbp)
	jmp	.L38
.L35:
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, -240(%rbp)
	leaq	-256(%rbp), %rdx
	leaq	-240(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE
	movsd	-64(%rbp), %xmm0
	movsd	%xmm0, -224(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv
	movsd	%xmm0, -328(%rbp)
	movl	$.LC6, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movsd	-328(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSolsEd
	movl	$.LC5, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LEHE2:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIfEC1Ev
	movl	-264(%rbp), %eax
	movslq	%eax, %rcx
	leaq	-64(%rbp), %rdx
	leaq	-96(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB3:
	call	_ZNSt6vectorIfSaIfEEC1EmRKS0_
.LEHE3:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIfED1Ev
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIfEC1Ev
	movl	-264(%rbp), %eax
	movslq	%eax, %rcx
	leaq	-192(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB4:
	call	_ZNSt6vectorIfSaIfEEC1EmRKS0_
.LEHE4:
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIfED1Ev
	movl	$0, -276(%rbp)
.L40:
	movl	-276(%rbp), %eax
	cmpl	-264(%rbp), %eax
	jge	.L39
	movl	-276(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEEixEm
	movss	.LC7(%rip), %xmm0
	movss	%xmm0, (%rax)
	movl	-276(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-64(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEEixEm
	movss	.LC8(%rip), %xmm0
	movss	%xmm0, (%rax)
	addl	$1, -276(%rbp)
	jmp	.L40
.L39:
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, -256(%rbp)
	movl	$0, -272(%rbp)
.L44:
	movl	-272(%rbp), %eax
	cmpl	-260(%rbp), %eax
	jge	.L41
	movl	$0, -268(%rbp)
.L43:
	movl	-268(%rbp), %eax
	cmpl	-264(%rbp), %eax
	jge	.L42
	movl	-268(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-64(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEEixEm
	movss	(%rax), %xmm3
	movss	%xmm3, -328(%rbp)
	movl	-268(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEEixEm
	movss	(%rax), %xmm0
	addss	-328(%rbp), %xmm0
	movss	%xmm0, (%rax)
	addl	$1, -268(%rbp)
	jmp	.L43
.L42:
	addl	$1, -272(%rbp)
	jmp	.L44
.L41:
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, -240(%rbp)
	leaq	-256(%rbp), %rdx
	leaq	-240(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chronomiINS_3_V212system_clockENS_8durationIlSt5ratioILl1ELl1000000000EEEES6_EENSt11common_typeIJT0_T1_EE4typeERKNS_10time_pointIT_S8_EERKNSC_ISD_S9_EE
	movq	%rax, -208(%rbp)
	leaq	-208(%rbp), %rdx
	leaq	-192(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6chrono8durationIdSt5ratioILl1ELl1EEEC1IlS1_ILl1ELl1000000000EEvEERKNS0_IT_T0_EE
	movsd	-192(%rbp), %xmm0
	movsd	%xmm0, -224(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6chrono8durationIdSt5ratioILl1ELl1EEE5countEv
	movsd	%xmm0, -328(%rbp)
	movl	$.LC9, %esi
	movl	$_ZSt4cout, %edi
.LEHB5:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movsd	-328(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSolsEd
	movl	$.LC5, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LEHE5:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEED1Ev
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEED1Ev
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EED1Ev
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED1Ev
	movl	$0, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L52
	jmp	.L58
.L55:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIfED1Ev
	jmp	.L47
.L56:
	movq	%rax, %rbx
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIfED1Ev
	jmp	.L49
.L57:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEED1Ev
.L49:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6vectorIfSaIfEED1Ev
	jmp	.L47
.L54:
	movq	%rax, %rbx
.L47:
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EED1Ev
	jmp	.L51
.L53:
	movq	%rax, %rbx
.L51:
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB6:
	call	_Unwind_Resume
.LEHE6:
.L58:
	call	__stack_chk_fail
.L52:
	addq	$328, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2109:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA2109:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2109-.LLSDACSB2109
.LLSDACSB2109:
	.uleb128 .LEHB0-.LFB2109
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB2109
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L53-.LFB2109
	.uleb128 0
	.uleb128 .LEHB2-.LFB2109
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L54-.LFB2109
	.uleb128 0
	.uleb128 .LEHB3-.LFB2109
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L55-.LFB2109
	.uleb128 0
	.uleb128 .LEHB4-.LFB2109
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L56-.LFB2109
	.uleb128 0
	.uleb128 .LEHB5-.LFB2109
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L57-.LFB2109
	.uleb128 0
	.uleb128 .LEHB6-.LFB2109
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
.LLSDACSE2109:
	.text
	.size	main, .-main
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev
	.type	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev, @function
_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev:
.LFB2189:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2189
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2189:
	.section	.gcc_except_table
.LLSDA2189:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2189-.LLSDACSB2189
.LLSDACSB2189:
.LLSDACSE2189:
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC5Ev,comdat
	.size	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev, .-_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC1Ev
	.set	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC1Ev,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EEC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD2Ev
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD2Ev, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD2Ev:
.LFB2193:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2193:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD2Ev, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD2Ev
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD1Ev
	.set	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD1Ev,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD2Ev
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev:
.LFB2195:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implD1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2195:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED1Ev
	.set	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED1Ev,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EED2Ev
	.section	.text._ZN10Particle1DILi0EEC2Ev,"axG",@progbits,_ZN10Particle1DILi0EEC5Ev,comdat
	.align 2
	.weak	_ZN10Particle1DILi0EEC2Ev
	.type	_ZN10Particle1DILi0EEC2Ev, @function
_ZN10Particle1DILi0EEC2Ev:
.LFB2198:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movss	.LC7(%rip), %xmm0
	movss	%xmm0, (%rax)
	movq	-8(%rbp), %rax
	movss	.LC8(%rip), %xmm0
	movss	%xmm0, 4(%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2198:
	.size	_ZN10Particle1DILi0EEC2Ev, .-_ZN10Particle1DILi0EEC2Ev
	.weak	_ZN10Particle1DILi0EEC1Ev
	.set	_ZN10Particle1DILi0EEC1Ev,_ZN10Particle1DILi0EEC2Ev
	.section	.text._ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_,"axG",@progbits,_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_,comdat
	.weak	_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_
	.type	_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_, @function
_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_:
.LFB2201:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2201:
	.size	_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_, .-_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_
	.type	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_, @function
_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_:
.LFB2200:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv
	movq	%rax, %rcx
	movq	-24(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_
	nop
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2200:
	.size	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_, .-_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9push_backEOS2_
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv
	.type	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv, @function
_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv:
.LFB2202:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	(%rax), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorI10Particle1DILi0EEEC1EPNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L68
	call	__stack_chk_fail
.L68:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2202:
	.size	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv, .-_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE5beginEv
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv,comdat
	.align 2
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv
	.type	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv, @function
_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv:
.LFB2203:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14_List_iteratorI10Particle1DILi0EEEC1EPNSt8__detail15_List_node_baseE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L71
	call	__stack_chk_fail
.L71:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2203:
	.size	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv, .-_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE3endEv
	.section	.text._ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_,"axG",@progbits,_ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_,comdat
	.align 2
	.weak	_ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_
	.type	_ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_, @function
_ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_:
.LFB2204:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2204:
	.size	_ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_, .-_ZNKSt14_List_iteratorI10Particle1DILi0EEEneERKS2_
	.section	.text._ZNSt14_List_iteratorI10Particle1DILi0EEEppEv,"axG",@progbits,_ZNSt14_List_iteratorI10Particle1DILi0EEEppEv,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorI10Particle1DILi0EEEppEv
	.type	_ZNSt14_List_iteratorI10Particle1DILi0EEEppEv, @function
_ZNSt14_List_iteratorI10Particle1DILi0EEEppEv:
.LFB2205:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2205:
	.size	_ZNSt14_List_iteratorI10Particle1DILi0EEEppEv, .-_ZNSt14_List_iteratorI10Particle1DILi0EEEppEv
	.section	.text._ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv,"axG",@progbits,_ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv,comdat
	.align 2
	.weak	_ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv
	.type	_ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv, @function
_ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv:
.LFB2206:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	addq	$16, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2206:
	.size	_ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv, .-_ZNKSt14_List_iteratorI10Particle1DILi0EEEdeEv
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev
	.type	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev, @function
_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev:
.LFB2209:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2209
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2209:
	.section	.gcc_except_table
.LLSDA2209:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2209-.LLSDACSB2209
.LLSDACSB2209:
.LLSDACSE2209:
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC5Ev,comdat
	.size	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev, .-_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC1Ev
	.set	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC1Ev,_ZNSt6vectorI10Particle1DILi0EESaIS1_EEC2Ev
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EED5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev
	.type	_ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev, @function
_ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev:
.LFB2212:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2212
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2212:
	.section	.gcc_except_table
.LLSDA2212:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2212-.LLSDACSB2212
.LLSDACSB2212:
.LLSDACSE2212:
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EED5Ev,comdat
	.size	_ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev, .-_ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EED1Ev
	.set	_ZNSt6vectorI10Particle1DILi0EESaIS1_EED1Ev,_ZNSt6vectorI10Particle1DILi0EESaIS1_EED2Ev
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_,comdat
	.align 2
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_
	.type	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_, @function
_ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_:
.LFB2214:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt4moveIR10Particle1DILi0EEEONSt16remove_referenceIT_E4typeEOS4_
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2214:
	.size	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_, .-_ZNSt6vectorI10Particle1DILi0EESaIS1_EE9push_backEOS1_
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm,comdat
	.align 2
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm
	.type	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm, @function
_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm:
.LFB2215:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	-16(%rbp), %rdx
	salq	$3, %rdx
	addq	%rdx, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2215:
	.size	_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm, .-_ZNSt6vectorI10Particle1DILi0EESaIS1_EEixEm
	.section	.text._ZNSaIfEC2Ev,"axG",@progbits,_ZNSaIfEC5Ev,comdat
	.align 2
	.weak	_ZNSaIfEC2Ev
	.type	_ZNSaIfEC2Ev, @function
_ZNSaIfEC2Ev:
.LFB2217:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorIfEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2217:
	.size	_ZNSaIfEC2Ev, .-_ZNSaIfEC2Ev
	.weak	_ZNSaIfEC1Ev
	.set	_ZNSaIfEC1Ev,_ZNSaIfEC2Ev
	.section	.text._ZNSaIfED2Ev,"axG",@progbits,_ZNSaIfED5Ev,comdat
	.align 2
	.weak	_ZNSaIfED2Ev
	.type	_ZNSaIfED2Ev, @function
_ZNSaIfED2Ev:
.LFB2220:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorIfED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2220:
	.size	_ZNSaIfED2Ev, .-_ZNSaIfED2Ev
	.weak	_ZNSaIfED1Ev
	.set	_ZNSaIfED1Ev,_ZNSaIfED2Ev
	.section	.text._ZNSt6vectorIfSaIfEEC2EmRKS0_,"axG",@progbits,_ZNSt6vectorIfSaIfEEC5EmRKS0_,comdat
	.align 2
	.weak	_ZNSt6vectorIfSaIfEEC2EmRKS0_
	.type	_ZNSt6vectorIfSaIfEEC2EmRKS0_, @function
_ZNSt6vectorIfSaIfEEC2EmRKS0_:
.LFB2223:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2223
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-24(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	-32(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB7:
	call	_ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_
.LEHE7:
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB8:
	call	_ZNSt6vectorIfSaIfEE21_M_default_initializeEm
.LEHE8:
	jmp	.L88
.L87:
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEED2Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB9:
	call	_Unwind_Resume
.LEHE9:
.L88:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2223:
	.section	.gcc_except_table
.LLSDA2223:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2223-.LLSDACSB2223
.LLSDACSB2223:
	.uleb128 .LEHB7-.LFB2223
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB8-.LFB2223
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L87-.LFB2223
	.uleb128 0
	.uleb128 .LEHB9-.LFB2223
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
.LLSDACSE2223:
	.section	.text._ZNSt6vectorIfSaIfEEC2EmRKS0_,"axG",@progbits,_ZNSt6vectorIfSaIfEEC5EmRKS0_,comdat
	.size	_ZNSt6vectorIfSaIfEEC2EmRKS0_, .-_ZNSt6vectorIfSaIfEEC2EmRKS0_
	.weak	_ZNSt6vectorIfSaIfEEC1EmRKS0_
	.set	_ZNSt6vectorIfSaIfEEC1EmRKS0_,_ZNSt6vectorIfSaIfEEC2EmRKS0_
	.section	.text._ZNSt6vectorIfSaIfEED2Ev,"axG",@progbits,_ZNSt6vectorIfSaIfEED5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorIfSaIfEED2Ev
	.type	_ZNSt6vectorIfSaIfEED2Ev, @function
_ZNSt6vectorIfSaIfEED2Ev:
.LFB2226:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2226
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIPffEvT_S1_RSaIT0_E
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2226:
	.section	.gcc_except_table
.LLSDA2226:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2226-.LLSDACSB2226
.LLSDACSB2226:
.LLSDACSE2226:
	.section	.text._ZNSt6vectorIfSaIfEED2Ev,"axG",@progbits,_ZNSt6vectorIfSaIfEED5Ev,comdat
	.size	_ZNSt6vectorIfSaIfEED2Ev, .-_ZNSt6vectorIfSaIfEED2Ev
	.weak	_ZNSt6vectorIfSaIfEED1Ev
	.set	_ZNSt6vectorIfSaIfEED1Ev,_ZNSt6vectorIfSaIfEED2Ev
	.section	.text._ZNSt6vectorIfSaIfEEixEm,"axG",@progbits,_ZNSt6vectorIfSaIfEEixEm,comdat
	.align 2
	.weak	_ZNSt6vectorIfSaIfEEixEm
	.type	_ZNSt6vectorIfSaIfEEixEm, @function
_ZNSt6vectorIfSaIfEEixEm:
.LFB2228:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	-16(%rbp), %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2228:
	.size	_ZNSt6vectorIfSaIfEEixEm, .-_ZNSt6vectorIfSaIfEEixEm
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev:
.LFB2288:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC1Ev
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2288:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC1Ev
	.set	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC1Ev,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EEC2Ev
	.section	.text._ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev,"axG",@progbits,_ZNSaISt10_List_nodeI10Particle1DILi0EEEED5Ev,comdat
	.align 2
	.weak	_ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev
	.type	_ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev, @function
_ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev:
.LFB2291:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2291:
	.size	_ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev, .-_ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev
	.weak	_ZNSaISt10_List_nodeI10Particle1DILi0EEEED1Ev
	.set	_ZNSaISt10_List_nodeI10Particle1DILi0EEEED1Ev,_ZNSaISt10_List_nodeI10Particle1DILi0EEEED2Ev
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv:
.LFB2293:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
.L96:
	movq	-24(%rbp), %rax
	cmpq	-16(%rbp), %rax
	je	.L97
	movq	-16(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_
	movq	-8(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E
	jmp	.L96
.L97:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2293:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE8_M_clearEv
	.section	.text._ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE,"axG",@progbits,_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE,comdat
	.weak	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	.type	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE, @function
_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE:
.LFB2295:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2295:
	.size	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE, .-_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_
	.type	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_, @function
_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_:
.LFB2294:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_
	movq	%rax, -8(%rbp)
	movq	-32(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	movq	-24(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2294:
	.size	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_, .-_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIIS2_EEEvSt14_List_iteratorIS2_EDpOT_
	.set	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIIS2_EEEvSt14_List_iteratorIS2_EDpOT_,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE9_M_insertIJS2_EEEvSt14_List_iteratorIS2_EDpOT_
	.section	.text._ZNSt14_List_iteratorI10Particle1DILi0EEEC2EPNSt8__detail15_List_node_baseE,"axG",@progbits,_ZNSt14_List_iteratorI10Particle1DILi0EEEC5EPNSt8__detail15_List_node_baseE,comdat
	.align 2
	.weak	_ZNSt14_List_iteratorI10Particle1DILi0EEEC2EPNSt8__detail15_List_node_baseE
	.type	_ZNSt14_List_iteratorI10Particle1DILi0EEEC2EPNSt8__detail15_List_node_baseE, @function
_ZNSt14_List_iteratorI10Particle1DILi0EEEC2EPNSt8__detail15_List_node_baseE:
.LFB2297:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2297:
	.size	_ZNSt14_List_iteratorI10Particle1DILi0EEEC2EPNSt8__detail15_List_node_baseE, .-_ZNSt14_List_iteratorI10Particle1DILi0EEEC2EPNSt8__detail15_List_node_baseE
	.weak	_ZNSt14_List_iteratorI10Particle1DILi0EEEC1EPNSt8__detail15_List_node_baseE
	.set	_ZNSt14_List_iteratorI10Particle1DILi0EEEC1EPNSt8__detail15_List_node_baseE,_ZNSt14_List_iteratorI10Particle1DILi0EEEC2EPNSt8__detail15_List_node_baseE
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD2Ev:
.LFB2301:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaI10Particle1DILi0EEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2301:
	.size	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD1Ev,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev
	.type	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev, @function
_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev:
.LFB2303:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2303:
	.size	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev, .-_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC1Ev
	.set	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC1Ev,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EEC2Ev
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev
	.type	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev, @function
_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev:
.LFB2306:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2306
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implD1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2306:
	.section	.gcc_except_table
.LLSDA2306:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2306-.LLSDACSB2306
.LLSDACSB2306:
.LLSDACSE2306:
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev, .-_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED1Ev
	.set	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED1Ev,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EED2Ev
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv:
.LFB2308:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2308:
	.size	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E,comdat
	.weak	_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E
	.type	_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E, @function
_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E:
.LFB2309:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2309:
	.size	_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E, .-_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_,comdat
	.align 2
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	.type	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_, @function
_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_:
.LFB2310:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	16(%rax), %rax
	cmpq	%rax, %rdx
	je	.L109
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	leaq	8(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	jmp	.L111
.L109:
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_
.L111:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2310:
	.size	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_, .-_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIIS1_EEEvDpOT_
	.set	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIIS1_EEEvDpOT_,_ZNSt6vectorI10Particle1DILi0EESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorIfEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorIfEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorIfEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorIfEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorIfEC2Ev:
.LFB2312:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2312:
	.size	_ZN9__gnu_cxx13new_allocatorIfEC2Ev, .-_ZN9__gnu_cxx13new_allocatorIfEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorIfEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorIfEC1Ev,_ZN9__gnu_cxx13new_allocatorIfEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorIfED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorIfED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorIfED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorIfED2Ev, @function
_ZN9__gnu_cxx13new_allocatorIfED2Ev:
.LFB2315:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2315:
	.size	_ZN9__gnu_cxx13new_allocatorIfED2Ev, .-_ZN9__gnu_cxx13new_allocatorIfED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorIfED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorIfED1Ev,_ZN9__gnu_cxx13new_allocatorIfED2Ev
	.section	.text._ZNSt12_Vector_baseIfSaIfEE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD2Ev:
.LFB2319:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIfED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2319:
	.size	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD1Ev,_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEEC5EmRKS0_,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_
	.type	_ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_, @function
_ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_:
.LFB2321:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2321
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-24(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC1ERKS0_
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB10:
	call	_ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm
.LEHE10:
	jmp	.L118
.L117:
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB11:
	call	_Unwind_Resume
.LEHE11:
.L118:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2321:
	.section	.gcc_except_table
.LLSDA2321:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2321-.LLSDACSB2321
.LLSDACSB2321:
	.uleb128 .LEHB10-.LFB2321
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L117-.LFB2321
	.uleb128 0
	.uleb128 .LEHB11-.LFB2321
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0
	.uleb128 0
.LLSDACSE2321:
	.section	.text._ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEEC5EmRKS0_,comdat
	.size	_ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_, .-_ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_
	.weak	_ZNSt12_Vector_baseIfSaIfEEC1EmRKS0_
	.set	_ZNSt12_Vector_baseIfSaIfEEC1EmRKS0_,_ZNSt12_Vector_baseIfSaIfEEC2EmRKS0_
	.section	.text._ZNSt12_Vector_baseIfSaIfEED2Ev,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEED5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEED2Ev
	.type	_ZNSt12_Vector_baseIfSaIfEED2Ev, @function
_ZNSt12_Vector_baseIfSaIfEED2Ev:
.LFB2324:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2324
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implD1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2324:
	.section	.gcc_except_table
.LLSDA2324:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2324-.LLSDACSB2324
.LLSDACSB2324:
.LLSDACSE2324:
	.section	.text._ZNSt12_Vector_baseIfSaIfEED2Ev,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEED5Ev,comdat
	.size	_ZNSt12_Vector_baseIfSaIfEED2Ev, .-_ZNSt12_Vector_baseIfSaIfEED2Ev
	.weak	_ZNSt12_Vector_baseIfSaIfEED1Ev
	.set	_ZNSt12_Vector_baseIfSaIfEED1Ev,_ZNSt12_Vector_baseIfSaIfEED2Ev
	.section	.text._ZNSt6vectorIfSaIfEE21_M_default_initializeEm,"axG",@progbits,_ZNSt6vectorIfSaIfEE21_M_default_initializeEm,comdat
	.align 2
	.weak	_ZNSt6vectorIfSaIfEE21_M_default_initializeEm
	.type	_ZNSt6vectorIfSaIfEE21_M_default_initializeEm, @function
_ZNSt6vectorIfSaIfEE21_M_default_initializeEm:
.LFB2326:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2326:
	.size	_ZNSt6vectorIfSaIfEE21_M_default_initializeEm, .-_ZNSt6vectorIfSaIfEE21_M_default_initializeEm
	.section	.text._ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv:
.LFB2327:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2327:
	.size	_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseIfSaIfEE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPffEvT_S1_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPffEvT_S1_RSaIT0_E,comdat
	.weak	_ZSt8_DestroyIPffEvT_S1_RSaIT0_E
	.type	_ZSt8_DestroyIPffEvT_S1_RSaIT0_E, @function
_ZSt8_DestroyIPffEvT_S1_RSaIT0_E:
.LFB2328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIPfEvT_S1_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2328:
	.size	_ZSt8_DestroyIPffEvT_S1_RSaIT0_E, .-_ZSt8_DestroyIPffEvT_S1_RSaIT0_E
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC2Ev,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC5Ev,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC2Ev
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC2Ev, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC2Ev:
.LFB2362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeImEC1IJEEEDpOT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2362:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC2Ev, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC2Ev
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC1Ev
	.set	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC1Ev,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE10_List_implC2Ev
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv:
.LFB2364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	-8(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2364:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE7_M_initEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev:
.LFB2366:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2366:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEED2Ev
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv:
.LFB2368:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2368:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_:
.LFB2369:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2369:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE7destroyIS4_EEvPT_
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E:
.LFB2370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movl	$1, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2370:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_,comdat
	.align 2
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_
	.type	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_, @function
_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_:
.LFB2371:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2371
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
.LEHB12:
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv
.LEHE12:
	movq	%rax, -24(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rbx
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE21_M_get_Node_allocatorEv
	movq	%rax, %rcx
	movq	-24(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%rcx, %rdi
.LEHB13:
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_
.LEHE13:
	movq	-24(%rbp), %rax
	jmp	.L137
.L135:
	movq	%rax, %rdi
	call	__cxa_begin_catch
	movq	-40(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_put_nodeEPSt10_List_nodeIS2_E
.LEHB14:
	call	__cxa_rethrow
.LEHE14:
.L136:
	movq	%rax, %rbx
	call	__cxa_end_catch
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB15:
	call	_Unwind_Resume
.LEHE15:
.L137:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2371:
	.section	.gcc_except_table
	.align 4
.LLSDA2371:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT2371-.LLSDATTD2371
.LLSDATTD2371:
	.byte	0x1
	.uleb128 .LLSDACSE2371-.LLSDACSB2371
.LLSDACSB2371:
	.uleb128 .LEHB12-.LFB2371
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB13-.LFB2371
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L135-.LFB2371
	.uleb128 0x1
	.uleb128 .LEHB14-.LFB2371
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L136-.LFB2371
	.uleb128 0
	.uleb128 .LEHB15-.LFB2371
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE2371:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT2371:
	.section	.text._ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_,"axG",@progbits,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_,comdat
	.size	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_, .-_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_
	.weak	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIIS2_EEEPSt10_List_nodeIS2_EDpOT_
	.set	_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIIS2_EEEPSt10_List_nodeIS2_EDpOT_,_ZNSt7__cxx114listI10Particle1DILi0EESaIS2_EE14_M_create_nodeIJS2_EEEPSt10_List_nodeIS2_EDpOT_
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm:
.LFB2372:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	16(%rax), %rdx
	movq	-16(%rbp), %rax
	addq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 16(%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2372:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_inc_sizeEm
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC2Ev,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC2Ev
	.type	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC2Ev, @function
_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC2Ev:
.LFB2374:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaI10Particle1DILi0EEEC2Ev
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-8(%rbp), %rax
	movq	$0, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2374:
	.size	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC2Ev, .-_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC2Ev
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC1Ev
	.set	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC1Ev,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE12_Vector_implC2Ev
	.section	.text._ZNSaI10Particle1DILi0EEED2Ev,"axG",@progbits,_ZNSaI10Particle1DILi0EEED5Ev,comdat
	.align 2
	.weak	_ZNSaI10Particle1DILi0EEED2Ev
	.type	_ZNSaI10Particle1DILi0EEED2Ev, @function
_ZNSaI10Particle1DILi0EEED2Ev:
.LFB2377:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2377:
	.size	_ZNSaI10Particle1DILi0EEED2Ev, .-_ZNSaI10Particle1DILi0EEED2Ev
	.weak	_ZNSaI10Particle1DILi0EEED1Ev
	.set	_ZNSaI10Particle1DILi0EEED1Ev,_ZNSaI10Particle1DILi0EEED2Ev
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m
	.type	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m, @function
_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m:
.LFB2379:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L143
	movq	-8(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m
.L143:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2379:
	.size	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m, .-_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m
	.section	.text._ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_,"axG",@progbits,_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_,comdat
	.weak	_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_
	.type	_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_, @function
_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_:
.LFB2380:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2380:
	.size	_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_, .-_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_
	.section	.text._ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_,comdat
	.weak	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_:
.LFB2381:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2381:
	.size	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_
	.weak	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_IS1_EEEvRS2_PT_DpOT0_
	.set	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_IS1_EEEvRS2_PT_DpOT0_,_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_
	.section	.rodata
.LC10:
	.string	"vector::_M_emplace_back_aux"
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_,comdat
	.align 2
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_
	.type	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_, @function
_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_:
.LFB2382:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2382
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-56(%rbp), %rax
	movl	$.LC10, %edx
	movl	$1, %esi
	movq	%rax, %rdi
.LEHB16:
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc
	movq	%rax, -32(%rbp)
	movq	-56(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm
.LEHE16:
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	leaq	0(,%rax,8), %rdx
	movq	-24(%rbp), %rax
	leaq	(%rdx,%rax), %rcx
	movq	-56(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE9constructIS1_JS1_EEEvRS2_PT_DpOT0_
	movq	$0, -40(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movq	8(%rax), %rsi
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rdx
	movq	%rax, %rdi
.LEHB17:
	call	_ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_
.LEHE17:
	movq	%rax, -40(%rbp)
	addq	$8, -40(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB18:
	call	_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E
	movq	-56(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	-56(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m
.LEHE18:
	movq	-56(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-32(%rbp), %rax
	leaq	0(,%rax,8), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	%rdx, 16(%rax)
	jmp	.L153
.L151:
	movq	%rax, %rdi
	call	__cxa_begin_catch
	cmpq	$0, -40(%rbp)
	jne	.L148
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	leaq	0(,%rax,8), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB19:
	call	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_
	jmp	.L149
.L148:
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-40(%rbp), %rcx
	movq	-24(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP10Particle1DILi0EES1_EvT_S3_RSaIT0_E
.L149:
	movq	-56(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE13_M_deallocateEPS1_m
	call	__cxa_rethrow
.LEHE19:
.L152:
	movq	%rax, %rbx
	call	__cxa_end_catch
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB20:
	call	_Unwind_Resume
.LEHE20:
.L153:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2382:
	.section	.gcc_except_table
	.align 4
.LLSDA2382:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT2382-.LLSDATTD2382
.LLSDATTD2382:
	.byte	0x1
	.uleb128 .LLSDACSE2382-.LLSDACSB2382
.LLSDACSB2382:
	.uleb128 .LEHB16-.LFB2382
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB17-.LFB2382
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L151-.LFB2382
	.uleb128 0x1
	.uleb128 .LEHB18-.LFB2382
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB19-.LFB2382
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L152-.LFB2382
	.uleb128 0
	.uleb128 .LEHB20-.LFB2382
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
.LLSDACSE2382:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT2382:
	.section	.text._ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_,comdat
	.size	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_, .-_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_
	.weak	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIIS1_EEEvDpOT_
	.set	_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIIS1_EEEvDpOT_,_ZNSt6vectorI10Particle1DILi0EESaIS1_EE19_M_emplace_back_auxIJS1_EEEvDpOT_
	.section	.text._ZNSt12_Vector_baseIfSaIfEE12_Vector_implC2ERKS0_,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC5ERKS0_,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC2ERKS0_
	.type	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC2ERKS0_, @function
_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC2ERKS0_:
.LFB2384:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSaIfEC2ERKS_
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-8(%rbp), %rax
	movq	$0, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2384:
	.size	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC2ERKS0_, .-_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC2ERKS0_
	.weak	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC1ERKS0_
	.set	_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC1ERKS0_,_ZNSt12_Vector_baseIfSaIfEE12_Vector_implC2ERKS0_
	.section	.text._ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm
	.type	_ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm, @function
_ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm:
.LFB2386:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	-16(%rbp), %rdx
	salq	$2, %rdx
	addq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2386:
	.size	_ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm, .-_ZNSt12_Vector_baseIfSaIfEE17_M_create_storageEm
	.section	.text._ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm
	.type	_ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm, @function
_ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm:
.LFB2387:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L158
	movq	-8(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm
.L158:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2387:
	.size	_ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm, .-_ZNSt12_Vector_baseIfSaIfEE13_M_deallocateEPfm
	.section	.text._ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E,"axG",@progbits,_ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E,comdat
	.weak	_ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E
	.type	_ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E, @function
_ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E:
.LFB2388:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt25__uninitialized_default_nIPfmET_S1_T0_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2388:
	.size	_ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E, .-_ZSt27__uninitialized_default_n_aIPfmfET_S1_T0_RSaIT1_E
	.section	.text._ZSt8_DestroyIPfEvT_S1_,"axG",@progbits,_ZSt8_DestroyIPfEvT_S1_,comdat
	.weak	_ZSt8_DestroyIPfEvT_S1_
	.type	_ZSt8_DestroyIPfEvT_S1_, @function
_ZSt8_DestroyIPfEvT_S1_:
.LFB2389:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2389:
	.size	_ZSt8_DestroyIPfEvT_S1_, .-_ZSt8_DestroyIPfEvT_S1_
	.section	.text._ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev,"axG",@progbits,_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC5Ev,comdat
	.align 2
	.weak	_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	.type	_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev, @function
_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev:
.LFB2408:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2408:
	.size	_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev, .-_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	.weak	_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC1Ev
	.set	_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC1Ev,_ZNSaISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	.section	.text._ZNSt10_List_nodeImEC2IJEEEDpOT_,"axG",@progbits,_ZNSt10_List_nodeImEC5IJEEEDpOT_,comdat
	.align 2
	.weak	_ZNSt10_List_nodeImEC2IJEEEDpOT_
	.type	_ZNSt10_List_nodeImEC2IJEEEDpOT_, @function
_ZNSt10_List_nodeImEC2IJEEEDpOT_:
.LFB2411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-8(%rbp), %rax
	movq	$0, 16(%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2411:
	.size	_ZNSt10_List_nodeImEC2IJEEEDpOT_, .-_ZNSt10_List_nodeImEC2IJEEEDpOT_
	.weak	_ZNSt10_List_nodeImEC2IIEEEDpOT_
	.set	_ZNSt10_List_nodeImEC2IIEEEDpOT_,_ZNSt10_List_nodeImEC2IJEEEDpOT_
	.weak	_ZNSt10_List_nodeImEC1IJEEEDpOT_
	.set	_ZNSt10_List_nodeImEC1IJEEEDpOT_,_ZNSt10_List_nodeImEC2IJEEEDpOT_
	.weak	_ZNSt10_List_nodeImEC1IIEEEDpOT_
	.set	_ZNSt10_List_nodeImEC1IIEEEDpOT_,_ZNSt10_List_nodeImEC1IJEEEDpOT_
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm:
.LFB2413:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2413:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_set_sizeEm
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m:
.LFB2414:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2414:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE10deallocateEPS4_m
	.section	.text._ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv,"axG",@progbits,_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv,comdat
	.align 2
	.weak	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv
	.type	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv, @function
_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv:
.LFB2415:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$0, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2415:
	.size	_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv, .-_ZNSt7__cxx1110_List_baseI10Particle1DILi0EESaIS2_EE11_M_get_nodeEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_:
.LFB2416:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rbx
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	movl	$24, %edi
	call	_ZnwmPv
	testq	%rax, %rax
	je	.L171
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZNSt10_List_nodeI10Particle1DILi0EEEC1IJS1_EEEDpOT_
.L171:
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2416:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_IS3_EEEvPT_DpOT0_
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_IS3_EEEvPT_DpOT0_,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE9constructIS4_JS3_EEEvPT_DpOT0_
	.section	.text._ZNSaI10Particle1DILi0EEEC2Ev,"axG",@progbits,_ZNSaI10Particle1DILi0EEEC5Ev,comdat
	.align 2
	.weak	_ZNSaI10Particle1DILi0EEEC2Ev
	.type	_ZNSaI10Particle1DILi0EEEC2Ev, @function
_ZNSaI10Particle1DILi0EEEC2Ev:
.LFB2418:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2418:
	.size	_ZNSaI10Particle1DILi0EEEC2Ev, .-_ZNSaI10Particle1DILi0EEEC2Ev
	.weak	_ZNSaI10Particle1DILi0EEEC1Ev
	.set	_ZNSaI10Particle1DILi0EEEC1Ev,_ZNSaI10Particle1DILi0EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev:
.LFB2421:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2421:
	.size	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev, .-_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED1Ev,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m,"axG",@progbits,_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m,comdat
	.weak	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m
	.type	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m, @function
_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m:
.LFB2423:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2423:
	.size	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m, .-_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE10deallocateERS2_PS1_m
	.section	.text._ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_,"axG",@progbits,_ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_,comdat
	.weak	_ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_
	.type	_ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_, @function
_ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_:
.LFB2424:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2424:
	.size	_ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_, .-_ZNSt12_Destroy_auxILb1EE9__destroyIP10Particle1DILi0EEEEvT_S5_
	.section	.text._ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_:
.LFB2425:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rbx
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	movl	$8, %edi
	call	_ZnwmPv
	testq	%rax, %rax
	je	.L179
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
.L179:
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2425:
	.size	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_IS2_EEEvPT_DpOT0_
	.set	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_IS2_EEEvPT_DpOT0_,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE9constructIS2_JS2_EEEvPT_DpOT0_
	.section	.text._ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc,"axG",@progbits,_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc,comdat
	.align 2
	.weak	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc
	.type	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc, @function
_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc:
.LFB2426:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	subq	%rax, %rbx
	movq	%rbx, %rdx
	movq	-64(%rbp), %rax
	cmpq	%rax, %rdx
	setb	%al
	testb	%al, %al
	je	.L181
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt20__throw_length_errorPKc
.L181:
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	movq	%rax, -40(%rbp)
	leaq	-64(%rbp), %rdx
	leaq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt3maxImERKT_S2_S2_
	movq	(%rax), %rax
	addq	%rbx, %rax
	movq	%rax, -32(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	cmpq	-32(%rbp), %rax
	ja	.L182
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv
	cmpq	-32(%rbp), %rax
	jnb	.L183
.L182:
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv
	jmp	.L185
.L183:
	movq	-32(%rbp), %rax
.L185:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L186
	call	__stack_chk_fail
.L186:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2426:
	.size	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc, .-_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE12_M_check_lenEmPKc
	.section	.text._ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm,"axG",@progbits,_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm
	.type	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm, @function
_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm:
.LFB2427:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L188
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m
	jmp	.L190
.L188:
	movl	$0, %eax
.L190:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2427:
	.size	_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm, .-_ZNSt12_Vector_baseI10Particle1DILi0EESaIS1_EE11_M_allocateEm
	.section	.text._ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv,"axG",@progbits,_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv,comdat
	.align 2
	.weak	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	.type	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv, @function
_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv:
.LFB2428:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2428:
	.size	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv, .-_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE4sizeEv
	.section	.text._ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_,"axG",@progbits,_ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_,comdat
	.weak	_ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_
	.type	_ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_, @function
_ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_:
.LFB2429:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_
	movq	%rax, %rdi
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, %rcx
	movq	%rax, %rdx
	movq	%rbx, %rsi
	call	_ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2429:
	.size	_ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_, .-_ZSt34__uninitialized_move_if_noexcept_aIP10Particle1DILi0EES2_SaIS1_EET0_T_S5_S4_RT1_
	.section	.text._ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_,comdat
	.weak	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_
	.type	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_, @function
_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_:
.LFB2430:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2430:
	.size	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_, .-_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE7destroyIS1_EEvRS2_PT_
	.section	.text._ZNSaIfEC2ERKS_,"axG",@progbits,_ZNSaIfEC5ERKS_,comdat
	.align 2
	.weak	_ZNSaIfEC2ERKS_
	.type	_ZNSaIfEC2ERKS_, @function
_ZNSaIfEC2ERKS_:
.LFB2432:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2432:
	.size	_ZNSaIfEC2ERKS_, .-_ZNSaIfEC2ERKS_
	.weak	_ZNSaIfEC1ERKS_
	.set	_ZNSaIfEC1ERKS_,_ZNSaIfEC2ERKS_
	.section	.text._ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm,"axG",@progbits,_ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm
	.type	_ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm, @function
_ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm:
.LFB2434:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L198
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaIfEE8allocateERS0_m
	jmp	.L200
.L198:
	movl	$0, %eax
.L200:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2434:
	.size	_ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm, .-_ZNSt12_Vector_baseIfSaIfEE11_M_allocateEm
	.section	.text._ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm,"axG",@progbits,_ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm,comdat
	.weak	_ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm
	.type	_ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm, @function
_ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm:
.LFB2435:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2435:
	.size	_ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm, .-_ZNSt16allocator_traitsISaIfEE10deallocateERS0_Pfm
	.section	.text._ZSt25__uninitialized_default_nIPfmET_S1_T0_,"axG",@progbits,_ZSt25__uninitialized_default_nIPfmET_S1_T0_,comdat
	.weak	_ZSt25__uninitialized_default_nIPfmET_S1_T0_
	.type	_ZSt25__uninitialized_default_nIPfmET_S1_T0_, @function
_ZSt25__uninitialized_default_nIPfmET_S1_T0_:
.LFB2436:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movb	$1, -1(%rbp)
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2436:
	.size	_ZSt25__uninitialized_default_nIPfmET_S1_T0_, .-_ZSt25__uninitialized_default_nIPfmET_S1_T0_
	.section	.text._ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_,"axG",@progbits,_ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_,comdat
	.weak	_ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_
	.type	_ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_, @function
_ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_:
.LFB2437:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2437:
	.size	_ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_, .-_ZNSt12_Destroy_auxILb1EE9__destroyIPfEEvT_S3_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev:
.LFB2448:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2448:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv, @function
_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv:
.LFB2450:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv
	cmpq	-16(%rbp), %rax
	setb	%al
	testb	%al, %al
	je	.L207
	call	_ZSt17__throw_bad_allocv
.L207:
	movq	-16(%rbp), %rdx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, %rdi
	call	_Znwm
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2450:
	.size	_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8allocateEmPKv
	.section	.text._ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_,"axG",@progbits,_ZNSt10_List_nodeI10Particle1DILi0EEEC5IJS1_EEEDpOT_,comdat
	.align 2
	.weak	_ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_
	.type	_ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_, @function
_ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_:
.LFB2452:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rdx), %rdx
	movq	%rdx, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2452:
	.size	_ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_, .-_ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_
	.weak	_ZNSt10_List_nodeI10Particle1DILi0EEEC2IIS1_EEEDpOT_
	.set	_ZNSt10_List_nodeI10Particle1DILi0EEEC2IIS1_EEEDpOT_,_ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_
	.weak	_ZNSt10_List_nodeI10Particle1DILi0EEEC1IJS1_EEEDpOT_
	.set	_ZNSt10_List_nodeI10Particle1DILi0EEEC1IJS1_EEEDpOT_,_ZNSt10_List_nodeI10Particle1DILi0EEEC2IJS1_EEEDpOT_
	.weak	_ZNSt10_List_nodeI10Particle1DILi0EEEC1IIS1_EEEDpOT_
	.set	_ZNSt10_List_nodeI10Particle1DILi0EEEC1IIS1_EEEDpOT_,_ZNSt10_List_nodeI10Particle1DILi0EEEC1IJS1_EEEDpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev:
.LFB2455:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2455:
	.size	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC1Ev,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m
	.type	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m, @function
_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m:
.LFB2457:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2457:
	.size	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m, .-_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE10deallocateEPS2_m
	.section	.text._ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv,"axG",@progbits,_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv,comdat
	.align 2
	.weak	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv
	.type	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv, @function
_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv:
.LFB2458:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2458:
	.size	_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv, .-_ZNKSt6vectorI10Particle1DILi0EESaIS1_EE8max_sizeEv
	.section	.text._ZSt3maxImERKT_S2_S2_,"axG",@progbits,_ZSt3maxImERKT_S2_S2_,comdat
	.weak	_ZSt3maxImERKT_S2_S2_
	.type	_ZSt3maxImERKT_S2_S2_, @function
_ZSt3maxImERKT_S2_S2_:
.LFB2459:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jnb	.L215
	movq	-16(%rbp), %rax
	jmp	.L216
.L215:
	movq	-8(%rbp), %rax
.L216:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2459:
	.size	_ZSt3maxImERKT_S2_S2_, .-_ZSt3maxImERKT_S2_S2_
	.section	.text._ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m,"axG",@progbits,_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m,comdat
	.weak	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m
	.type	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m, @function
_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m:
.LFB2460:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2460:
	.size	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m, .-_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8allocateERS2_m
	.section	.text._ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_,"axG",@progbits,_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_,comdat
	.weak	_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_
	.type	_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_, @function
_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_:
.LFB2461:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt13move_iteratorIP10Particle1DILi0EEEC1ES2_
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L221
	call	__stack_chk_fail
.L221:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2461:
	.size	_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_, .-_ZSt32__make_move_if_noexcept_iteratorIP10Particle1DILi0EESt13move_iteratorIS2_EET0_T_
	.section	.text._ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E,"axG",@progbits,_ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E,comdat
	.weak	_ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E
	.type	_ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E, @function
_ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E:
.LFB2462:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -8(%rbp)
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	movq	-16(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2462:
	.size	_ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E, .-_ZSt22__uninitialized_copy_aISt13move_iteratorIP10Particle1DILi0EEES3_S2_ET0_T_S6_S5_RSaIT1_E
	.section	.text._ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_:
.LFB2463:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2463:
	.size	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE7destroyIS2_EEvPT_
	.section	.text._ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorIfEC5ERKS1_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_
	.type	_ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_, @function
_ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_:
.LFB2465:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2465:
	.size	_ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_, .-_ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_
	.weak	_ZN9__gnu_cxx13new_allocatorIfEC1ERKS1_
	.set	_ZN9__gnu_cxx13new_allocatorIfEC1ERKS1_,_ZN9__gnu_cxx13new_allocatorIfEC2ERKS1_
	.section	.text._ZNSt16allocator_traitsISaIfEE8allocateERS0_m,"axG",@progbits,_ZNSt16allocator_traitsISaIfEE8allocateERS0_m,comdat
	.weak	_ZNSt16allocator_traitsISaIfEE8allocateERS0_m
	.type	_ZNSt16allocator_traitsISaIfEE8allocateERS0_m, @function
_ZNSt16allocator_traitsISaIfEE8allocateERS0_m:
.LFB2467:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2467:
	.size	_ZNSt16allocator_traitsISaIfEE8allocateERS0_m, .-_ZNSt16allocator_traitsISaIfEE8allocateERS0_m
	.section	.text._ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm
	.type	_ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm, @function
_ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm:
.LFB2468:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2468:
	.size	_ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm, .-_ZN9__gnu_cxx13new_allocatorIfE10deallocateEPfm
	.section	.text._ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_,"axG",@progbits,_ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_,comdat
	.weak	_ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_
	.type	_ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_, @function
_ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_:
.LFB2469:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movss	%xmm0, -12(%rbp)
	leaq	-12(%rbp), %rdx
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt6fill_nIPfmfET_S1_T0_RKT1_
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L231
	call	__stack_chk_fail
.L231:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2469:
	.size	_ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_, .-_ZNSt27__uninitialized_default_n_1ILb1EE18__uninit_default_nIPfmEET_S3_T0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv:
.LFB2479:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movabsq	$768614336404564650, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2479:
	.size	_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt10_List_nodeI10Particle1DILi0EEEE8max_sizeEv
	.section	.text._ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_,"axG",@progbits,_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_,comdat
	.weak	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_
	.type	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_, @function
_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_:
.LFB2480:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2480:
	.size	_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_, .-_ZNSt16allocator_traitsISaI10Particle1DILi0EEEE8max_sizeERKS2_
	.section	.text._ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv,comdat
	.align 2
	.weak	_ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	.type	_ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv, @function
_ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv:
.LFB2481:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2481:
	.size	_ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv, .-_ZNKSt12_Vector_baseI10Particle1DILi0EESaIS1_EE19_M_get_Tp_allocatorEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv, @function
_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv:
.LFB2482:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv
	cmpq	-16(%rbp), %rax
	setb	%al
	testb	%al, %al
	je	.L239
	call	_ZSt17__throw_bad_allocv
.L239:
	movq	-16(%rbp), %rax
	salq	$3, %rax
	movq	%rax, %rdi
	call	_Znwm
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2482:
	.size	_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8allocateEmPKv
	.section	.text._ZNSt13move_iteratorIP10Particle1DILi0EEEC2ES2_,"axG",@progbits,_ZNSt13move_iteratorIP10Particle1DILi0EEEC5ES2_,comdat
	.align 2
	.weak	_ZNSt13move_iteratorIP10Particle1DILi0EEEC2ES2_
	.type	_ZNSt13move_iteratorIP10Particle1DILi0EEEC2ES2_, @function
_ZNSt13move_iteratorIP10Particle1DILi0EEEC2ES2_:
.LFB2484:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2484:
	.size	_ZNSt13move_iteratorIP10Particle1DILi0EEEC2ES2_, .-_ZNSt13move_iteratorIP10Particle1DILi0EEEC2ES2_
	.weak	_ZNSt13move_iteratorIP10Particle1DILi0EEEC1ES2_
	.set	_ZNSt13move_iteratorIP10Particle1DILi0EEEC1ES2_,_ZNSt13move_iteratorIP10Particle1DILi0EEEC2ES2_
	.section	.text._ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_,"axG",@progbits,_ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_,comdat
	.weak	_ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_
	.type	_ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_, @function
_ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_:
.LFB2486:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -32(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movb	$1, -1(%rbp)
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	-32(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2486:
	.size	_ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_, .-_ZSt18uninitialized_copyISt13move_iteratorIP10Particle1DILi0EEES3_ET0_T_S6_S5_
	.section	.text._ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv, @function
_ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv:
.LFB2487:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv
	cmpq	-16(%rbp), %rax
	setb	%al
	testb	%al, %al
	je	.L245
	call	_ZSt17__throw_bad_allocv
.L245:
	movq	-16(%rbp), %rax
	salq	$2, %rax
	movq	%rax, %rdi
	call	_Znwm
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2487:
	.size	_ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorIfE8allocateEmPKv
	.section	.text._ZSt6fill_nIPfmfET_S1_T0_RKT1_,"axG",@progbits,_ZSt6fill_nIPfmfET_S1_T0_RKT1_,comdat
	.weak	_ZSt6fill_nIPfmfET_S1_T0_RKT1_
	.type	_ZSt6fill_nIPfmfET_S1_T0_RKT1_, @function
_ZSt6fill_nIPfmfET_S1_T0_RKT1_:
.LFB2488:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_
	movq	%rax, %rcx
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rax
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	_ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZSt6fill_nIPfmfET_S1_T0_RKT1_, .-_ZSt6fill_nIPfmfET_S1_T0_RKT1_
	.section	.text._ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv:
.LFB2495:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movabsq	$2305843009213693951, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2495:
	.size	_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorI10Particle1DILi0EEE8max_sizeEv
	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_,comdat
	.weak	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_
	.type	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_, @function
_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_:
.LFB2496:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2496
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -48(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -24(%rbp)
.L253:
	leaq	-64(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB21:
	call	_ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_
.LEHE21:
	testb	%al, %al
	je	.L252
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt11__addressofI10Particle1DILi0EEEPT_RS2_
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt13move_iteratorIP10Particle1DILi0EEEppEv
	addq	$8, -24(%rbp)
	jmp	.L253
.L252:
	movq	-24(%rbp), %rax
	jmp	.L259
.L257:
	movq	%rax, %rdi
	call	__cxa_begin_catch
	movq	-24(%rbp), %rdx
	movq	-56(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP10Particle1DILi0EEEvT_S3_
.LEHB22:
	call	__cxa_rethrow
.LEHE22:
.L258:
	movq	%rax, %rbx
	call	__cxa_end_catch
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB23:
	call	_Unwind_Resume
.LEHE23:
.L259:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2496:
	.section	.gcc_except_table
	.align 4
.LLSDA2496:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT2496-.LLSDATTD2496
.LLSDATTD2496:
	.byte	0x1
	.uleb128 .LLSDACSE2496-.LLSDACSB2496
.LLSDACSB2496:
	.uleb128 .LEHB21-.LFB2496
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L257-.LFB2496
	.uleb128 0x1
	.uleb128 .LEHB22-.LFB2496
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L258-.LFB2496
	.uleb128 0
	.uleb128 .LEHB23-.LFB2496
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
.LLSDACSE2496:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT2496:
	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_,comdat
	.size	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_, .-_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP10Particle1DILi0EEES5_EET0_T_S8_S7_
	.section	.text._ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv:
.LFB2497:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movabsq	$4611686018427387903, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2497:
	.size	_ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorIfE8max_sizeEv
	.section	.text._ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_,"axG",@progbits,_ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_,comdat
	.weak	_ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_
	.type	_ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_, @function
_ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_:
.LFB2498:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2498:
	.size	_ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_, .-_ZSt12__niter_baseIPfENSt11_Niter_baseIT_E13iterator_typeES2_
	.section	.text._ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_,"axG",@progbits,_ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_,comdat
	.weak	_ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_
	.type	_ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_, @function
_ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_:
.LFB2499:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movss	(%rax), %xmm0
	movss	%xmm0, -12(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -8(%rbp)
.L266:
	cmpq	$0, -8(%rbp)
	je	.L265
	movq	-24(%rbp), %rax
	movss	-12(%rbp), %xmm0
	movss	%xmm0, (%rax)
	subq	$1, -8(%rbp)
	addq	$4, -24(%rbp)
	jmp	.L266
.L265:
	movq	-24(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2499:
	.size	_ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_, .-_ZSt10__fill_n_aIPfmfEN9__gnu_cxx11__enable_ifIXsrSt11__is_scalarIT1_E7__valueET_E6__typeES6_T0_RKS4_
	.section	.text._ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_,"axG",@progbits,_ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_,comdat
	.weak	_ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_
	.type	_ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_, @function
_ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_:
.LFB2500:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_
	xorl	$1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2500:
	.size	_ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_, .-_ZStneIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_
	.section	.text._ZNSt13move_iteratorIP10Particle1DILi0EEEppEv,"axG",@progbits,_ZNSt13move_iteratorIP10Particle1DILi0EEEppEv,comdat
	.align 2
	.weak	_ZNSt13move_iteratorIP10Particle1DILi0EEEppEv
	.type	_ZNSt13move_iteratorIP10Particle1DILi0EEEppEv, @function
_ZNSt13move_iteratorIP10Particle1DILi0EEEppEv:
.LFB2501:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	leaq	8(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2501:
	.size	_ZNSt13move_iteratorIP10Particle1DILi0EEEppEv, .-_ZNSt13move_iteratorIP10Particle1DILi0EEEppEv
	.section	.text._ZSt11__addressofI10Particle1DILi0EEEPT_RS2_,"axG",@progbits,_ZSt11__addressofI10Particle1DILi0EEEPT_RS2_,comdat
	.weak	_ZSt11__addressofI10Particle1DILi0EEEPT_RS2_
	.type	_ZSt11__addressofI10Particle1DILi0EEEPT_RS2_, @function
_ZSt11__addressofI10Particle1DILi0EEEPT_RS2_:
.LFB2502:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2502:
	.size	_ZSt11__addressofI10Particle1DILi0EEEPT_RS2_, .-_ZSt11__addressofI10Particle1DILi0EEEPT_RS2_
	.section	.text._ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv,"axG",@progbits,_ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv,comdat
	.align 2
	.weak	_ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv
	.type	_ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv, @function
_ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv:
.LFB2503:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2503:
	.size	_ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv, .-_ZNKSt13move_iteratorIP10Particle1DILi0EEEdeEv
	.section	.text._ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_,"axG",@progbits,_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_,comdat
	.weak	_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_
	.type	_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_, @function
_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_:
.LFB2504:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI10Particle1DILi0EEEOT_RNSt16remove_referenceIS2_E4typeE
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rsi
	movl	$8, %edi
	call	_ZnwmPv
	testq	%rax, %rax
	je	.L279
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
.L279:
	nop
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2504:
	.size	_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_, .-_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_
	.weak	_ZSt10_ConstructI10Particle1DILi0EEIS1_EEvPT_DpOT0_
	.set	_ZSt10_ConstructI10Particle1DILi0EEIS1_EEvPT_DpOT0_,_ZSt10_ConstructI10Particle1DILi0EEJS1_EEvPT_DpOT0_
	.section	.text._ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_,"axG",@progbits,_ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_,comdat
	.weak	_ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_
	.type	_ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_, @function
_ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_:
.LFB2505:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2505:
	.size	_ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_, .-_ZNSt10_Iter_baseIPfLb0EE7_S_baseES0_
	.section	.text._ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_,"axG",@progbits,_ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_,comdat
	.weak	_ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_
	.type	_ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_, @function
_ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_:
.LFB2506:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv
	movq	%rax, %rbx
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv
	cmpq	%rax, %rbx
	sete	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2506:
	.size	_ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_, .-_ZSteqIP10Particle1DILi0EEEbRKSt13move_iteratorIT_ES7_
	.section	.text._ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv,"axG",@progbits,_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv,comdat
	.align 2
	.weak	_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv
	.type	_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv, @function
_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv:
.LFB2507:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2507:
	.size	_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv, .-_ZNKSt13move_iteratorIP10Particle1DILi0EEE4baseEv
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB2508:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L288
	cmpl	$65535, -8(%rbp)
	jne	.L288
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	call	__cxa_atexit
.L288:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2508:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB2509:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2509:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.section	.rodata
	.align 8
.LC0:
	.long	0
	.long	1104006501
	.align 4
.LC7:
	.long	1036831949
	.align 4
.LC8:
	.long	1008981770
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.2) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
