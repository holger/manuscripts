// clang++ -std=c++11 -O3 -o course course.cpp -Rpass-analysis=loop-vectorize
// clang++ -std=c++11 -O3 -o course course.cpp -Rpass=loop-vectorize

// g++ -std=c++11 -O3 -o course_vectorizationTest course_vectorizationTest.cpp -fopt-info-vec
// g++ -std=c++11 -O3 -o course_vectorizationTest course_vectorizationTest.cpp -fopt-info-vec-missed=missed.txt

// g++ -O2 -o course course.cpp -Q --help=optimizers

#include <iostream>
#include <cstdlib>
#include <list>
#include <vector>
#include <chrono>

#include "particle.hpp"

const int tempSize = 0;
typedef Particle1D<tempSize> Particle;

int main(int argc, char** argv)
{
std::cerr << "Computing time measurements ...\n";

  if(argc < 3) {
    std::cerr << "usage: ./course <particle number> <loop number>\n";
    exit(1);
  }

  // get the number of particles from the command line
  const int particleNumber = atoi(argv[1]);
  const int loopNumber = atoi(argv[2]);

  std::chrono::time_point<std::chrono::system_clock> start, end;
  
  std::cout << "particleNumber = " << particleNumber << std::endl;
  
  // --------------------------------------------------------------
  
  std::list<Particle1D<tempSize>> pList;

  for(int i=0;i<particleNumber;i++)
    pList.push_back(Particle());


  start = std::chrono::system_clock::now();
  
  for(int loop=0; loop<loopNumber; loop++) {
    for(auto &p : pList)
      p.pos += p.vel;
  }

  end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  
  std::cout << "list : elapsed time: " << elapsed_seconds.count() << "\t" << pList.begin()->pos << "\n";

  // --------------------------------------------------------------

  std::vector<Particle1D<tempSize>> pVec;

  for(int i=0;i<particleNumber;i++)
    pVec.push_back(Particle());

  start = std::chrono::system_clock::now();
  
  for(int loop=0; loop<loopNumber; loop++) {
    for(auto &p : pVec)
      p.pos += p.vel;
  }

  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  
  std::cout << "vector : elapsed time: " << elapsed_seconds.count() << "\t" << pVec[0].pos << "\n";
  
  // --------------------------------------------------------------

  std::vector<float> pos(particleNumber);
  std::vector<float> vel(particleNumber);

  for(int i=0;i<particleNumber;i++) {
    pos[i] = 0.1;
    vel[i] = 0.01;
  }

  start = std::chrono::system_clock::now();
  
  for(int loop=0; loop<loopNumber; loop++) {
    for(int p=0; p<particleNumber; p++) {
      pos[p] += vel[p];
    }
  }

  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  
  std::cout << "arrays : elapsed time: " << elapsed_seconds.count() << "\t" << pos[0] << "\n";
};
