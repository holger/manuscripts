// g++ -o course course.cpp -std=c++11 -O3 -mavx -I/home/holger/Downloads/boost_1_62_0 -I/home/holger/Downloads/boost.simd-4.16.9.0/include

// clang++ -std=c++11 -O3 -o course course.cpp -Rpass-analysis=loop-vectorize
// clang++ -std=c++11 -O3 -o course course.cpp -Rpass=loop-vectorize

// g++ -O2 -o course course.cpp -Q --help=optimizers

#include <iostream>
#include <cstdlib>
#include <list>
#include <vector>
#include <chrono>

#include <boost/align.hpp>

#include <boost/simd.hpp>
#include <boost/simd/function/load.hpp>
#include <boost/simd/function/store.hpp>
#include <boost/simd/function/shuffle.hpp>

#include "particle.hpp"

const int tempSize = 0;
typedef Particle1D<tempSize> Particle;

int main(int argc, char** argv)
{
std::cerr << "Computing time measurements ...\n";

  if(argc < 3) {
    std::cerr << "usage: ./course <particle number> <loop number>\n";
    exit(1);
  }

  // get the number of particles from the command line
  const int particleNumber = atoi(argv[1]);
  const int loopNumber = atoi(argv[2]);

  std::chrono::time_point<std::chrono::system_clock> start, end;
  
  std::cout << "particleNumber = " << particleNumber << std::endl;
  
  // --------------------------------------------------------------
  
  std::list<Particle1D<tempSize>> pList;

  for(int i=0;i<particleNumber;i++)
    pList.push_back(Particle());


  start = std::chrono::system_clock::now();
  
  for(int loop=0; loop<loopNumber; loop++) {
    for(auto &p : pList)
      p.pos += p.vel;
  }

  end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  
  std::cout << "list : elapsed time: " << elapsed_seconds.count() << "\t" << pList.begin()->pos << "\n";

  // --------------------------------------------------------------

  std::vector<Particle1D<tempSize>> pVec;

  for(int i=0;i<particleNumber;i++)
    pVec.push_back(Particle());

  start = std::chrono::system_clock::now();
  
  for(int loop=0; loop<loopNumber; loop++) {
    for(auto &p : pVec)
      p.pos += p.vel;
  }

  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  
  std::cout << "vector : elapsed time: " << elapsed_seconds.count() << "\t" << pVec[0].pos << "\n";
  
  // --------------------------------------------------------------

  std::vector<float> pos(particleNumber);
  std::vector<float> vel(particleNumber);

  for(int i=0;i<particleNumber;i++) {
    pos[i] = 0.1;
    vel[i] = 0.01;
  }

  start = std::chrono::system_clock::now();
  
  for(int loop=0; loop<loopNumber; loop++) {
    for(int p=0; p<particleNumber; p++) {
      pos[p] += vel[p];
    }
  }

  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  
  std::cout << "arrays : elapsed time: " << elapsed_seconds.count() << "\t" << pos[0] << "\n";

  // --------------------------------------------------------------

  for(int i=0; i<pVec.size(); i++) {
    pVec[i].init();
  }
  
  start = std::chrono::system_clock::now();

  for(int loop=0; loop<loopNumber; loop++) 
    for(int p=0; p<particleNumber-1; p+=2) {

      boost::simd::pack<float,2> posPack{pVec[p].pos, pVec[p+1].pos};
      boost::simd::pack<float,2> velPack{pVec[p].vel, pVec[p+1].vel};

      posPack += velPack;

      float temp[2];
      boost::simd::store(posPack, &temp[0]);

      pVec[p].pos = temp[0];
      pVec[p+1].pos = temp[1];
    }
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  
  std::cout << "boost.simd vector : elapsed time: " << elapsed_seconds.count() << "\t" << pVec[0].pos << "\n";

  // --------------------------------------------------------------

  for(int i=0; i<pVec.size(); i++) {
    pVec[i].init();
  }
  
  start = std::chrono::system_clock::now();

  for(int loop=0; loop<loopNumber; loop++) 
    for(int p=0; p<particleNumber-3; p+=4) {
      boost::simd::pack<float,4> posPack{pVec[p].pos, pVec[p+1].pos, pVec[p+2].pos, pVec[p+3].pos};
      boost::simd::pack<float,4> velPack{pVec[p].vel, pVec[p+1].vel, pVec[p+2].vel, pVec[p+3].vel};

      posPack += velPack;

      float temp[4];
      boost::simd::store(posPack, &temp[0]);

      pVec[p].pos = temp[0];
      pVec[p+1].pos = temp[1];
      pVec[p+2].pos = temp[2];
      pVec[p+3].pos = temp[3];
    }

  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  
  std::cout << "boost.simd vector pack 4 : elapsed time: " << elapsed_seconds.count() << "\t" << pVec[0].pos << "\n";
    
  // --------------------------------------------------------------

  for(int i=0; i<pVec.size(); i++) {
    pVec[i].init();
  }

  namespace bs = boost::simd;
  namespace ba = boost::alignment;
  using pack_t = bs::pack<int>;
  std::size_t alignment    = pack_t::alignment;
  
  float* floatArr = (float*)ba::aligned_alloc(alignment, particleNumber*2);
  Particle* pArr = reinterpret_cast<Particle*>(floatArr);
  
  const size_t packSize = boost::simd::cardinal_of<boost::simd::pack<float>>();

  start = std::chrono::system_clock::now();
  
  for(int loop=0; loop<loopNumber; loop++) 
    for(int p=0; p<particleNumber*2-packSize*2; p+=packSize*2) {
      
      boost::simd::pack<float> pack1(floatArr+p);
      boost::simd::pack<float> pack2(floatArr+packSize+p);

      auto posPack = boost::simd::shuffle<0,2,4,6,8,10,12,14>(pack1,pack2);
      auto velPack = boost::simd::shuffle<1,3,5,7,9,11,13,15>(pack1,pack2);
      
      posPack += velPack;

      auto resultPack1 = boost::simd::shuffle<0,packSize+0,1,packSize+1,2,packSize+2,3,packSize+3>(posPack,velPack);
      auto resultPack2 = boost::simd::shuffle<4,packSize+4,5,packSize+5,6,packSize+6,7,packSize+7>(posPack,velPack);
      
      boost::simd::store(resultPack1, floatArr+p);
      boost::simd::store(resultPack2, floatArr+p+packSize);
    }

  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;


  
  std::cout << "boost.simd vector shuffle : elapsed time: " << elapsed_seconds.count() << "\t" << floatArr[0] << "\t" << pArr[0].pos << "\n";
};
