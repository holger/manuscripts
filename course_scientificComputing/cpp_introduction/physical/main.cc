#include <iostream>

#include "physical.h"

// templates help to compute in physical units dimensionally correct!

int main(int argc, char** argv)
{
  std::cout << "dimensional correct computations start now ...\n";

  // imagine that you want to compute the force on an object from its mass and acceleration.
  // Usually, you write:

  double m = 5;
  double a = 10;
  double f = m * a;

  // But what about the Units? Is it dimensionally correct?

  // Would'nt it be better to write:

  Mass mass = 5;
  Acceleration acc = 10;
  Force force = mass * acc;

  // ... and expect the compiler to check whether the units are correct ?
  // So that he complains in case that you write
  
  //  Force force2 = mass * acc * acc;

  // let us look into "physical.h" !!!
  
  // some additional tests :

  Length distance = 10;
  Time duration = 2;
  
  Velocity velocity = distance/duration;

  std::cout << velocity.dim() << std::endl;

  Force force3 = Mass(5) * Acceleration (10);
  
  std::cout << force3() << " " << force3.dim() << std::endl;

  //  Force force2 = Mass(5) * velocity;

  std::cout << write(force3) << std::endl;
  std::cout << write(Mass(5)) << std::endl;
}
