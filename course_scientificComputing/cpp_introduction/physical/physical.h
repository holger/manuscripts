#ifndef physical_h
#define physical_h

// for a more complete implementation see
// https://code.google.com/p/unitscpp/

// we use our small string-conversion function
#include "../toString/toString.h"
#include "../typeManip/typeManip.h"

#include <ostream>
#include <sstream>
#include <string>
#include <sstream>

// Type of the values (could be a template parameter...)
typedef double NT;   

// forward declaration
template<int U1, int U2, int U3, int U4>
class Units;

// forward declaration
template <int U1, int U2, int U3, int U4>
std::ostream & operator<<(std::ostream & s, const Units<U1, U2, U3, U4> & rhs);

// Fun starts here:
// U1, U2, U3, U4 represent the dimension of a physical quantity
// U1 -> kg
// U2 -> m
// U3 -> s
// U4 -> rad
template<int U1, int U2, int U3, int U4>
class Units
{
public:
  Units(NT value_=NT(0)) : value(value_) {}

  // This turns the class into a function object that allows the user
  // to easily get the value.
  NT operator()() const { return value; }
  
  // Helper function to get a text representation of the
  // object's dimensions.  It is static because the
  // representation is known at compile time.
  // you can write Units<1,1,1,1>.dim();
  static std::string dim()
  {
    std::stringstream s;
    s << "<" << U1 << ","<< U2 << ","<< U3 << ","<< U4 << ">";
    return s.str();
  }
  
  // Assignment operator
  Units& operator=(const Units & rhs)
  {
    value = rhs.value;
    return *this;
  }  

private:

  // Value;
  NT value;
};

// Proper names for basic dimensions:
typedef Units<1,0,0,0> Mass;
typedef Units<0,1,0,0> Length;
typedef Units<0,0,1,0> Time;
typedef Units<0,0,0,1> Angle;

typedef Units<0,1,-1,0> Velocity;
typedef Units<0,1,-2,0> Acceleration;
typedef Units<1,1,-2,0> Force;

// Fun starts when adding two physics quantities:
// Velocity(3) + Velocity(4) should be fine, but
// Velocity(4) + Acceleration(5) not!!!

template <int U1, int U2, int U3, int U4>
  const Units<U1, U2, U3, U4> operator+(const Units<U1, U2, U3, U4> & lhs, const Units<U1, U2, U3, U4> & rhs)
{
    return Units<U1, U2, U3, U4>(lhs()+rhs());
}

template <int U1, int U2, int U3, int U4>
  const Units<U1, U2, U3, U4> operator-(const Units<U1, U2, U3, U4> & lhs, const Units<U1, U2, U3, U4> & rhs)
{
    return Units<U1, U2, U3, U4>(lhs()-rhs());
}

// even better for multiplication or division

template <int U1a, int U2a, int U3a, int U4a, int U1b, int U2b, int U3b, int U4b>
const Units<U1a+U1b, U2a+U2b, U3a+U3b, U4a+U4b> operator*(const Units<U1a, U2a, U3a, U4a> & lhs, const Units<U1b, U2b, U3b, U4b> & rhs)
{
    return Units<U1a+U1b, U2a+U2b, U3a+U3b, U4a+U4b>(lhs()*rhs());
}

template <int U1a, int U2a, int U3a, int U4a, int U1b, int U2b, int U3b, int U4b>
const Units<U1a-U1b, U2a-U2b, U3a-U3b, U4a-U4b> operator/(const Units<U1a, U2a, U3a, U4a> & lhs, const Units<U1b, U2b, U3b, U4b> & rhs)
{
    return Units<U1a-U1b, U2a-U2b, U3a-U3b, U4a-U4b>(lhs()/rhs());
}

template<class TYPE>
inline double intPow(TYPE x, int potenz)
{
  if(potenz == 0)
    return 1.;
  double tmp = x;
  if(potenz < 0){
    for(int i=0;i<-potenz-1;i++){
      tmp *= x;
    }
    return 1/tmp;
  }
  else
    for(int i=0;i<potenz-1;i++){
      tmp *= x;
    }
  return tmp;
}

template <int U1, int U2, int U3, int U4, int EXP>
  const Units<U1*EXP, U2*EXP, U3*EXP, U4*EXP> operator^(const Units<U1, U2, U3, U4> & lhs, const Int2Type<EXP> & rhs)
{
  return Units<U1*EXP, U2*EXP, U3*EXP, U4*EXP>(intPow(lhs(),EXP));
}

// 
template <int U1, int U2, int U3, int U4>
std::ostream & operator<<(std::ostream & s, const Units<U1, U2, U3, U4> & rhs)
{
    return s << rhs();
}

// writing human-readible units:

template<int U1, int U2, int U3, int U4>
std::string write(Units<U1,U2,U3,U4> u)
{
return std::string(toString(u)+" kg^"+toString(U1)+" m^"+toString(U2)+" s^"+toString(U3)+" rad^"+toString(U4));
}

template<int U1, int U2, int U3>
std::string write(Units<U1,U2,U3,0> u)
{
return std::string(toString(u)+" kg^"+toString(U1)+" m^"+toString(U2)+" s^"+toString(U3));
}

template<int U2, int U3, int U4>
std::string write(Units<0,U2,U3,U4> u)
{
return std::string(toString(u)+" m^"+toString(U2)+" s^"+toString(U3)+" rad^"+toString(U4));
}

template<int U2, int U3>
std::string write(Units<0,U2,U3,0> u)
{
return std::string(toString(u)+" m^"+toString(U2)+" s^"+toString(U3));
}

std::string write(Units<1,0,0,0> u)
{
return std::string(toString(u)+" kg");
}

std::string write(Units<0,1,0,0> u)
{
return std::string(toString(u)+" m");
}

std::string write(Units<0,0,1,0> u)
{
return std::string(toString(u)+" s");
}

std::string write(Units<1,1,-2,0> u)
{
return std::string(toString(u)+" N");
}


#endif
