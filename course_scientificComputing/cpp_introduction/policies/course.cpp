#include <iostream>

#include "particleHandler.hpp"

int main()
{
  std::cout << "starting policy-based class design test ...\n";

  int number = 10;
  ParticleHandler<> pH(number);
  //ParticleHandler<MvLastElementErase> pH(number);

  std::cout << sizeof(std::vector<float>) << std::endl;
  
  for(int i=0; i<number; i++)
    pH(i) = 0.1*i;

  for(auto p : pH)
    std::cout << p << std::endl;
    
  pH.erase(7);

  for(auto p : pH)
    std::cout << p << std::endl;

}
