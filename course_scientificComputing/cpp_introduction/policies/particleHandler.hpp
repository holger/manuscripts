#ifndef particleHandler_hpp
#define particleHandler_hpp

#include <vector>

class StdVecErase
{
public:
  StdVecErase(std::vector<float>& vec) : vec_(vec) {}

  void erase(int p) {
    std::cout << "StdVecErase::erase\n";

    auto it = vec_.begin() + p;
    vec_.erase(it);
  }
private:
  std::vector<float>& vec_;
};

class MvLastElementErase
{
public:
  MvLastElementErase(std::vector<float>& vec) : vec_(vec) {}

  void erase(int p) {
    std::cout << "MvLastElementErase::erase\n";

    vec_[p] = vec_[vec_.size()-1];
    vec_.pop_back();
  }
private:
  std::vector<float>& vec_;
};

template<typename Erase = StdVecErase>
class ParticleHandler : public Erase
{
public:
  
  ParticleHandler(int n) : pVec_(n), Erase(pVec_) {}

  size_t size() { return pVec_.size(); }
  
  float operator()(int p) const {
    return pVec_[p];
  }

  float& operator()(int p) {
    return pVec_[p];
  }

  auto begin() { return pVec_.begin(); }
  auto end() { return pVec_.end(); }

private:

  std::vector<float> pVec_;
};

#endif
