#include "polymorph.h"

#include <cstdlib>

// We saw in ../inheritance that a class can inherit properties of
// another classes. What is called polymorphismus is the possibility
// to use a base class pointer to access member functions of objects
// of derived classes

int main(int argc, char** argv)
{
  std::cerr << "Polymorphism test starts\n";
  
  // create base and derived objects
  Base base;
  Derived derived;
  
  // create base class pointer to base class object
  Base* baseP = &base;
  // create base class pointer to derived class object
  Base* derivedP = &derived;
  
  std::cerr << baseP->name() << std::endl;
  std::cerr << derivedP->name() << std::endl;

  // Why do we now get the correct names?
  // The keyword in polymorph.h is virtual!!!

  // The correct function can be chosen at runtime:
  
  Base* pointer;

  std::cout << "type b for base class and d for derived class: ";
  char a;
  // read character from standard input
  std::cin >> a;
  // create object according to users choice
  if(a == 'b')
    pointer = new Base;
  else if (a == 'd')
    pointer = new Derived;
  else { std::cerr << "character " << a << " not known\n"; exit(1);}
  
  std::cerr << pointer->name() << std::endl;

  // This is not possible without virtual functions (polymorphism)
}

// Exercise:

// Implement a base class Person with a member function
// name(). Additionally, derive a class Student from Person which has
// a member-function study. Create objects and pointers and play
// around!
