#include "polymorph.h"

int main(int argc, char** argv)
{
  std::cerr << "Polymorphism test starts\n";

  Base* base = new Base;
  Base* derived = new Derived;
  
  std::cerr << base->name() << std::endl;
  std::cerr << derived->name() << std::endl;
}
