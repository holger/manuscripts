#ifndef polymorph_h
#define polymorph_h

#include <iostream>

class Base
{
public:
  Base() { std::cerr << "Base::Base()\n"; }

  virtual std::string name() { return "I am the Base class"; }
};

class Derived : public Base
{
public:
  Derived() { std::cerr << "Derived::Derived()\n"; }

  std::string name() { return "I am the Derived class"; }
};

#endif
