// clang++ -o bench bench.cpp -L/home/holger/Downloads/benchmark-master/src -lbenchmark -pthread -O3 -mavx2

#include "/home/holger/Downloads/benchmark-master/include/benchmark/benchmark.h"
#include <cmath>
#include <iostream>

#include "power.hpp"

static void escape(void *p) {
  asm volatile("" : : "g"(p) : "memory");
}

static void clobber() {
  asm volatile("" : : : "memory");
}

static void BM_StringCreation(benchmark::State& state) {
  while (state.KeepRunning())
    std::string empty_string;
}
// Register the function as a benchmark
//BENCHMARK(BM_StringCreation);

// Define another benchmark
static void BM_StringCopy(benchmark::State& state) {
  std::string x = "hello";
  while (state.KeepRunning())
    std::string copy(x);
}

// Define another benchmark
static void BM_pow(benchmark::State& state) {
  float x = M_PI;
  float y;
  while (state.KeepRunning()) {
    escape(&x);
    escape(&y);
    y = pow(x,20);
    clobber();
  }
}

// Define another benchmark
static void BM_power(benchmark::State& state) {
  float x = M_PI;
  float y;
  while (state.KeepRunning()) {
    escape(&x);
    escape(&y);
    y = power(x,20);
    clobber();
  }
}

// Define another benchmark
static void BM_powerDirect(benchmark::State& state) {
  float x = M_PI;
  float y;
  while (state.KeepRunning()) {
    escape(&x);
    escape(&y);
    y = x*x*x*x*x*
      x*x*x*x*x*
      x*x*x*x*x*
      x*x*x*x*x;
    clobber();
  }
}

static void BM_powerTmp(benchmark::State& state) {
  float x = M_PI;
  float y;
  while (state.KeepRunning()) {
    escape(&x);
    escape(&y);
    y = Pow<20>::pow(x);
    clobber();
  }
}

// Define another benchmark
static void BM_powerTmp_opt(benchmark::State& state) {
  float x = M_PI;
  float y;
  while (state.KeepRunning()) {
    escape(&x);
    escape(&y);
    y = Pow1<20>::pow(x);
    clobber();
  }
}

BENCHMARK(BM_pow);
BENCHMARK(BM_power);
BENCHMARK(BM_powerDirect);
BENCHMARK(BM_powerTmp);
BENCHMARK(BM_powerTmp_opt);

BENCHMARK_MAIN();
