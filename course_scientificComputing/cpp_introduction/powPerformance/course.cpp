#include <iostream>
#include <chrono>
#include <cmath>
#include <vector>

#include "power.hpp"

int main(int argc, char** argv)
{
  std::cerr << "Performance tests of pow starts...\n";

  if(argc < 2) {
    std::cerr << "ERROR: argc = " << argc << " < 2\n";
    std::cerr << "usage: course <n>\n";
    exit(1);
  }    
  
  int n = atoi(argv[1]);

  const int p = 20;
  
  std::vector<double> array(n);

  for(int i=0;i<n;i++)
    array[i] = double(rand())/RAND_MAX;
  
  std::chrono::time_point<std::chrono::system_clock> start, end;
  
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++)
    array[i] = pow(array[i],p);

  end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  std::cout << "std::pow : elapsed time: " << elapsed_seconds.count() << "\n";

  // ------------------------------------------------------------------------------

  for(int i=0;i<n;i++)
    array[i] = double(rand())/RAND_MAX;
    
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++)
    array[i] = power(array[i],p);
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "power : elapsed time: " << elapsed_seconds.count() << "\n";

  // ------------------------------------------------------------------------------

  for(int i=0;i<n;i++)
    array[i] = double(rand())/RAND_MAX;
    
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++) {
    double val = array[i];
    array[i] = array[i]*array[i]*array[i]*array[i]*array[i]*
      array[i]*array[i]*array[i]*array[i]*array[i]*
      array[i]*array[i]*array[i]*array[i]*array[i]*
      array[i]*array[i]*array[i]*array[i]*array[i];
  }
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "direct : elapsed time: " << elapsed_seconds.count() << "\n";

  // ------------------------------------------------------------------------------

  for(int i=0;i<n;i++)
    array[i] = double(rand())/RAND_MAX;
    
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++) {
    array[i] = Pow<p>::pow(array[i]);
  }
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "TMP : elapsed time: " << elapsed_seconds.count() << "\n";

  // ------------------------------------------------------------------------------
  
  for(int i=0;i<n;i++)
    array[i] = double(rand())/RAND_MAX;
    
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++) {
    array[i] = Pow1<p>::pow(array[i]);
  }
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "TMP opt : elapsed time: " << elapsed_seconds.count() << "\n";

  // ------------------------------------------------------------------------------

}
