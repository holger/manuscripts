#ifndef power_hpp
#define power_hpp

float power(float value, int power)
{
  float val = value;
  for(int i=0;i<power-1;i++)
    value *= val;

  return value;
}

// general template for powers of N
template<int N> 
struct Pow
{
  static double pow(double x) {
    return x*Pow<N-1>::pow(x);
  }
};

// recursion stopping condition
template<>
struct Pow<1> 
{
  static double pow(double x) {
    return x;
  }
};

// special N=0 treatement
template<>
struct Pow<0> 
{
  static  double pow(double x) {
    return 1;
  }
};

// for the call Pow<4>::pow(2) the compiler generates the code 2*2*2*2.
// is this perfect? No! Better would be 2^2 * 2^2 because of reusing the
// result 2^2. -> see following implementation

template<int N, bool odd=N%2>
struct Pow1
{
  static double pow(double x) {
    return Pow1<N/2>::pow(x)*Pow1<N/2>::pow(x);
  }
};

template<int N>
struct Pow1<N,true>
{
  static double pow(double x) {
    return x*Pow1<(N-1)/2>::pow(x)*Pow1<(N-1)/2>::pow(x);
  }
};

// the second template argument (odd) is never explicitely given by
// the user. It is there and evaluated by the compiler to be able to
// do partial spezialization.

template<>
struct Pow1<1,true>
{
  static double pow(double x) {
    return x;
  }
};

template<>
struct Pow1<0,false>
{
  static double pow(double x) {
    return 1;
  }
};




#endif
