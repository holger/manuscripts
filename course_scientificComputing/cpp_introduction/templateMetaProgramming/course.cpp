// What is Template Meta Programming (TMP) ?

// - Move computations from run-time to compile-time (factorial)

// - Code generation. Let the compile generate code for computations (xPowN)
//   and for function calls (soax)
