#include "studentClass.hpp"
#include "studentClass_TMP.hpp"

int main()
{
  std::cout << "let us test the student class\n\n";

  Class studentClass;

  // all students learn;
  studentClass.Steve::learn();
  studentClass.Yousseff::learn();

  studentClass.learn();
  studentClass.call();

  std::cout << "\nlet us test the TMP student class\n\n";

  // ok. But what if Franck joins the class? All member functions
  // (learn, call) would have to be updated. Better: specify a list of
  // students and let the compiler implement all functions ->
  // studentClass_TMP.hpp
  

  
  // specify the list of students by a tuple:
  typedef std::tuple<Steve,Yousseff> StudentTuple;
  //  typedef std::tuple<int, double> StudentTuple;

  TmpClass<StudentTuple> tmpStudentClass;
  
  tmpStudentClass.learn();
  tmpStudentClass.call();
  tmpStudentClass.writeProperty();
}

// excersise: 1) add another Student to the class. 2) Add a member
// function to the students and call them for the whole class
