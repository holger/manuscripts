// task: compute x^N.
// Different ways:

// 1) C math library: call function pow(x,N)
// x AND N are double -> bad performance

// 2) TMP: x^N = x*x^(N-1) (by recursion)

#include <iostream>
#include <cmath>

#include "xPowN.hpp"
#include "xPowN_opt.hpp"

int main()
{
  std::cout << "xPowN test starts ...\n";

  std::cout << "c-function: 3^11 = " << pow(3,11) << std::endl;
  std::cout << "TMP: 3^11 = " << Pow<11>::pow(3.) << std::endl;
  std::cout << "TMP optimized: 3^11 = " << Pow1<11>::pow(3.) << std::endl;
}

// Exercise: Check the performance of these three implementations
