#ifndef studentClass_hpp
#define studentClass_hpp

#include <string>
#include <iostream>

class Student
{
public:

  Student() : name("Student"), memory("") {}

  void call() {
    std::cout << "I am " << name << " and I remember " << memory << std::endl;
  }

  std::string name;
  std::string memory;
};

class Steve : public Student
{
public:
  Steve() { name = "Steve"; property = 2;}
  
  void learn() 
  {
    memory = "nothing";
  }

  int property;
};

class Yousseff : public Student
{
public:
  Yousseff() { name = "Yousseff"; property = 3.14;}
  
  void learn() 
  {
    memory = "everything";
  }
  
  double property;
};

class Class: public Steve, public Yousseff
{
public:

  void learn() {
    Steve::learn();
    Yousseff::learn();
  }
  
  void call() {
    Steve::call();
    Yousseff::call();
  }  
};


#endif
