#ifndef studentClass_TMP_hpp
#define studentClass_TMP_hpp

#include <string>
#include <iostream>
#include <tuple>

#include "studentClass.hpp"
#include "tupleHelper.hpp"


template<class T>
struct Learn
{
  template<class SC>
  static void doIt(SC& sc) {
    sc.T::learn();
  }
};


template<class T>
struct Call
{
  template<class SC>
  static void doIt(SC& sc) {
    sc.T::call();
  }
};

template<class T>
struct WriteProperty
{
  template<class SC>
  static void doIt(SC& sc) {
    std::cout << sc.T::name << " : my property is " << sc.T::property << std::endl;
  }
};

template<class Tuple, std::size_t N, template<class T> class DoItClass>
struct TupleDo {
  
  template<class T>
  static void doIt(T& studentClass)
  {
    TupleDo<Tuple, N-1, DoItClass>::doIt(studentClass);
    typedef typename std::tuple_element<N-1,Tuple>::type Type;
    DoItClass<Type>::doIt(studentClass);
  }
};

template<class Tuple, template<class T> class DoItClass>
struct TupleDo<Tuple, 1, DoItClass> {
  template<class T>
  static void doIt(T& studentClass)
  {
    typedef typename std::tuple_element<0,Tuple>::type Type;
    DoItClass<Type>::doIt(studentClass);
  }
};

// lets inherit all types from the tuple:
template<class StudentTuple>
class TmpClass : public InheritFromTuple<StudentTuple>
{
public:

  // nearly as before: function calls for each type in the tuple:
  // void learn() {
  //   std::tuple_element<0,StudentTuple>::type::learn();
  //   std::tuple_element<1,StudentTuple>::type::learn();
  // }
  
  // void call() {
  //   std::tuple_element<0,StudentTuple>::type::call();
  //   std::tuple_element<1,StudentTuple>::type::call();
  // }  

  // NOW: Recursion over all tuple elements possible!
  void learn() {
    TupleDo<StudentTuple, std::tuple_size<StudentTuple>::value, Learn>::doIt(*this);
  }

  void call() {
    TupleDo<StudentTuple, std::tuple_size<StudentTuple>::value, Call>::doIt(*this);
  }

  void writeProperty() {
    TupleDo<StudentTuple, std::tuple_size<StudentTuple>::value, WriteProperty>::doIt(*this);
  }
};


#endif
