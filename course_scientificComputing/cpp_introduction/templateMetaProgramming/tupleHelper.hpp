#ifndef tupleHelper_hpp
#define tupleHelper_hpp

// Template to inherit all types from a tuple

template<class T>
class Inheritor : public T {};

template<class Type>
class InheritFromTuple;

template<class AtomicType, class... TypeList>
class InheritFromTuple<std::tuple<AtomicType, TypeList...>>
  : public InheritFromTuple<AtomicType>
  , public InheritFromTuple<std::tuple<TypeList...>>
{};

template<>
class InheritFromTuple<std::tuple<>>
{};

template<class AtomicType>
class InheritFromTuple
  : public Inheritor<AtomicType>
{};



#endif
