#ifndef xPowN_hpp
#define xPowN_hpp

// general template for powers of N
template<int N> 
struct Pow
{
  static double pow(double x) {
    return x*Pow<N-1>::pow(x);
  }
};

// recursion stopping condition
template<>
struct Pow<1> 
{
  static double pow(double x) {
    return x;
  }
};

// special N=0 treatement
template<>
struct Pow<0> 
{
  static  double pow(double x) {
    return 1;
  }
};


// for the call Pow<4>::pow(2) the compiler generates the code 2*2*2*2.
// is this perfect? No! Better would be 2^2 * 2^2 because of reusing the
// result 2^2. -> see xPowN_opt.hpp

#endif
