#ifndef xPowN_opt_hpp
#define xPowN_opt_hpp

// x^12 is better computed as x^6*x^6

// template<int N>
// struct Pow
// {
//   static double pow(double x) {
//     return Pow<N/2>::pow(x)*Pow<N/2>::pow(x);
//   }
// };

// Exect for odd N ...
// use partial specialization:

template<int N, bool odd=N%2>
struct Pow1
{
  static double pow(double x) {
    return Pow1<N/2>::pow(x)*Pow1<N/2>::pow(x);
  }
};

template<int N>
struct Pow1<N,true>
{
  static double pow(double x) {
    return x*Pow1<(N-1)/2>::pow(x)*Pow1<(N-1)/2>::pow(x);
  }
};

// the second template argument (odd) is never explicitely given by
// the user. It is there and evaluated by the compiler to be able to
// do partial spezialization.

template<>
struct Pow1<1,true>
{
  static double pow(double x) {
    return x;
  }
};

template<>
struct Pow1<0,false>
{
  static double pow(double x) {
    return 1;
  }
};

#endif
