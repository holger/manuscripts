#include <iostream>

// Introduction to templates in C++

// In C++ all variables and functions have to be declared with a
// specific type: 
// int number1;
// double number2;
// double getMaxOf(int a, double b) { ... }
// you can call: getMaxOf(number1, number2);
// However, you might later need getMaxOf(double a, double b).
// Solution: implement double getMaxOf(double a, double b)
// But what about int getMaxOf(int a, int b) -> implement it!
// Better: templates. The actual type will be specified later! 
// More precisely: The actual function will be generated (instantiated) 
// for the specific types by the compiler.

template<typename T>
T getMaxOf(T a,T b)
{
  if(a > b)
    return a;
  else
    return b;
}

// or directely with two templates? :

// template<typename T1, typename T2>
// T getMaxOf(T1 a, T2 b)
// {
//   if(a > b)
//     return a;
//   else
//     return b;
// }

// but what will be the type T? -> Wait for C++11!
// template<typename T1, typename T2>
// auto getMaxOf(T1 a, T2 b) -> decltype(a+b)

// -------------------

// Templates can also appear in classes:

template<typename T>
class Particle {
public:
  T x;
  T v;
};

// Let us use all this:

int main(int argc, char** argv)
{
  int a = 1;
  int b = 2;

  std::cerr << "getMaxOf(a,b) = " << getMaxOf(a,b) << std::endl;
  
  Particle<double> particle;
  particle.x = 1.2;
  particle.v = 2;

}
