// g++ -std=c++11 -o cours_mutex -pthread cours_mutex.cc

#include <iostream>      
#include <thread>        
#include <vector> 
#include <cmath>
#include <mutex>

std::mutex cerrMutex;

// function to show the principles of mutexes:
void doSomething(int nThreads, int threadId)
{
  // The first thread that arrives at this line of code locks the
  // mutex that coodinates the access to the cout output. All other
  // threads arriving later wait until the coutMutex has been unlocked
  // so that they can lock it.
  cerrMutex.lock();
  std::cerr << "hi, I am thread " << threadId << " of " << nThreads << std::endl;
  cerrMutex.unlock();
}

int main(int argc, char** argv) 
{
  // number of threads
  int nThreads = 4;
   
  // pointers to threads are stored in a vector
  std::vector<std::thread*> threadVec(nThreads);
  
  // creation of threads and execution of function diff
  for(int threadId = 0; threadId < nThreads; threadId++) {
    threadVec[threadId] = new std::thread(doSomething,nThreads,threadId);
  }    
  
  // wait until the threads completed the task
  for(auto thread = threadVec.begin(); thread != threadVec.end(); thread++) {
    (*thread)->join();
  } 
  
  // erase the threads
  for(auto thread = threadVec.begin(); thread != threadVec.end(); thread++) {
    delete *thread;
  } 
 
  return 0;
}
