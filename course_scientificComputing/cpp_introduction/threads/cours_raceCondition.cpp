#include <iostream>      
#include <thread>
#include <vector>
#include <atomic>

void add(int i, int& result)
//void add(int i, std::atomic<int>& result)
{
  for(int j=0;j<i;j++)
    result += j;
}

int main()
{
  std::vector<std::thread> threadVec;

  int n = 1000;

  int result = 0;
  //std::atomic<int> result(0);
  
  for(int i=0;i<n;i++)
    threadVec.push_back(std::thread(add,i,std::ref(result)));

  for(auto & th : threadVec)
    th.join();

  std::cerr << "result = " << result << std::endl;
}
