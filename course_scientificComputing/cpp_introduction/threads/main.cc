// g++ -std=c++11 -o main -pthread main.cc

#include <iostream>      
#include <thread>        
#include <vector> 
#include <cmath>

// Thread implementation of the derivative of a function
//
// All threads (processors) share the same memory
// Threads are created for a specific task (function)

// Threaded differentiation: nThreads = number of threads, threadId = id of thread
void diff(const std::vector<double>& vec, std::vector<double>& result, int nThreads, int threadId)
{
  std::cout << "thread " << threadId << " of " << nThreads << std::endl;
  // global size of the vector
  int size = vec.size() - 2;
  // part computed by the thread
  int localSize = size/nThreads;
  
  // start index for the thread in the vector
  int from = threadId*localSize+1;
  // end index
  int to = (threadId+1)*localSize+1;

  double dx = 2*M_PI/vec.size();

  int loops = 10000;
  for(int loop=0;loop<loops;loop++) 
    for(int i=from; i != to; i++)
      result[i] = (vec[i+1] - vec[i-1])/(2*dx);
  
}

int main(int argc, char** argv) 
{
  // number of threads
  int nThreads = 4;
  // number of grid points 
  int n=10;
  
  // allocation of vectors of global size
  std::vector<double> vec(n);
  std::vector<double> vecDiff(n);
  
  // initialization with a sin
  for(int i=0;i<n;i++) {
    vec[i] = sin(2*M_PI*i/n);
  }
  
  // pointers to threads are stored in a vector
  std::vector<std::thread*> threadVec(nThreads);
  
  // creation of threads and execution of function diff
  for(int threadId = 0; threadId < nThreads; threadId++) {
    threadVec[threadId] = new std::thread(diff,std::ref(vec),std::ref(vecDiff),nThreads,threadId);
  }    
  
  // wait until the threads completed the task
  for(auto thread = threadVec.begin(); thread != threadVec.end(); thread++) {
    (*thread)->join();
  } 
  
  // erase the threads
  for(auto thread = threadVec.begin(); thread != threadVec.end(); thread++) {
    delete *thread;
  } 
  
  // output
  for(int i=0;i<n;i++)
    std::cout << i << "\t" << vec[i] << "\t" << vecDiff[i] << std::endl;

  return 0;
}

// CAUTION!!! : Thread safety !

// imagine that you have an int x = 0; and now you use threads to
// increase its value (++x) (a function like diff could just do this);
// the problem: ++x consists of three operations:

// 1) fetch the variable from the memory
// 2) Increment the variable inside the arithmetic logic unit (ALU)
// 3) write the variable back to the memory

// with multithreads another thread (T2) can read the variable x while the
// first thread (T1) uses the ALU:

// T1 fetches x (x=0 in memory)
// T1 increases x (x=1 in ALU)
// T2 fetches x (x=0 in memory)
// T1 writes x (x=1 in memory)
// T2 increases x (x=1 in ALU)
// T2 writes x (x=1 in memory)

// The result is: x=1 in memory. We expected x=2.  For this one has to
// guarante that the read-modify-write operation is ATOMIC.
// -> introduction of locks by Mutex = Mutual Exclusives

// exlusive access to a variable by acquiring a mutex associated to
// the variable. Other thread have to wait until the first thread
// releases the mutex.

// no problem in our example above: each thread accesses its own
// portion of the memory.
