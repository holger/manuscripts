// thread example
#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <vector> 
#include <cmath>

void foo() 
{
  long long int counter = 0;
  std::vector<double> vec(1000);
  while(true) {
    for(int i=0;i<1000;i++) {
      vec[i] = sin(2*6.28*i);
    }
    ++counter;
    if(counter % 10000 == 0)
      std::cerr << "foo\n";
  }
}

void bar(int x)
{
  long long int counter = 0;
  std::vector<double> vec(1000);
  while(true) {
    for(int i=0;i<500;i++) {
      vec[i] = sin(2*6.28*i);
    }
    ++counter;
    if(counter % 10000 == 0)
      std::cerr << "bar\n";
  }
}

int main() 
{
  std::thread first (foo);     // spawn new thread that calls foo()
  std::cout << "first.get_id() = " << first.get_id() << std::endl;
  std::thread second (bar,0);  // spawn new thread that calls bar(0)
  std::cout << "second.get_id() = " << second.get_id() << std::endl;
    
  std::cout << "main, foo and bar now execute concurrently...\n";

  // synchronize threads:
  first.join();                // pauses until first finishes
  second.join();               // pauses until second finishes

  std::cout << "foo and bar completed.\n";

  return 0;
}
