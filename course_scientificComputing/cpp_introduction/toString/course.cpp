#include "toString.hpp"
#include "toMultiString.hpp"

#include <iostream>
#include <cstdio>
#include <functional>

// toString -----------------------------------------------------

// The problem: often one wants to write something like
// std::string filename = "particle-" + number + ".txt";
// However, + operation only for strings!

class TestClass {
public:
  void testCall(int a)
  {
    std::cout << "testCall : " << a << std::endl;
  }
};

int main(int argc, char** argv)
{
  int number = 1;
  TestClass t;
  
  std::function<void(int)> testCallF(std::bind(&TestClass::testCall,t,std::placeholders::_1));
  testCallF(78);
  
  // There are different ways to convert an int to a string:
  
  // c-way:
  char str[128];  
  sprintf(str,"%d",number);
  // Caution: 1) Length safety: size of str must be sufficiently large !!! Buffer overflow! (write behind str in memory)
  //          2) Type safecty: specifier must be correct !!! %s = pointer to character! (write somewhere in memory)

  std::cout << "number = " << str << std::endl;
  
  // Let us look on the c++ way in toString.h
  std::cout << "number = " << intToString(number) << std::endl;

  // The same with templates
  std::cout << "number = " << toString(number) << std::endl;

  // This also works for user-defined classes if the operator<< is defined!
  Position position(1,2,3);
  std::cout << "position = " << position << std::endl;

  std::cout << "multiString = " << toMultiString(number,"-",position) << std::endl;

  // write code for
  // std::string filename = toString(number,particle);
  // and
  // std::string filename = toString(number,particle,".txt");
}
