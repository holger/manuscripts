#ifndef toMultiString_hpp
#define toMultiString_hpp

#include <sstream>
#include <string>

template<typename First>
void convertToStream(std::stringstream& os, First begin)
{
  os << begin;
}  

template<typename First, typename ... Rest>
void convertToStream(std::stringstream& os, First begin, Rest... rest)
{
  os << begin;
  convertToStream(os,rest...);  
}  

template<typename ... Types>
std::string toMultiString(Types... list)
{
  std::stringstream os;

  convertToStream(os, list...);

  return os.str();  
}

#endif
