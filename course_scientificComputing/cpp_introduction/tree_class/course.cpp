#include <iostream>

int main()
{
  std::cout << "tree test starts ...\n";

  int n = 10;
  Tree* forest = new Tree[n];
  
  for(int i = 0; i < 100; i++)
    for(int t = 0; t < n; t++)
      forest[i].grow();

  for(int t = 0; t < n; t++)
    std::cout << forest[t].get_roots() << "\t" << forest[t].get_leaves() << std::endl;
  
}
