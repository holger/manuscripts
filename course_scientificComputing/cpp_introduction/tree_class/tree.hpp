#ifndef tree_hpp
#define tree_hpp

#include <iostream>
#include <cstdlib>

class Tree
{
public:

  Tree() {
    roots_ = 1;
    leaves_ = 1;
  }
  
  int get_roots() const {
    return roots_;
  }

  int get_leaves() const {
    return leaves_;
  }

  void grow() {
    if(double(rand())/RAND_MAX >= 0.5) 
      ++roots_;
    else
      ++leaves_;
  }

private:
  int roots_;
  int leaves_;
};

#endif
