// A forest is created via a tree factory class

// Compiling: g++ -std=c++11 -o course course.cpp tree.cpp treeFactoryStock.cpp
// or just
// make

#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <algorithm>
#include <map>
#include <functional>

#include "forest.hpp"
#include "gardener.hpp"

NotificationHandler notify;

int main()
{
  std::cout << "(factory powered) Fruit-forest study starts ...\n";
 
  int treeNumber = 10;

  Forest forest;
  for(int i=0;i<treeNumber/2;i++)
    forest.plantTree("tree");

  for(int i=0;i<treeNumber/2;i++)
    forest.plantTree("fruitTree");

  Gardener gardener;
  
  forest.print();
  
  for(int a = 0; a < 100; a++)
    forest.evolve();
    
  std::cout << "after 100 years:\n";
  forest.print();
}
