// class for handling a forest containing trees

#ifndef forest_hpp
#define forest_hpp

#include "treeFactory.hpp"

extern TreeFactory treeFactory;

class Forest
{
public:

  void plantTree(std::string name) {
    forestPList_.push_back(treeFactory.create(name));
  }

  void print() {
    for(auto it : forestPList_) {
      std::cout << *it << std::endl;
    }
  }

  void evolve() {
    for(auto it : forestPList_)
      it->grow();
  }
  
private:
  
  std::list<Tree*> forestPList_;
};

#endif
