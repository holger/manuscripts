#ifndef gardener_hpp
#define gardener_hpp

extern NotificationHandler notify;

class Gardener
{
public:

  Gardener() {
    auto functor = std::function<void(Tree*)>(std::bind(&Gardener::work, this, std::placeholders::_1));
    notify.add("leave", functor);
  }
  
  void work(Tree* tree) {
    if(tree->get_leaves() >= 10) {
      std::cout << "gardener is felling tree " << *tree << std::endl;
      tree->fell();
    }
  }
};

#endif
