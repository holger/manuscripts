#ifndef notificationHandler_hpp
#define notificaitonHandler_hpp

#include <map>
#include <functional>
#include <string>

//#include "tree.hpp"

class Tree;

class NotificationHandler
{
public:

  typedef std::multimap<std::string, std::function<void(Tree*)>> Map;

  void add(std::string key, std::function<void(Tree*)> callBack) {
    notifyMap_.insert(Map::value_type(key,callBack));
  }

  void exec(std::string key, Tree* tree) {
    auto range = notifyMap_.equal_range(key);
    for(auto it = range.first; it != range.second; it++)
      (it->second)(tree);
  }
  
private:

  Map notifyMap_;
  
};

#endif
