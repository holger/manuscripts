#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <algorithm>

#include "treeFactory.hpp"
#include "tree.hpp"
#include "fruitTree.hpp"
#include "forest.hpp"
#include "gardener.hpp"

TreeFactory treeFactory;
NotificationHandler<void, Tree*> notify;

Tree* createTree() { return new Tree; }
Tree* createFruitTree() { return new FruitTree; }

void fell(Tree* tree)
{
  if(tree->get_leaves() >= 10)
    std::cout << "felling tree " << *tree << std::endl;
}

int main()
{
  std::cout << "Forest study starts ...\n";

  treeFactory.registerTree("tree", createTree);
  treeFactory.registerTree("fruitTree", createFruitTree);

  int treeNumber = 10;

  Forest forest;

  for(int i=0; i < treeNumber/2; i++)
    forest.plant("tree");

  for(int i=0; i < treeNumber/2; i++)
    forest.plant("fruitTree");


  Gardener gardener;
  //  notify.add("leave",std::function<void(Tree*)>(fell));
  
  forest.print();

  int n = 100;
  for(int a = 0; a < n; a++) {
    forest.evolve();
  }

  forest.print();
}
