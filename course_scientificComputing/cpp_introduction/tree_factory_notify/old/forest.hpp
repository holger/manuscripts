#ifndef forest_hpp
#define forest_hpp

#include "tree.hpp"
#include "treeFactory.hpp"

extern TreeFactory treeFactory;

class Forest
{
public:

  void plant(std::string species) {
    std::cout << "planting a " << species << std::endl;
    treeList_.push_back(treeFactory.createTree(species));
  }
  
  int get_treeNumber() const {
    return treeList_.size();
  }

  void evolve() {
    for(Tree* tree : treeList_)
      tree->grow();
  }

  void print() const {
    for(Tree* tree : treeList_)
      std::cout << *tree << std::endl;
  }
  
private:
  std::list<Tree*> treeList_;
};

#endif
