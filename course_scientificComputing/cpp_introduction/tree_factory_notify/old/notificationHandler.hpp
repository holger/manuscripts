#ifndef notificationHandler_hpp
#define notificationHandler_hpp

#include <map>
#include <functional>

template<typename TResult, typename ...TArgs>
class NotificationHandler
{
public:

  typedef std::multimap<std::string, std::function<TResult(TArgs...)>> NotifyMap;
  
  void add(std::string key, std::function<TResult(TArgs...)> callback) {
    fieldNotifyMap_.insert(typename NotifyMap::value_type(key, callback));
  }  

  void exec(std::string key, TArgs... parameter) { 
    auto ret = fieldNotifyMap_.equal_range(key);
    for(auto it = ret.first; it != ret.second; it++)
      (it->second)(parameter...);
  }

private:

  NotifyMap fieldNotifyMap_;
};

#endif
