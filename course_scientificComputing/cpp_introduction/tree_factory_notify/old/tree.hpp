#ifndef tree_hpp
#define tree_hpp

#include <iostream>

#include "notificationHandler.hpp"

class Tree;

extern NotificationHandler<void, Tree*> notify;

class Tree
{
public:

  
  Tree() {
    leaves_ = 1;
    roots_ = 1;
  }

  virtual void grow() {
    if(double(rand())/RAND_MAX >= 0.5) {
      ++leaves_;
      notify.exec("leave", this);
    }
    else {
      ++roots_;
      notify.exec("root", this);
    }
  }

  bool operator<(Tree const& tree) {
    if(leaves_ < tree.leaves_)
      return true;
    else
      return false;
  }

  int get_leaves() const {
    return leaves_;
  }

  int get_roots() const {
    return roots_;
  }

  virtual std::ostream& print(std::ostream& of) const {
    return of << "(" << leaves_
	      << "," << roots_
	      << ")";
  }


protected:
  int leaves_;
  int roots_;
};


std::ostream& operator<<(std::ostream& of, Tree const& tree)
{
  return tree.print(of);
}


#endif
