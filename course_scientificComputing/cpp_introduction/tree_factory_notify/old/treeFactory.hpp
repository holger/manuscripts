#ifndef treeFactory_hpp
#define treeFactory_hpp

#include <string>
#include <map>
#include <functional>

#include "tree.hpp"

class TreeFactory
{
public:

  typedef std::function<Tree*(void)> CreatorCallBack;
  typedef std::map<std::string, CreatorCallBack> CallBackMap;
  
  Tree* createTree(std::string species) {
    auto it = callBackMap_.find(species);

    if(it == callBackMap_.end()) {
      std::cerr << "ERROR: unknown tree species = " << species << std::endl;
      exit(1);
    }

    return (it->second)();
  }
  
  void registerTree(std::string species, CreatorCallBack createFunction) {
    callBackMap_[species] = createFunction;
  }
  
private:

  CallBackMap callBackMap_;
};

#endif
