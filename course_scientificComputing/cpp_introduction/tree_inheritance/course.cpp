#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <algorithm>

#include "tree.hpp"
#include "fruitTree.hpp"

int main()
{
  std::cout << "(object-oriented) Forest study starts ...\n";

  int treeNumber = 10;
  std::list<Tree*> treeList;

  for(int i=0; i < treeNumber/2; i++)
    treeList.push_back(new Tree);

  for(int i=0; i < treeNumber/2; i++)
    treeList.push_back(new FruitTree);

  for(Tree* tree : treeList)
    std::cout << *tree << std::endl;

  int n = 100;
  for(int a = 0; a < n; a++) {
    for(Tree* tree : treeList)
      tree->grow();
  }

  for(Tree* tree : treeList)
    std::cout << *tree << std::endl;


}
