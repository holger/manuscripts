#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <algorithm>

#include "tree.hpp"
#include "fruitTree.hpp"
#include "forest.hpp"

NotificationHandler<void, Tree*> notify;

void fell(Tree* tree)
{
  if(tree->get_leaves() >= 10)
    std::cout << "felling tree " << *tree << std::endl;
}

int main()
{
  std::cout << "Forest study starts ...\n";

  notify.add("leave",std::function<void(Tree*)>(fell));
  
  int treeNumber = 10;

  Forest forest;
  
  for(int i=0; i < treeNumber/2; i++)
    forest.plant(new Tree);

  for(int i=0; i < treeNumber/2; i++)
    forest.plant(new FruitTree);

  forest.print();

  int n = 100;
  for(int a = 0; a < n; a++) {
    forest.evolve();
  }

  forest.print();
}
