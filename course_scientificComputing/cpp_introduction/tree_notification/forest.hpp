#ifndef forest_hpp
#define forest_hpp

#include "tree.hpp"

class Forest
{
public:

  void plant(Tree* tree) {
    treeList_.push_back(tree);
  }
  
  int get_treeNumber() const {
    return treeList_.size();
  }

  void evolve() {
    for(Tree* tree : treeList_)
      tree->grow();
  }

  void print() const {
    for(Tree* tree : treeList_)
      std::cout << *tree << std::endl;
  }
  
private:
  std::list<Tree*> treeList_;
};

#endif
