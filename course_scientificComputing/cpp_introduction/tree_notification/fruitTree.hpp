#ifndef fruitTree_hpp
#define fruitTree_hpp

#include "tree.hpp"

class FruitTree : public Tree
{
public:
  
  FruitTree() {
    fruits_ = 0;
  }

  void grow() {
    if(double(rand())/RAND_MAX >= 2./3)
      ++leaves_;
    else if(double(rand())/RAND_MAX <= 1./3)
      ++roots_;
    else
      ++fruits_;
  }

  int get_fruits() const {
    return fruits_;
  }

  std::ostream& print(std::ostream& of) const {
    return of << "(" << leaves_
	      << "," << roots_
	      << "," << fruits_
	      << ")";
  }
  

private:

  int fruits_;
};


#endif
