#ifndef notificationHandler_hpp
#define notificationHandler_hpp

#include <map>
#include <functional>

template<typename TResult, typename ...TArgs>
class NotificationHandler
{
public:
 
  void add(std::string key, std::function<TResult(TArgs...)> callback) {
    fieldNotifyMap_.insert(std::pair<std::string, std::function<TResult(TArgs...)>>(key, callback));
  }  

  void exec(std::string key, TArgs... parameter) { 
    auto ret = fieldNotifyMap_.equal_range(key);
    for(auto it = ret.first; it != ret.second; it++)
      (it->second)(parameter...);
  }  

private:

  std::multimap<std::string, std::function<TResult(TArgs...)>> fieldNotifyMap_;
};

#endif
