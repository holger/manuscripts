#include <tuple>
#include <iostream>

#include "tuple.h"

int main(int argc, char** argv)
{
  std::cout << "tuple test starts ...\n";

  std::tuple<int, float, double> tuple1;

  std::get<0>(tuple1) = 1;
  std::get<1>(tuple1) = 2.5;
  std::get<2>(tuple1) = 3.5;

  // Caution: Looping not possible: 
  // for(int i=0;i<3;i++) std::get<i>(tuple) = 1;
  
 
  auto tuple2 = std::make_tuple(1,2.5,3.5);

  // unpacking
  int a;
  float b;
  double c;
  std::tie(a,b,c) = tuple1;
  
  // concatenating
  auto tuple12 = std::tuple_cat(tuple1,tuple2);
  
  // How to print tuples?
  
  // Straight forward:
  std::cout << std::get<0>(tuple12) << ", " 
	    << std::get<1>(tuple12) << ", " 
	    << std::get<2>(tuple12) << ", "
	    << std::get<3>(tuple12) << ", " 
	    << std::get<4>(tuple12) << ", " 
	    << std::get<5>(tuple12) << std::endl;
  
  // annoying? Yes. Let us look into tuple.h
  
  typedef std::tuple<int, float, double> TUPLE;
  TuplePrinter< TUPLE, std::tuple_size<TUPLE>::value >::print(tuple1);
  std::cout << std::endl;

  // Why not print tuple12? Because we do not know explicitly its type
  // (auto). Therefore build small helper function print(...):

  print(tuple12);

  // We have for the first time seen variadic templates (in
  // template<class... Args>). Let us improve our implementation for
  // toString with this C++11 feature (see ../variadicTemplates/cours.cc).

  // We are coming back to the tuples: Now let us search a tuple for a
  // type: At which position is a float in tuple12?

  std::cout << Index<float, TUPLE>::value << std::endl;
}
