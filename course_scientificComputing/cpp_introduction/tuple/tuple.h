#ifndef tuple_h
#define tuple_h

// print tuple by recursion: Function that takes a tuple of size N and
// print the N-1th element. BUT: before it calls himself for the N-2th
// element (recursion). The call for the first element is done by
// partial specialization (see below)

template<class Tuple, std::size_t N>
struct TuplePrinter {
    static void print(const Tuple& t) 
    {
        TuplePrinter<Tuple, N-1>::print(t);
        std::cout << ", " << std::get<N-1>(t);
    }
};

// prints a tuple of length one and stopps the recursion process.
template<class Tuple>
struct TuplePrinter<Tuple, 1> {
    static void print(const Tuple& t) 
    {
        std::cout << std::get<0>(t);
    }
};

// helper function for printing tuples using variadic templates
// (templates with variable number of types):

template<class... Args> // ... denotes a variable number
void print(const std::tuple<Args...>& t) 
{
    std::cout << "(";
    TuplePrinter<decltype(t), sizeof...(Args)>::print(t);
    // or TuplePrinter<decltype(t), std::tuple_size<std::tuple<Args...>>::value>::print(t);
    std::cout << ")\n";
}

// decltype(t) represents the type of t which might not explicitly be
// known (by using auto = std::make_tuple(1,2.5,3.5);)

// sizeof...(Args) gives the size of the variable template list


// ------------------------------------------------------
// function to find index of first occurence of type in tuple 
// ------------------------------------------------------

template <class T, class Tuple>
struct Index;

// remark: std::tuple<T, Types...> defines a std::tuple that has T as
// the first type. In this case we expect Index::value to be 0!

template <class T, class... Types>
struct Index<T, std::tuple<T, Types...>> {
    static const int value = 0;
};

template <class T, class U, class... Types>
struct Index<T, std::tuple<U, Types...>> {
    static const int value = 1 + Index<T, std::tuple<Types...>>::value;
};

/* template <class T, class... Types> */
/* struct Index<T, std::tuple<T, Types...>> { */
/*   enum{ value = 0 }; */
/* }; */

/* template <class T, class U, class... Types> */
/* struct Index<T, std::tuple<U, Types...>> { */
/*   enum{ value = 1 + Index<T, std::tuple<Types...>>::value }; */
/* }; */

// or

#endif
