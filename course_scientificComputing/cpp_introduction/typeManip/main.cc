#include <iostream>

#include "typeManip.h"

int main(int argc, char** argv)
{
  Particle particle;
  particle.x = 2;
  advance(particle);

  std::cout << "particle.x = " << particle.x << std::endl;

  Particle* particleP = &particle;
  advance(particleP);
  
  std::cout << "particle.x = " << particle.x << std::endl;
}
