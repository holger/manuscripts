#ifndef typeManip_h
#define typeManip_h

// class from integer value

#include <iostream>

template <int v>
struct Int2Type
{
  enum { value = v };
};

// class from type

template <typename T>
struct Type2Type
{
  typedef T OriginalType;
};

// check whether type T is an integer

template<class T>
class IsInt
{
public:
  enum { value = false };
};

template<>
class IsInt<int>
{
public:
  enum { value = true };
};

class NullType {};

template <class U> 
struct IsPointer
{
  enum { result = false };
  typedef NullType PointeeType;
};

template <class U> 
struct IsPointer<U*>
{
  enum { result = true };
  typedef U PointeeType;
};

class Particle
{
 public:
  double x;
};

template<class PARTICLE>
void advance(PARTICLE& p) {
  advanceHelper(p, Int2Type<IsPointer<PARTICLE>::result >());
}

template<class PARTICLE>
void advanceHelper(PARTICLE& p, Int2Type<false>) 
{
std::cout << "void advanceHelper(PARTICLE p, Int2Type<false>\n";
p.x *= 2;
}

template<class PARTICLE>
void advanceHelper(PARTICLE& p, Int2Type<true>) {
std::cout << "void advanceHelper(PARTICLE p, Int2Type<true>\n";
p->x *= 2;
}


// check wheter T and U have the same type

template <typename T, typename U>
struct IsSameType
{
  enum { value = false };
};

template <typename T>
struct IsSameType<T,T>
{
  enum { value = true };
};




#endif
