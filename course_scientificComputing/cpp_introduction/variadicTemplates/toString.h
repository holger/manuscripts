#ifndef toString_h
#define toString_h

#include <string>
#include <sstream>

std::string intToString(int s1)
{
  std::ostringstream temp; // a stream like cout
  temp << s1;
  return temp.str(); // conversion to std::string
}

template<class T>
std::string toString(T s1) // Type of argument now template parameter
{
  std::ostringstream temp;
  temp << s1;
  return temp.str();
}

template<class T1, class T2>
std::string toString(T1 s1, T2 s2) // Type of argument now template parameter
{
  std::ostringstream temp;
  temp << s1 << s2;
  return temp.str();
}

template<class T1, class T2, class T3>
std::string toString(T1 s1, T2 s2, T3 s3) // Type of argument now template parameter
{
  std::ostringstream temp;
  temp << s1 << s2 << s3;
  return temp.str();
}


// ------------------------------------------------------------------

class Position
{

public:
  Position(double xx, double yy, double zz) : x(xx), y(yy), z(zz) {}

  double x;
  double y;
  double z;
};

// cannot be member function of Particle! Why? operator<< can only be member
// function of class ostream because it modifies the stream object (std::ostream&).
// In Matrix a; a *= 2 the *= operator can only be of the class Matrix.
// However, we cannot change std::ostream!
std::ostream& operator<<(std::ostream &of, Position p) {
  of << "(" << p.x << "," << p.y << "," << p.z << ")";
  return of;
}

#endif
