#ifndef vector_multiTemplate_h
#define vector_multiTemplate_h

#include <cmath>
#include <cstring>
#include <cassert>
#include <iostream>
#include <cstdlib>

class 

class IndexCheck
{
public:
  void check(int i, int size) {
    if(i < 0 || i > size -1 ) {
      std::cerr << "index check failed for i = " 
		<< i 
		<< " : size = " 
		<< size << std::endl;
      exit(1);
    }
  }
};

class IndexNoCheck
{
public:
  void check(int i, int size) {}
};

template<typename T, class INDEXCHECK>
class Vector : public INDEXCHECK
{
public:
  Vector();
  Vector(int n);
  Vector(const Vector& vector);
  ~Vector();  

  int size() const;

  T operator[](int n) const;
  T& operator[](int n);

  Vector& operator=(const Vector& vector);
  template<typename TYPE>  Vector& operator=(TYPE value);
  
private:
  T* data_;
  int size_;
};

// definitions:

template<typename T, class INDEXCHECK>
Vector<T,INDEXCHECK>::Vector()
{
  data_ = NULL;
  size_ = 0;
}

template<typename T, class INDEXCHECK>
Vector<T,INDEXCHECK>::Vector(int n)
{
  size_ = n;
  data_ = new T[n];
}

template<typename T, class INDEXCHECK>
Vector<T,INDEXCHECK>::Vector(const Vector& vector)
{
  size_ = vector.size_;
  data_ = new T[size_];

  this->operator=(vector);
}

template<typename T, class INDEXCHECK>
Vector<T,INDEXCHECK>::~Vector()
{
  delete [] data_;
}

template<typename T, class INDEXCHECK>
inline int Vector<T,INDEXCHECK>::size() const
{
  return size_;
}

template<typename T, class INDEXCHECK>
inline T Vector<T,INDEXCHECK>::operator[](int i) const
{
  this->check(i,size_);
  return value(data_,i);
}

template<typename T, class INDEXCHECK>
inline T& Vector<T,INDEXCHECK>::operator[](int i)
{
  this->check(i,size_);
  return value(data_,i);
}

template<typename T, class INDEXCHECK>
Vector<T,INDEXCHECK>& Vector<T,INDEXCHECK>::operator=(const Vector& vector)
{
  std::memcpy(data_,vector.data_,size_*sizeof(T));

  return *this;
}

template<typename T, class INDEXCHECK>
template<typename TYPE>  
Vector<T,INDEXCHECK>& Vector<T,INDEXCHECK>::operator=(TYPE value)
{
  for(int i=0;i<size_;i++)
    data_[i] = value;
}

template<typename T>
void Vector<T>::resize(int n)
{
  if(size_ < n) {
    delete [] data_;
    data_ = new T[n];
    size_ = n;
  }
  else {}
}

  
#endif
