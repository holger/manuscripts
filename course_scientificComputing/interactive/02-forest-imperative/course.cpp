#include <iostream>
#include <cstdlib>

void grow_tree(int& leaves, int& roots)
{
  if(double(rand())/RAND_MAX >= 0.5)
    ++leaves;
  else
    ++roots;
}

int main()
{
  std::cout << "Forest study starts ..." << std::endl;

  int treeNumber = 10;
  int* leavesArr = new int[treeNumber];
  int* rootsArr = new int[treeNumber];

  for(int tree=0; tree<treeNumber; tree++) {
    leavesArr[tree] = 1;
    rootsArr[tree] = 1;
  }

  for(int tree=0; tree<treeNumber; tree++) {
    std::cout << "tree " << tree
	      << " = " << leavesArr[tree]
	      << "\t" << rootsArr[tree] << std::endl;
  }
  
  int n = 100;
  for(int a = 0; a < n; a++) {
    for(int tree=0; tree<treeNumber; tree++) {
      grow_tree(leavesArr[tree], rootsArr[tree]);
    }
  }

  std::cout << "after 100 years:" << std::endl;
  for(int tree=0; tree<treeNumber; tree++) {
    std::cout << "tree " << tree
	      << " = " << leavesArr[tree]
	      << "\t" << rootsArr[tree] << std::endl;
  }
}
