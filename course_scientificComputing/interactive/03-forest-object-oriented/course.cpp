#include <iostream>
#include <cstdlib>

class Tree
{
public:

  Tree() {
    leaves_ = 1;
    roots_ = 1;
  }
  
  void grow() {
    if(double(rand())/RAND_MAX >= 0.5)
      ++leaves_;
    else
      ++roots_;
  }

  int get_leaves() const {
    return leaves_;
  }

  int get_roots() const {
    return roots_;
  }

private:
  int leaves_;
  int roots_;
};

class TreeArray
{
public:
  TreeArray(int treeNumber) {
    treeNumber_ = treeNumber;
    treeArr_ = new Tree[treeNumber];
  }

  Tree get_tree(int id) const
  {
    if(id < 0 || id >= treeNumber_) {
      std::cout << "ERROR ...\n";
      exit(0);
    }
    return treeArr_[id];
  }
  
private:
  int treeNumber_;
  Tree* treeArr_;
};

int main()
{
  std::cout << "(object-oriented) Forest study starts ...\n";

  int treeNumber = 10;
  Tree* treeArr = new Tree[treeNumber];

  for(int tree=0; tree<treeNumber; tree++) {
    std::cout << "tree " << tree
	      << " = " << treeArr[tree].get_leaves() << "\t"
	      << treeArr[tree].get_roots() << std::endl;
      }

  int n = 100;
  for(int a = 0; a < n; a++) {
    for(int tree=0; tree<treeNumber; tree++) {
      treeArr[tree].grow();
    }
  }

  std::cout << "after 100 years:" << std::endl;
  for(int tree=0; tree<treeNumber; tree++) {
    std::cout << "tree " << tree
	      << " = " << treeArr[tree].get_leaves() << "\t"
	      << treeArr[tree].get_roots() << std::endl;
  }
}
