#include <iostream>
#include <cstdlib>
#include <vector>

class Tree
{
public:

  Tree() {
    leaves_ = 1;
    roots_ = 1;
  }

  void grow() {
    if(double(rand())/RAND_MAX >= 0.5)
      ++leaves_;
    else
      ++roots_;
  }

  int get_leaves() const {
    return leaves_;
  }

  int get_roots() const {
    return roots_;
  }

private:
  int leaves_;
  int roots_;
};

int main()
{
  std::cout << "(object-oriented) Forest study starts ...\n";

  int treeNumber = 10;
  std::vector<Tree> treeVec(treeNumber);
  
  for(int tree=0; tree<treeNumber; tree++) {
    std::cout << "tree " << tree
	      << " = " << treeVec[tree].get_leaves() << "\t"
	      << treeVec[tree].get_roots() << std::endl;
      }

  int n = 100;
  for(int a = 0; a < n; a++) {
    for(int tree=0; tree<treeNumber; tree++) {
      treeVec[tree].grow();
    }
  }

  std::cout << "after 100 years:" << std::endl;
  for(int tree=0; tree<treeNumber; tree++) {
    std::cout << "tree " << tree
	      << " = " << treeVec[tree].get_leaves() << "\t"
	      << treeVec[tree].get_roots() << std::endl;
  }
}
