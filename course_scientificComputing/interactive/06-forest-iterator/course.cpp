#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <algorithm>

#include "tree.hpp"

int main()
{
  std::cout << "(object-oriented) Forest study starts ...\n";

  int treeNumber = 10;
  std::vector<Tree> treeVec(treeNumber);
  
  for(int tree=0; tree<treeVec.size(); tree++) {
    std::cout << "tree " << tree
	      << " = " << treeVec[tree].get_leaves() << "\t"
	      << treeVec[tree].get_roots() << std::endl;
      }
  
  int n = 100;
  for(int a = 0; a < n; a++) {
    for(int tree=0; tree<treeNumber; tree++) {
      treeVec[tree].grow();
    }
  }

  std::cout << "after 100 years:" << std::endl;
  for(int tree=0; tree<treeVec.size(); tree++) {
    std::cout << "tree " << tree
	      << " = " << treeVec[tree].get_leaves() << "\t"
	      << treeVec[tree].get_roots() << std::endl;
  }

  // Tree with a pointer
  Tree* treeP = new Tree;
  std::cout << "treeP = " << treeP << std::endl;
  std::cout << "treeP->get_leaves() = " << treeP->get_leaves() << std::endl;

  // example with std::list
  std::list<Tree> treeList;
  for(int i=0;i<treeNumber;i++)
    treeList.push_back(Tree());

  for(int a = 0; a < n; a++) {
    auto treeIt = treeList.begin();    
    for(int i=0;i<treeList.size();i++) {
      treeIt->grow();
      treeIt++;
    }
  }
  
  std::cout << "forest with a list :\n";
  int counter = 0;
  for(auto treeIt = treeList.begin(); treeIt != treeList.end();treeIt++) {
    std::cout << "tree " << counter
    	      << " = " << treeIt->get_leaves() << "\t"
    	      << treeIt->get_roots() << std::endl;
    ++counter;
  }

  std::cout << "biggest tree = " << std::max_element(treeList.begin(), treeList.end())->get_leaves()
	    << std::endl;
  
}
