#ifndef tree_hpp
#define tree_hpp

class Tree
{
public:

  Tree() {
    leaves_ = 1;
    roots_ = 1;
  }

  void grow() {
    if(double(rand())/RAND_MAX >= 0.5)
      ++leaves_;
    else
      ++roots_;
  }
  
  bool operator<(Tree const& tree) {
    if(leaves_ < tree.leaves_)
      return true;
    else
      return false;
  }

  int get_leaves() const {
    return leaves_;
  }

  int get_roots() const {
    return roots_;
  }

private:
  int leaves_;
  int roots_;
};

#endif
