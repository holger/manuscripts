#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <algorithm>

#include "fruitTree.hpp"

int main()
{
  std::cout << "(object-oriented) Fruit-forest study starts ...\n";

  int treeNumber = 10;
  std::list<Tree*> forestPList;

  for(int i=0;i<treeNumber/2;i++)
    forestPList.push_back(new Tree);

  for(int i=0;i<treeNumber/2;i++)
    forestPList.push_back(new FruitTree);

  for(auto it : forestPList) {
    std::cout << *it << std::endl;
  }

  for(int a = 0; a < 100; a++)
    for(auto it : forestPList)
      it->grow();

  std::cout << "after 100 years:\n";

  for(auto it : forestPList) {
    std::cout << *it << std::endl;
  }
}
