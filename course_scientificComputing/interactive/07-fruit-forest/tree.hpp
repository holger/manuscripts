#ifndef tree_hpp
#define tree_hpp

class Tree
{
public:

  Tree() {
    std::cout << "Tree::Tree()\n";
    leaves_ = 1;
    roots_ = 1;
  }

  virtual void grow() {
    std::cout << "Tree::grow()\n";
    if(double(rand())/RAND_MAX >= 0.5)
      ++leaves_;
    else
      ++roots_;
  }
  
  bool operator<(Tree const& tree) {
    if(leaves_ < tree.leaves_)
      return true;
    else
      return false;
  }

  int get_leaves() const {
    return leaves_;
  }

  int get_roots() const {
    return roots_;
  }

  virtual std::ostream& print(std::ostream& os) const {
    os << leaves_ << "\t" << roots_;
  }
  
protected:
  int leaves_;
  int roots_;
};

std::ostream& operator<<(std::ostream& os, Tree const& tree)
{
  return tree.print(os);
}
#endif
