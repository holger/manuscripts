#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <algorithm>
#include <map>
#include <functional>

#include "forest.hpp"



int main()
{
  std::cout << "(object-oriented) Fruit-forest study starts ...\n";
 
  int treeNumber = 10;

  Forest forest;
  for(int i=0;i<treeNumber/2;i++)
    forest.plantTree("tree");

  for(int i=0;i<treeNumber/2;i++)
    forest.plantTree("fruitTree");

  forest.print();
  
  for(int a = 0; a < 100; a++)
    forest.evolve();
    
  std::cout << "after 100 years:\n";
  forest.print();
}
