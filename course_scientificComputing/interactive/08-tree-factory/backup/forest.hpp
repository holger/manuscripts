#ifndef forest_hpp
#define forest_hpp

#include "forestFactory.hpp"

extern ForestFactory forestFactory;

class Forest
{
public:

  void plantTree(std::string name) {
    forestPList_.push_back(forestFactory.create(name));
  }

  void print() {
    for(auto it : forestPList_) {
      std::cout << *it << std::endl;
    }
  }

  void evolve() {
    for(auto it : forestPList_)
      it->grow();
  }
  
private:
  
  std::list<Tree*> forestPList_;
};

#endif
