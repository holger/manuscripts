#ifndef tree_hpp
#define tree_hpp

#include <iostream>

class Tree
{
public:

  Tree() {
    leaves_ = 1;
    roots_ = 1;
  }

  virtual void grow() {
    if(double(rand())/RAND_MAX >= 0.5)
      ++leaves_;
    else
      ++roots_;
  }
  
  bool operator<(Tree const& tree) {
    if(leaves_ < tree.leaves_)
      return true;
    else
      return false;
  }

  int get_leaves() const {
    return leaves_;
  }

  int get_roots() const {
    return roots_;
  }

  virtual std::ostream& print(std::ostream& os) const {
    os << leaves_ << "\t" << roots_;
  }
  
protected:
  int leaves_;
  int roots_;
};

std::ostream& operator<<(std::ostream& os, Tree const& tree);

#endif
