#ifndef treeFactory_hpp
#define treeFactory_hpp

#include <functional>

#include "tree.hpp"

class TreeFactory
{
public:
  Tree* create(std::string name) {
    auto it = creationMap_.find(name);
    
    if(it == creationMap_.end()) {
      std::cerr << "ERROR: unknown tree species = " << name << std::endl;
      exit(1);
    }

    return (it->second)();
  }

  bool registerTree(std::string name, std::function<Tree*()> function) {
    creationMap_[name] = function;
  }
  
private:
  
  std::map<std::string, std::function<Tree*()>> creationMap_;
};

#endif
