#include <iostream>
#include <map>

#include "forestFactory.hpp"
#include "tree.hpp"
#include "fruitTree.hpp"

ForestFactory forestFactory;

Tree* createTree()
{
  std::cout << "createTree()\n";
  return new Tree;
}

Tree* createFruitTree()
{
  std::cout << "createFruitTree()\n";
  return new FruitTree;
}

bool treeRegistered = forestFactory.registerTree("tree", createTree);
bool fruitTreeRegistered = forestFactory.registerTree("fruitTree", createFruitTree);
 
