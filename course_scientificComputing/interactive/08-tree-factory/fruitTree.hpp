#ifndef fruitTree_hpp
#define fruitTree_hpp

#include "tree.hpp"

class FruitTree : public Tree
{
public:

  FruitTree() {
    fruits_ = 0;
  }

  void grow() {
    if(double(rand())/RAND_MAX >= 2./3)
      ++leaves_;
    else if(double(rand())/RAND_MAX <= 1./3)
      ++roots_;
    else
      ++fruits_;
  }

  
  int get_fruits() const {
    return fruits_;
  }
  
  std::ostream& print(std::ostream& os) const {
    os << leaves_ << "\t" << roots_ << "\t" << fruits_;
    return os;
  }

private:

  int fruits_;
};

std::ostream& operator<<(std::ostream& os, FruitTree const& tree)
{
  os << tree.get_leaves() << "\t" << tree.get_roots() << "\t" << tree.get_fruits();
  return os;
}

#endif
