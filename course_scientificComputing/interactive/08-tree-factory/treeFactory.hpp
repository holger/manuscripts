// class for creating tree species from a string

// species have to be registered (by registerTree) that is done in
// treeFactoryStock.cpp

#ifndef treeFactory_hpp
#define treeFactory_hpp

#include <functional>

#include "tree.hpp"

class TreeFactory
{
public:
  Tree* create(std::string name) {
    auto it = creationMap_.find(name);
    
    if(it == creationMap_.end()) {
      std::cerr << "ERROR: unknown tree species = " << name << std::endl;
      exit(1);
    }

    return (it->second)();
  }

  bool registerTree(std::string name, std::function<Tree*()> function) {
    creationMap_[name] = function;
    return true;
  }
  
private:
  
  std::map<std::string, std::function<Tree*()>> creationMap_;
};

#endif
