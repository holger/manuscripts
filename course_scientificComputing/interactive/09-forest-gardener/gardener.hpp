#ifndef gardener_hpp
#define gardener_hpp

#include "tree.hpp"

extern NotificationHandler notify;


class Gardener
{

public:

  Gardener() {
    auto function = std::function<void(Tree*)>(std::bind(&Gardener::work, this, std::placeholders::_1));
    notify.add("leave", function);
  }
  
  void work(Tree* tree) {
    if(tree->get_leaves() > 10)
      tree->fell();
  }
};

#endif
