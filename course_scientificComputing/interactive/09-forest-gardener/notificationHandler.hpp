#ifndef notificationHandler_hpp
#define notificationHandler_hpp

#include <map>
#include <functional>

class Tree;

class NotificationHandler
{
public:

  typedef std::multimap<std::string, std::function<void(Tree*)>> Map;
  
  void exec(std::string incident, Tree* tree) {
    auto range = notifyMap_.equal_range(incident);

    for(auto it = range.first; it != range.second; it++)
      (it->second)(tree);
    }

  void add(std::string incident, std::function<void(Tree*)> function) {
    notifyMap_.insert(Map::value_type(incident,function));		      
  }

private:
 Map notifyMap_;
};

#endif
