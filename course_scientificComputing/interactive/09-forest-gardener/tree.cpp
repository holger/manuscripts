#include "tree.hpp"

NotificationHandler notify;

std::ostream& operator<<(std::ostream& os, Tree const& tree)
{
  return tree.print(os);
}
