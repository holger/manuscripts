#ifndef tree_hpp
#define tree_hpp

#include <iostream>

#include "notificationHandler.hpp"

extern NotificationHandler notify;

class Tree
{
public:

  Tree() {
    leaves_ = 1;
    roots_ = 1;
  }

  virtual void grow() {
    if(double(rand())/RAND_MAX >= 0.5) {
      ++leaves_;
      notify.exec("leave", this);
    }
    else {
      ++roots_;
      notify.exec("roots", this);
    }
  }
  
  bool operator<(Tree const& tree) {
    if(leaves_ < tree.leaves_)
      return true;
    else
      return false;
  }

  int get_leaves() const {
    return leaves_;
  }

  int get_roots() const {
    return roots_;
  }

  void fell() {
    leaves_ = 0;
  }

  virtual std::ostream& print(std::ostream& os) const {
    os << leaves_ << "\t" << roots_;
    return os;
  }
  
protected:
  int leaves_;
  int roots_;
};

std::ostream& operator<<(std::ostream& os, Tree const& tree);

#endif
