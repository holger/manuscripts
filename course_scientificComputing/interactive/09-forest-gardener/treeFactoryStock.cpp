#include <iostream>
#include <map>

#include "treeFactory.hpp"
#include "tree.hpp"
#include "fruitTree.hpp"

TreeFactory treeFactory;

Tree* createTree()
{
  return new Tree;
}

Tree* createFruitTree()
{
  return new FruitTree;
}

bool treeRegistered = treeFactory.registerTree("tree", createTree);
bool fruitTreeRegistered = treeFactory.registerTree("fruitTree", createFruitTree);
 
