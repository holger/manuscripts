// g++ -o course course.cpp -std=c++11 -O2 -ftree-vectorize -fopt-info-vec-missed
// g++ -o course course.cpp -std=c++11 -O2 -ftree-vectorize -fopt-info-vec

#include <iostream>
#include <list>
#include <vector>
#include <cstdlib>
#include <chrono>

#include "particle.hpp"

int main(int argc, char** argv)
{

  const int pSize = 0;
  
  if(argc < 3) {
    std::cout << "ERROR !!!! : \n";
    std::cout << "argc = " << argc << " < 3 "<< std::endl;
    for(int i=0; i< argc; i++)
      std::cout << "argv[" << i << "] = " << argv[i] << std::endl;

    std::cout << "./main <particleNumber> <loop>\n";
    exit(1);
  }
    
  int particleNumber = atoi(argv[1]);
  int loop = atoi(argv[2]);

  std::cout << "particleNumber = " << particleNumber << std::endl;
  std::cout << "loop = " << loop << std::endl;

  float dt = 0.1;
  
  std::list<Particle<pSize>> pList;
  for(int i=0;i<particleNumber;i++)
    pList.push_back(Particle<pSize>());

  std::chrono::time_point<std::chrono::system_clock> start, end;
  
  start = std::chrono::system_clock::now();
  for(int l = 0; l < loop; l++) {
    //    for(std::list<Particle<pSize>>::iterator it = pList.begin(); it != pList.end(); it++)
    //    it->x += dt* it->v;
    for(auto &p : pList) 
      p.x += dt*p.v;
  }
  end = std::chrono::system_clock::now();
 
  std::chrono::duration<double> elapsed_seconds = end-start;

  std::cout << "elapsed time list : " << elapsed_seconds.count() << "s\n";

  std::vector<Particle<pSize>> pVec;
  for(int i=0;i<particleNumber;i++)
    pVec.push_back(Particle<pSize>());

  start = std::chrono::system_clock::now();
  for(int l = 0; l < loop; l++) {
    // for(int i=0;i<pVec.size();i++)
    //   pVec[i].x += dt*pVec[i].v;
      
    for(auto &p : pVec) 
      p.x += dt*p.v;
  }
  end = std::chrono::system_clock::now();
 
  elapsed_seconds = end-start;

  std::cout << "elapsed time vector (AoS) : " << elapsed_seconds.count() << "s\n";

  std::vector<float> pX(particleNumber);
  std::vector<float> pV(particleNumber);
  std::vector<float> pX1(particleNumber);
  std::vector<float> pV1(particleNumber);
  for(int i=0;i<particleNumber;i++) {
    pX[i] = 0.1;
    pV[i] = 0.01;
  }

  start = std::chrono::system_clock::now();
  for(int l = 0; l < loop; l++) {
    for(int i=0;i<particleNumber;i++)
      pX[i] += dt*pV[i];
  }
  end = std::chrono::system_clock::now();
 
  elapsed_seconds = end-start;

  std::cout << "elapsed time imperative (SoA) : " << elapsed_seconds.count() << "s\n";
}
