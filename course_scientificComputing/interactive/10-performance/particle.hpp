#ifndef particle_hpp
#define particle_hpp

template<int SIZE>
class Particle
{
public:
  Particle() {
    x = 0.1;
    v = 0.01;
  }
  
  float x;
  float v;

  float temp[SIZE];
};

#endif
