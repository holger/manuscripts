#include <iostream>
#include <thread>
#include <vector>

void print(int thNumber)
{
  std::cout << "hi I am thread " << thNumber << std::endl;
}

int main(int argc, char** argv)
{
  std::cerr << "Thread test starts ...\n";

  int threadNumber = 10;
  std::vector<std::thread> thVec;

  for(int i=0; i < threadNumber; i++) {
    thVec.push_back(std::thread(print, i));
  }
  
  for(auto & th : thVec)
    th.join();
}

// CAUTION!!! : Thread safety !

// imagine that you have an int x = 0; and now you use threads to
// increment its value (++x) (a function like diff could just do this);
// the problem: ++x consists of three operations:

// 1) fetch the variable from the memory
// 2) Increment the variable inside the arithmetic logic unit (ALU)
// 3) write the variable back to the memory

// with in a multi threaded implementation another thread (T2) could
// read the variable x while the first thread (T1) uses the ALU in the
// following sequence:

// T1 fetches x (x=0 in memory)
// T1 increases x (x=1 in ALU)
// T2 fetches x (x=0 in memory)
// T1 writes x (x=1 in memory)
// T2 increases x (x=1 in ALU)
// T2 writes x (x=1 in memory)

// The result is: x=1 in memory. We expected x=2.  For this one has to
// guarante that the read-modify-write operation is ATOMIC.
// -> introduction of locks by Mutex = Mutual Exclusives

// exlusive access to a variable by acquiring a mutex associated to
// the variable. Other thread have to wait until the first thread
// releases the mutex.

// no problem in our example above: each thread accesses its own
// portion of the memory.

// Task: write multi-threaded program to make the threads write "hi, I
// am thread 0 (... n) of n threads".

