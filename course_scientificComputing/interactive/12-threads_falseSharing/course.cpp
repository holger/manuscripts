// g++ -o course course.cpp -std=c++11 -pthread

#include <iostream>
#include <thread>
#include <vector>
#include <cmath>

// function with false-sharing (very slow because of cache line conflicts)
void normL2(const std::vector<double>& vec, float& norm, int begin, int end)
// {
//   for(int i=begin; i<end;i++)
//     norm += vec[i]*vec[i];
// }

// function without false-sharing problems
void normL2(const std::vector<double>& vec, float& norm, int begin, int end)
{
  float normTmp = 0;
  for(int i=begin; i<end;i++)
    normTmp += vec[i]*vec[i];

  norm = normTmp;
}

int main(int argc, char** argv)
{
  std::cerr << "Thread diff test starts ...\n";

  if(argc < 4) {
    std::cerr << "ERROR: argc = " << argc << " < 4\n";
    std::cerr << "usage: ./course <n> <max threadNumber> <loopNumber>\n";
    exit(1);
  }

  int n = atoi(argv[1]);
  int maxThreadNumber = atoi(argv[2]);
  int loopNumber = atoi(argv[3]);
  
  std::vector<double> vec(n);
  std::vector<double> vecDiff(n);

  for(int i=0;i<n;i++)
    vec[i] = sin(2*M_PI/n*i);

  std::vector<double> speedUpVec(maxThreadNumber);
  
  std::chrono::time_point<std::chrono::system_clock> start, end;
  for(int threadNumber = 1; threadNumber <= maxThreadNumber; threadNumber++) {

    std::vector<float> normL2Local(threadNumber);

    start = std::chrono::system_clock::now();
    int nLocal = (n-2)/threadNumber;
    for(int loop = 0; loop < loopNumber; loop++) {

      std::vector<std::thread> thVec(threadNumber);
      for(int i=0;i<threadNumber;i++) 
	thVec[i] = std::thread(normL2, std::ref(vec), std::ref(normL2Local[i]), i*nLocal+1, (i+1)*nLocal+1);
      
      for(auto & th : thVec)
      	th.join();

      float norm = 0;
      for(int i=0;i<threadNumber;i++)
	norm += normL2Local[i];

      norm = sqrt(norm);
    }
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;

    speedUpVec[threadNumber-1] = elapsed_seconds.count();
    
    std::cout << "elapsed time with " << threadNumber << " threads : " << elapsed_seconds.count() << "s, number of elements = " << nLocal << "\n";
    //    std::cout << "vecDiff[3] = " << vecDiff[3] << std::endl;
  }

  for(int i=1;i<speedUpVec.size();i++)
    speedUpVec[i] = speedUpVec[0]/speedUpVec[i];

  for(auto & speed : speedUpVec)
    std::cout << &speed-&speedUpVec[0] << "\t" << speed << std::endl;
}
