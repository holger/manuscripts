// mpicxx -o course course.cpp -std=c++11
// mpirun -np 4 course <n> <loop>

#include <iostream>
//#include <vector>
#include <array>
#include <cmath>
#include <mpi.h>

// MPI implementation of the derivative of a function
// 
// Each processor has its own memory.
// Each processor reads each line of the code for its own.
// Communication between processes by MPI functions

const int n = 1000000;

int main(int argc, char** argv)
{
  // initialize MPI with command line arguments
  MPI_Init(&argc, &argv);
  
  std::cerr << "starting mpi test ...\n";

  if(argc < 3) {
    std::cerr << "ERROR : argc = " << argc << " < 3\n";
    std::cerr << "usage : course <n> <loop>\n";
    exit(0);
  }

  int commSize; // id of process
  int commRank; // number of processes

  MPI_Comm_size(MPI_COMM_WORLD, &commSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &commRank);

  std::cerr << "hello world from rank " << commRank << " of " << commSize << " ranks" << std::endl;

  // global size of vector
  //  int n = atoi(argv[1]);
  // number of loops
  int loop = atoi(argv[2]);
  // local size of vector
  int nLocal = n/commSize;
    
  std::array<std::array<float,3>,n> vec;
  std::array<std::array<float,3>,n> vecDiff;
  
  for(int i=0;i<nLocal;i++) {
    vec[i][0] = sin(2*M_PI/n*(i));
    vec[i][1] = sin(2*M_PI/n*(i));
    vec[i][2] = sin(2*M_PI/n*(i));
  }
  
  for(int i=0;i<loop;i++) {
    for(int j=0;j<nLocal-1;j++) {
      vecDiff[j][0] = vec[j+1][0] - vec[j][0];
      vecDiff[j][1] = vec[j+1][1] - vec[j][1];
      vecDiff[j][2] = vec[j+1][2] - vec[j][2];
    }
    // exchange of boundary values:
    float neighboorValue;
    int tag = 1;
    MPI_Status status;

    if(commRank == 0)
      MPI_Send(&vec[0][0], 1, MPI_FLOAT, commSize-1, tag, MPI_COMM_WORLD);
    else
      MPI_Send(&vec[0][0], 1, MPI_FLOAT, commRank-1, tag, MPI_COMM_WORLD);
    
    if(commRank == commSize - 1)
      MPI_Recv(&neighboorValue, 1, MPI_FLOAT, 0, tag, MPI_COMM_WORLD, &status);
    else
      MPI_Recv(&neighboorValue, 1, MPI_FLOAT, commRank+1, tag, MPI_COMM_WORLD, &status);
	     
    vecDiff[nLocal-1][0] = neighboorValue - vec[nLocal-1][0];
  }

  // for(int i=0; i<nLocal;i++)
  //   std::cout << i+localStart << "\t" << vec[i] << "\t" << vecDiff[i] << std::endl;

  //  int i = 13;
  // for(int i=0; i<n;i++)
  //   std::cout << i << "\t" << vec[i] << "\t" << vecDiff[i] << std::endl;
  
  MPI_Finalize();
}
