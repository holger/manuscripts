// mpicxx -o course course.cpp -std=c++11
// mpirun -np 4 course <n> <loop>

#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include <CL/sycl.hpp>

// SYCL Code

//const int n = 100000;

int main(int argc, char** argv)
{
  std::cerr << "starting sycl test ...\n";

  if(argc < 3) {
    std::cerr << "ERROR : argc = " << argc << " < 3\n";
    std::cerr << "usage : course <n> <loop>\n";
    exit(0);
  }

  // global size of vector
  int n = atoi(argv[1]);
  // number of loops
  int loop = atoi(argv[2]);

  std::vector<std::array<float,3>> vec(n);
  std::vector<std::array<float,3>> vecDiff(n);
  //std::array<std::array<float,n>,3> vec;
  //std::array<std::array<float,n>,3> vecDiff;
  
  for(int i=0;i<n;i++) {
    vec[0][i] = sin(2*M_PI/n*(i));
    vec[1][i] = sin(2*M_PI/n*(i));
    vec[2][i] = sin(2*M_PI/n*(i));
    // vec[i][0] = sin(2*M_PI/n*(i));
    // vec[i][1] = sin(2*M_PI/n*(i));
    // vec[i][2] = sin(2*M_PI/n*(i));
  }

  sycl::range<2> vec_range{size_t(n),3};
  sycl::range<2> vecDiff_range{size_t(n-1),3};
  
  auto platforms = sycl::platform::get_platforms();
  
  for (auto &platform : platforms) {
    
    std::cout << "Platform: "
	      << platform.get_info<sycl::info::platform::name>()
	      << std::endl;
    
    
    auto devices = platform.get_devices();
    for (auto &device : devices ) {
      std::cout << "  Device: "
		<< device.get_info<sycl::info::device::name>()
		<< std::endl;
    }
  }
  
  sycl::cpu_selector device_selector;
  sycl::queue d_queue(device_selector);
  
  std::cout << "using device : " << d_queue.get_device().get_info<sycl::info::device::name>() << std::endl;

  auto R = sycl::range<2>(n,3);
  sycl::buffer<float, 2>  b_vec((float*)(vec.data()), R);
  sycl::buffer<float, 2>  b_vecDiff((float*)(vecDiff.data()), R);

  for(int i=0;i<loop;i++) {
    d_queue.submit([&](sycl::handler &cgh) {
	auto ab_vec = b_vec.get_access<sycl::access::mode::read>(cgh);
	auto ab_vecDiff = b_vecDiff.get_access<sycl::access::mode::write>(cgh);
	
	cgh.parallel_for<class ex1>(vecDiff_range,[=](sycl::id<2> idx) {
	    ab_vecDiff[idx[0]][idx[1]] = ab_vec[idx[0]+1][idx[1]] - ab_vec[idx[0]][idx[1]];
	  });
	
      });
  }
  b_vecDiff.get_access<sycl::access::mode::read>();

  
    // for(int j=0;j<nLocal-1;j++) 
    //   vecDiff[j] = vec[j+1] - vec[j];
    
    // // exchange of boundary values:
    // float neighboorValue;
    // int tag = 1;
    // MPI_Status status;

    // if(commRank == 0)
    //   MPI_Send(&vec[0], 1, MPI_FLOAT, commSize-1, tag, MPI_COMM_WORLD);
    // else
    //   MPI_Send(&vec[0], 1, MPI_FLOAT, commRank-1, tag, MPI_COMM_WORLD);
    
    // if(commRank == commSize - 1)
    //   MPI_Recv(&neighboorValue, 1, MPI_FLOAT, 0, tag, MPI_COMM_WORLD, &status);
    // else
    //   MPI_Recv(&neighboorValue, 1, MPI_FLOAT, commRank+1, tag, MPI_COMM_WORLD, &status);
	     
    // vecDiff[nLocal-1] = neighboorValue - vec[nLocal-1];

    //  int i = 13;
  // for(int i=0; i<n;i++)
  //   std::cout << i << "\t" << vec[i] << "\t" << vecDiff[i] << std::endl;
  
}


