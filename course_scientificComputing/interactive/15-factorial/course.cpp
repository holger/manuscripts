#include <iostream>

// Template that allows to compute the factorial at compile time.

template<size_t n>
class Factorial
{
public:
  static const size_t value = n*Factorial<n-1>::value;
};

template<>
class Factorial<1>
{
public:
  static const size_t value = 1;
};

int main()
{
  std::cerr << "24! = " << Factorial<24>::value << std::endl;

  // Factorial<3>::value evaluates recursively to
  
  //  Factorial<3>::value = 3*Factorial<2>::value
  //  Factorial<2>::value = 2*Factorial<1>::value
  //  Factorial<1>::value = 1;
}
