#include <iostream>
#include <cmath>
#include <chrono>
#include <vector>

// run-time function

double power(double number, int exponent)
{
  double value = number;
  for(int i=1;i<exponent;i++) {
    value = value*number;
  }

  return value;
}

// Template that allows to generate the relevant code at runtime
// The compiler replaces Pow<3>::pow(5.) par 5.*5.*5.*5.*5.

template<int N>
class Pow
{
public:
  static double pow(double x) {
    return x*Pow<N-1>::pow(x);
  }
};

template<>
class Pow<0>
{
public:
  static double pow(double x) {
    return 1;
  }
};

// Optimized template that allows to resuse for 2^4 the allready
// computed result 2^2: 2^4=2^2 * 2^2

// The second (hidden) template parameter impaire dipatches
// automatically between the computation of even and odd exponents.

template<int N, bool impaire=N%2>
class PowOpt
{
public:
  static double pow(double x) {
    return PowOpt<N/2>::pow(x)*PowOpt<N/2>::pow(x);
  }
};

template<int N>
class PowOpt<N,true>
{
public:
  static double pow(double x) {
    return x*PowOpt<(N-1)/2>::pow(x)*PowOpt<(N-1)/2>::pow(x);
  }
};

template<>
class PowOpt<0,false>
{
public:
  static double pow(double x) {
    return 1;
  }
};

int main(int argc, char** argv)
{
  // 5^6;

  if(argc < 3) {
    std::cerr << "ERROR: argc = " << argc << " < 3\n";
    std::cerr << "usage: course <n> <p>\n";
    exit(1);
  }
  
  std::cerr << "2^3 = " << power(2,3) << std::endl;
  std::cerr << "2^3 = " << std::pow(2,3) << std::endl;

  int n = atoi(argv[1]);
  const int p=20;
  std::vector<double> array(n);

  for(int i=0;i<n;i++)
    array[i] = 2;
  
  std::chrono::time_point<std::chrono::system_clock> start, end;
  
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++)
    array[i] = power(array[i],p);

  end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  std::cout << "power : elapsed time: " << elapsed_seconds.count() << "\t" << array[0] << "\n";

  // -------------------------------------

  for(int i=0;i<n;i++)
    array[i] = 2;
 
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++)
    array[i] = std::pow(array[i],p);

  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "std::pow : elapsed time: " << elapsed_seconds.count() << "\t" << array[0] << "\n";

  // -------------------------------------

  for(int i=0;i<n;i++)
    array[i] = 2;
  
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++) {
    array[i] = array[i]*array[i]*array[i]*array[i]*
      array[i]*array[i]*array[i]*array[i]*array[i]*array[i]*
      array[i]*array[i]*array[i]*array[i]*
      array[i]*array[i]*array[i]*array[i]*array[i]*array[i];
  }
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "a la main : elapsed time: " << elapsed_seconds.count() << "\t" << array[0] << "\n";

  // -------------------------------------

  for(int i=0;i<n;i++)
    array[i] = 2;
  
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++) {
    array[i] = Pow<p>::pow(array[i]);
  }
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "template : elapsed time: " << elapsed_seconds.count() << "\t" << array[0] << "\n";

  // -------------------------------------

  for(int i=0;i<n;i++)
    array[i] = 2;
  
  start = std::chrono::system_clock::now();

  for(int i=0;i<n;i++) {
    array[i] = PowOpt<p>::pow(array[i]);
  }
  
  end = std::chrono::system_clock::now();

  elapsed_seconds = end-start;
  std::cout << "template optimized: elapsed time: " << elapsed_seconds.count() << "\t" << array[0] << "\n";
}
       
