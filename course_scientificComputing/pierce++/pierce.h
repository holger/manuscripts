#ifndef pierce_h
#define pierce_h

#include "../smallLib/toString/toString.h"
#include "../smallLib/vector/vector.h"

#include "../../gnuplot-cpp/gnuplot_i.hpp"
//#include "../gnuplot-cpp/new/gnuplot-cpp-read-only/gnuplot_i.hpp"
#include <list>
#include <iostream>
#include <fstream>
#include <cmath>

class Parameter {
public:
  double dt,dx;
  int mx;
  double numberOfParticles;
  double particleDensity;
  double particleCharge;
  double alpha;
	      
  Parameter() {
    dt = 0.01;
    mx = 100;
    dx = 1./(mx-1);
    numberOfParticles = 200;
    particleDensity = numberOfParticles;
    particleCharge = -1/numberOfParticles;
    alpha = 8.; // 5. or 8.
  }
};

class Pic : public Parameter {
public: 
  typedef Vector<double> VectorType;

  VectorType phi;
  VectorType E;
  VectorType rho;
  std::list<Particle> particleList;

  double time;
  double particleOutFlux;
  int step;
  double charge;
  bool restart;
  Gnuplot g1;
		 
  Pic() {
    std::cerr << "Pic::Pic()\n";

    restart = false;
    init();
  }
    
  void init();

  void eliminate();
  
  void integrate() {
    if(restart){
      restart = false;
      init();
    }
    
    int counter = 0;
    while (counter < 100000) {

      //      write();

      singlestep();

      time += dt;
      ++step;

      particleOutFlux += 1*dt*particleDensity;
      
      while(particleOutFlux > 1) {
	particleList.push_back(Particle(particleCharge));

	charge += particleCharge;
	
	--particleOutFlux;
      }
      ++counter;
    }
    write();
  }
    
  void write();

  void move_x(double dt);

  void move_v(double dt);

  void weight_rho();

  void force();

  void poisson();
  
  void singlestep() {
    //      std::cout << "Pic::singlestep()\n";
      move_x(dt/2.);

      eliminate();
      weight_rho();
      
      poisson();
      write();
      force();
      move_v(dt);

      move_x(dt/2);
    }
  

};


#endif
