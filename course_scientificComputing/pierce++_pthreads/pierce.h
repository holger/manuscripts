#ifndef pierce_h
#define pierce_h

#include "../smallLib/toString/toString.h"
#include "../smallLib/vector/vector.h"

#include "../../gnuplot-cpp/gnuplot_i.hpp"
//#include "../gnuplot-cpp/new/gnuplot-cpp-read-only/gnuplot_i.hpp"
#include <list>
#include <iostream>
#include <fstream>
#include <cmath>

#include <pthread.h>

class Parameter {
public:
  double dt,dx;
  int mx;
  double numberOfParticles;
  double particleDensity;
  double particleCharge;
  double alpha;
	      
  Parameter() {
    dt = 0.01;
    mx = 100;
    dx = 1./(mx-1);
    numberOfParticles = 200;
    particleDensity = numberOfParticles;
    particleCharge = -1/numberOfParticles;
    alpha = 15.; // 5. or 8.
  }
};

class Pic : public Parameter {
public: 
  typedef Vector<double> VectorType;

  VectorType phi;
  VectorType E;
  VectorType rho;
  //  Vector<std::mutex> rho_mutex;
  std::list<Particle> particleList;
  //  Vector<std::mutex> rho_globalMutex;

  double time;
  double particleOutFlux;
  int step;
  double charge;
  bool restart;
  Gnuplot g1;
  int nThreads;
  
  Pic() {
    std::cerr << "Pic::Pic()\n";
    
    nThreads = 2;
    restart = false;
    init();
  }
    
  void init();

  void eliminate();
  
  void integrate() {
    if(restart){
      restart = false;
      init();
    }
    
    int counter = 0;
    while (counter < 10000) {

      //      write();

      singlestep();

      time += dt;
      ++step;

      particleOutFlux += 1*dt*particleDensity;
      
      while(particleOutFlux > 1) {
	particleList.push_back(Particle(particleCharge));

	charge += particleCharge;
	
	--particleOutFlux;
      }
      ++counter;
    }
    write();
  }
    
  void write();

  void move_x(double dt);

  void move_v(double dt);

  void weight_rho();
  void* weight_rho_mt(void* threadParameter);
  
  

  void force();

  void poisson();
  
  void singlestep() {
    //      std::cout << "Pic::singlestep()\n";
      move_x(dt/2.);

      eliminate();

      for(int i = 0; i < mx; i++)
      	rho[i] = 0;

      /* for(int i=0;i<nThreads;i++) */
      /* 	rho_globalMutex[i].lock(); */
      
      bool succes;
      pthread_t threads[nThreads];
      for(int threadId = 0; threadId < nThreads; threadId++) {
	int threadParameter[2];
	threadParameter[0] = nThreads;
	threadParameter[1] = threadId;
 
	succes = pthread_create(&threads[threadId], NULL, &Pic::weight_rho_mt,this,(void *)(&threadParameter[0]));
      /* 	threadVec[threadId] = new std::thread(&Pic::weight_rho_mt,this,nThreads,threadId); */
      }
      
      /* for(auto thread = threadVec.begin(); thread != threadVec.end(); thread++) { */
      /* 	(*thread)->join(); */
      /* } */

       
      //weight_rho();
      poisson();
      //      write();
      force();
      move_v(dt);

      move_x(dt/2);
    }
  

};


#endif
