#include "particle.h"
#include "pierce.h"

#include <iostream>

void Pic::init() {
  std::cerr << "Pic::init\n";
  
  charge = 0;
  phi.resize(mx);
  E.resize(mx);
  rho.resize(mx);
  rho_mutex.resize(mx);
  rho_globalMutex.resize(nThreads);
  
  time = 0;
  step = 0;
  particleOutFlux = 0;
  
  double deltaXParticles = 1/numberOfParticles;
  for (int i = 0; i < numberOfParticles; i++) {
    particleList.push_back(Particle(particleCharge,i*deltaXParticles));
    charge += particleCharge;
  }
}

void Pic::eliminate() {
  for(std::list<Particle>::iterator particleIt = particleList.begin(); particleIt != particleList.end(); particleIt++) 
    if(particleIt->x < 0 || particleIt->x >= 1.) {
      charge -= particleIt->qc;
      particleList.erase(particleIt);
      --particleIt;
    }
}

void Pic::move_x(double dt) {
  for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) 
    pIt->move_x(dt);
}

void Pic::move_v(double dt) {
  for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) 
    pIt->move_v(dt);
}

void Pic::weight_rho() {
  for(int i = 0; i < mx; i++) rho[i] = 0;
  
  for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
    int i = int(pIt->x/dx);
    rho[i] += pIt->qc*((i+1)*dx - pIt->x)/dx/dx;
    rho[i+1] += pIt->qc*(pIt->x - i*dx)/dx/dx;
  }
}

void Pic::weight_rho_mt(int nThreads, int threadId) 
{
  // // thread threadId begins its work ...
  // rho_globalMutex[threadId].lock();
  
  // global particle number
  int particleNumber = particleList.size();

  // approx. particle number per thread
  int localParticleNumber = int(particleNumber/nThreads);
  
  // compute set of particles treated by the thread
  int fromParticle = threadId*localParticleNumber;
  int toParticle = (threadId+1)*localParticleNumber;
  // last thread takes remaining particles (not optimal!)
  if(threadId == nThreads-1)
    toParticle = particleNumber;

  std::list<Particle>::iterator pItStart = particleList.begin();
  // move iterator to first particle of the thread
  advance(pItStart,fromParticle);

  std::list<Particle>::iterator pItEnd = particleList.begin();
  // move iterator to last particle of the thread
  advance(pItEnd,toParticle);
  
  // update of rho with the subset of particles belonging to the thread
  int counter = 0;
  for(std::list<Particle>::iterator pIt = pItStart; pIt != pItEnd; pIt++) {
    ++counter;
    int i = int(pIt->x/dx);
    // locking access to element i of rho in order to perform atomic summation
    rho_mutex[i].lock();
    rho[i] += pIt->qc*((i+1)*dx - pIt->x)/dx/dx;
    rho_mutex[i].unlock();

    // locking access to element i+1 of rho in order to perform atomic summation
    rho_mutex[i+1].lock();
    rho[i+1] += pIt->qc*(pIt->x - i*dx)/dx/dx;
    rho_mutex[i+1].unlock();
  }  
  
  // // thread has completed its work
  // rho_globalMutex[threadId].unlock();

  // // test if all threads have completed their work so that the
  // // calculation of the global rho is completed
  // for(int i=0;i<nThreads;i++) {
  //   rho_globalMutex[i].lock();
  //   rho_globalMutex[i].unlock();
  // }
}

void Pic::force() {
  //      std::cout << "Pic::force()\n";
  for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
    int i = (int)(pIt->x/dx);
    pIt->F = - ((i+1)*dx - pIt->x)/dx*E[i] - (pIt->x - i*dx)/dx*E[i+1];
  }
}

void Pic::poisson() {
  VectorType f(mx);
  VectorType gamma(mx);
  VectorType y(mx);
  double alpha2dx2 = alpha*alpha*dx*dx;
  double ddx2 = 1./2./dx;
  
  int i;
  for (i = 0; i < mx; i++) {
    f[i] = - alpha2dx2*(1 + rho[i]);
  }
  
  // determination of series a and b
  gamma[0] = y[0] = 0.;
  for (i = 1; i < mx; i++) {
    gamma[i] = -1./(2. + gamma[i-1]);
    y[i] = gamma[i]*(f[i] - y[i-1]);
  }
  
  // back substitution
  phi[mx-1] = 0.;
  for (i = mx-2; i >= 0; i--)
    phi[i] = y[i] - gamma[i]*phi[i+1];
  
  // calculation of E
  E[0] = - (-3.*phi[0] + 4.*phi[1] - phi[2])*ddx2;
  for (i = 1; i < mx-1; i++) {
    E[i] = - (phi[i+1] - phi[i-1])*ddx2;
  }
  E[mx-1] = - (3.*phi[mx-1] - 4.*phi[mx-2] + phi[mx-3])*ddx2;
}

void Pic::write() {
  //  std::cerr << "Pic::write for step = " << step << std::endl;
  
  if(step % 1 == 0) {
    // std::string filename = toString("particles_") + toString(0) + toString(".txt");
    // std::ofstream out(filename.c_str());
    
    // int counter = 1;
    // for(std::list<Particle>::const_iterator pIt=particleList.begin(); pIt != particleList.end(); pIt++)
    // 	out << counter++ << "\t" << pIt->qc << "\t" << pIt->x << "\t" << pIt->v << std::endl;
    
    // out.close();
    
    // filename = toString("rho_") + toString(step) + toString(".txt");
    // out.open(filename.c_str());
    // for(int i=0;i<mx;i++)
    // 	out << i << "\t" << rho[i] << "\t" << E[i] << std::endl;
    
    // out.close();
    
    
    int particleNumber = particleList.size();
    VectorType x(particleNumber);
    VectorType y(particleNumber);
    int pCounter = 0;
    for(std::list<Particle>::const_iterator pIt=particleList.begin(); pIt != particleList.end(); pIt++) {
      x[pCounter] = pIt->x;
      y[pCounter] = pIt->v;
      ++pCounter;
      
    }
    
    std::cout << pCounter << std::endl;
    std::string filename = toString("particles_") + toString(step) + toString(".txt");
    std::cout << "writing " << filename << std::endl;
    std::ofstream out(filename.c_str());
    int counter = 0;
    for(std::list<Particle>::const_iterator pIt=particleList.begin(); pIt != particleList.end(); pIt++) {
      out << counter << "\t" << pIt->x << "\t" << pIt->v << std::endl;
      std::cout << counter  << "\t" << pIt->x << "\t" << pIt->v << std::endl;
      ++counter;
    }
    out.close();
    
    /*   g1.reset_plot(); */
    /* g1.set_style("points").set_xlabel("x").set_ylabel("v").set_xrange(0,1).set_yrange(-2.5,2.5).plot_xy(x,y,"particles"); */

    /* std::cout << std::endl << "Press ENTER to continue..." << std::endl; */
    
    /* std::cin.clear(); */
    /* std::cin.ignore(std::cin.rdbuf()->in_avail()); */
    /* std::cin.get(); */
  }
}


int main(int argc, char** argv)
{
  std::cerr << "PIC simulation of a Pierce Diode\n";

  Pic pic;

  pic.integrate();
}
