import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

// PIC-simulation of Pierce-diode

class Particle {
    double x;
    double v;
    double F;
    double qc;

    Particle() {
	x = 0;
	v = 1.0;
	F = 0;
	qc = 0;
    }

    Particle(double q) {
	x = 0;
	v = 0.99;
	F = 0;
	qc = q;
    }

    Particle(double q, double xx) {
	x = xx;
	v = 1.0;
	F = 0;
	qc = q;
    }

    void move_x(double dt) {
	x += dt*v;
    }

    void move_v(double dt) {
	v += dt*F;
    }

}

class Parameter {
    double dt,dx;
    public double dtout;
    public int mx;
    double T;
    double alpha;

    public Parameter() {
	dt = 0.01;
	dtout = 0.01;
	mx = 100;
	dx = 1./(mx-1);
	T = 0.005;
	alpha = 10.0;
    }
    public void setAlpha(double alpha){
	this.alpha = alpha;
    }
    
}

public class Pierce
{
    static PIC pierce;

    public static void main(String[] args){
	System.out.println("Pierce starts");
	
	pierce = new PIC();

	pierce.integrate();
	
	System.out.println("Pierce ends");
    }
}

class PIC extends Parameter {
    
    double phi[];
    double E[];
    double rho[];
    Vector pl;
    Vector cl;
    double time,tinj;
    int ic_out;
    int ic_dtout;
    double charge;
    int control;
    boolean restart;

    public PIC() {
	System.out.println("Pic::Pic");
	ic_out = 1;
	ic_dtout = (int)(dtout/dt+0.5);
	restart = false;
	control = 0;
	init();
	
    }

    public void setControl(int control)
    {
	this.control = control;
    }
    
    public void init() {
	System.out.println("init");
	charge = 0;
	phi = new double [mx];
	E   = new double [mx];
	rho = new double [mx];
	pl = new Vector((int)(1.5/T));
	cl = new Vector();

	time = 0;
	for (double x = 1-T; x > -T/2; x -= T) {
	    //	    System.out.format("%f %f\n",T,x);
	    pl.addElement(new Particle(-1*T,x));
	    charge += -1*T;
	}
    }

    void eliminate() {
	for (int pi = 0; pi < pl.size();) {
	    Particle pli = (Particle)pl.elementAt(pi);
	    if (pli.x < 0 || pli.x >= 1.){
		charge -= pli.qc;
      		pl.removeElementAt(pi);
	    }
	    else pi++;
	}
    }

    void integrate() {
	if(restart){
	    restart = false;
	    init();
	}
	
	int counter = 0;
	while (counter < 200) {
	    double dq, q;

	    System.out.format("counter = %d\n",counter);
	    singlestep();

	    time += dt;
	    tinj += dt;
	    while (tinj > T) {
		switch(control){
		case 1:
		    dq = -0.02*(charge+1);
		    break;
		case 2:
		    dq = -0.038*(charge+1) + 0.3*(charge+1)*(charge+1);
		    break;
		default:
		    dq = 0.;
		}
		q = -1*T + dq;
		pl.addElement(new Particle(q));
		cl.addElement(new Double(dq));
		charge += q;
		tinj -= T;
	    }
	
	    // if (ic_out >= ic_dtout) {
	    // 	ic_out = 1;
	    // 	break;
	    // } else {
	    // 	ic_out++;
	    // }
	    counter = counter + 1;
	}
    }

    void move_x(double dt) {
	for (int i = 0; i < pl.size(); i++) {
	    ((Particle)pl.elementAt(i)).move_x(dt);
	    System.out.format("%d %f %f\n",i,((Particle)pl.elementAt(i)).x,((Particle)pl.elementAt(i)).v);
	}
	System.out.println("\n");
    }

    void move_v(double dt) {
	for (int i = 0; i < pl.size(); i++) {
	    ((Particle)pl.elementAt(i)).move_v(dt);

	}
    }

    void weight_rho() {
	for (int i = 0; i < mx; i++) rho[i] = 0;

	for (int pi = 0; pi < pl.size(); pi++) {
	    Particle pli = (Particle)pl.elementAt(pi);
	    int i = (int)(pli.x/dx);
	    rho[i] += pli.qc*((i+1)*dx - pli.x)/dx/dx;
	    rho[i+1] += pli.qc*(pli.x - i*dx)/dx/dx;

	}
	
    }

    void force() {
	for (int pi = 0; pi < pl.size(); pi++) {
	    Particle pli = (Particle)pl.elementAt(pi);
	    int i = (int)(pli.x/dx);
	    ((Particle)pl.elementAt(pi)).F 
		= - ((i+1)*dx - pli.x)/dx*E[i] - (pli.x - i*dx)/dx*E[i+1];
	}
    }

    void singlestep() {
	move_x(dt/2.);
	eliminate();
	weight_rho();
	poisson();
	force();
	move_v(dt);
	move_x(dt/2);
    }

    void poisson() {
	double f[] = new double [mx];
	double a[] = new double [mx];
	double b[] = new double [mx];
	double alpha2dx2 = alpha*alpha*dx*dx;
	double ddx2 = 1./2./dx;

	int i;
	for (i = 0; i < mx; i++) {
	    f[i] = - alpha2dx2*(1 + rho[i]);
	}
 
	// for (int j = 0; j < mx; j++)
	//     System.out.format("f(%d)=%f  %f  %f\n",j,f[j],alpha2dx2,rho[j]);

	// determination of series a and b
	a[0] = b[0] = 0.;
	for (i = 1; i < mx; i++) {
	    a[i] = 1./(2. - a[i-1]);
	    b[i] = a[i]*(b[i-1] - f[i]);
	}
 
	// back substitution
	phi[mx-1] = 0.;
	for (i = mx-2; i >= 0; i--)
	    phi[i] = a[i]*phi[i+1] + b[i];

	// calculation of E
	E[0] = - (-3.*phi[0] + 4.*phi[1] - phi[2])*ddx2;
	for (i = 1; i < mx-1; i++) {
	    E[i] = - (phi[i+1] - phi[i-1])*ddx2;
	}
	E[mx-1] = - (3.*phi[mx-1] - 4.*phi[mx-2] + phi[mx-3])*ddx2;


    }

}
