// <applet code=Pierce width=500 height=300> </applet>
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

// PIC-simulation of Pierce-diode

class Particle {
    double x;
    double v;
    double F;
    double qc;

    Particle() {
	x = 0;
	v = 1.0;
	F = 0;
	qc = 0;
    }

    Particle(double q) {
	x = 0;
	v = 0.99;
	F = 0;
	qc = q;
    }

    Particle(double q, double xx) {
	x = xx;
	v = 1.0;
	F = 0;
	qc = q;
    }

    void move_x(double dt) {
	x += dt*v;
    }

    void move_v(double dt) {
	v += dt*F;
    }

}

class Parameter {
    double dt,dx;
    public double dtout;
    public int mx;
    double T;
    double alpha;

    public Parameter() {
	dt = 0.01;
	dtout = 0.01;
	mx = 100;
	dx = 1./(mx-1);
	T = 0.005;
	alpha = 10.0;
    }
    public void setAlpha(double alpha){
	this.alpha = alpha;
    }
    
}

class PIC extends Parameter {
    double phi[];
    double E[];
    double rho[];
    Vector pl;
    Vector cl;
    double time,tinj;
    int ic_out;
    int ic_dtout;
    double charge;
    int control;
    boolean restart;

    public PIC() {
	ic_out = 1;
	ic_dtout = (int)(dtout/dt+0.5);
	restart = false;
	control = 0;
	init();
	
    }

    public void setControl(int control)
    {
	this.control = control;
    }
    
    public void init() {
	charge = 0;
	phi = new double [mx];
	E   = new double [mx];
	rho = new double [mx];
	pl = new Vector((int)(1.5/T));
	cl = new Vector();

	time = 0;
	for (double x = 1-T; x > -T/2; x -= T) {
	    pl.addElement(new Particle(-1*T,x));
	    charge += -1*T;
	}
    }

    void eliminate() {
	for (int pi = 0; pi < pl.size();) {
	    Particle pli = (Particle)pl.elementAt(pi);
	    if (pli.x < 0 || pli.x >= 1.){
		charge -= pli.qc;
      		pl.removeElementAt(pi);
	    }
	    else pi++;
	}
    }

    void integrate() {
	if(restart){
	    restart = false;
	    init();
	}
	
	while (true) {
	    double dq, q;

	    singlestep();

	    time += dt;
	    tinj += dt;
	    while (tinj > T) {
		switch(control){
		case 1:
		    dq = -0.02*(charge+1);
		    break;
		case 2:
		    dq = -0.038*(charge+1) + 0.3*(charge+1)*(charge+1);
		    break;
		default:
		    dq = 0.;
		}
		q = -1*T + dq;
		pl.addElement(new Particle(q));
		cl.addElement(new Double(dq));
		charge += q;
		tinj -= T;
	    }
	
	    if (ic_out >= ic_dtout) {
		ic_out = 1;
		break;
	    } else {
		ic_out++;
	    }
	}
    }

    void move_x(double dt) {
	for (int i = 0; i < pl.size(); i++) {
	    ((Particle)pl.elementAt(i)).move_x(dt);
	}
    }

    void move_v(double dt) {
	for (int i = 0; i < pl.size(); i++) {
	    ((Particle)pl.elementAt(i)).move_v(dt);
	}
    }

    void weight_rho() {
	for (int i = 0; i < mx; i++) rho[i] = 0;

	for (int pi = 0; pi < pl.size(); pi++) {
	    Particle pli = (Particle)pl.elementAt(pi);
	    int i = (int)(pli.x/dx);
	    rho[i] += pli.qc*((i+1)*dx - pli.x)/dx/dx;
	    rho[i+1] += pli.qc*(pli.x - i*dx)/dx/dx;
	}
    }

    void force() {
	for (int pi = 0; pi < pl.size(); pi++) {
	    Particle pli = (Particle)pl.elementAt(pi);
	    int i = (int)(pli.x/dx);
	    ((Particle)pl.elementAt(pi)).F 
		= - ((i+1)*dx - pli.x)/dx*E[i] - (pli.x - i*dx)/dx*E[i+1];
	}
    }

    void singlestep() {
	move_x(dt/2.);
	eliminate();
	weight_rho();
	poisson();
	force();
	move_v(dt);
	move_x(dt/2);
    }

    void poisson() {
	double f[] = new double [mx];
	double a[] = new double [mx];
	double b[] = new double [mx];
	double alpha2dx2 = alpha*alpha*dx*dx;
	double ddx2 = 1./2./dx;

	int i;
	for (i = 0; i < mx; i++) {
	    f[i] = - alpha2dx2*(1 + rho[i]);
	}
 
	// determination of series a and b
	a[0] = b[0] = 0.;
	for (i = 1; i < mx; i++) {
	    a[i] = 1./(2. - a[i-1]);
	    b[i] = a[i]*(b[i-1] - f[i]);
	}
 
	// back substitution
	phi[mx-1] = 0.;
	for (i = mx-2; i >= 0; i--)
	    phi[i] = a[i]*phi[i+1] + b[i];

	// calculation of E
	E[0] = - (-3.*phi[0] + 4.*phi[1] - phi[2])*ddx2;
	for (i = 1; i < mx-1; i++) {
	    E[i] = - (phi[i+1] - phi[i-1])*ddx2;
	}
	E[mx-1] = - (3.*phi[mx-1] - 4.*phi[mx-2] + phi[mx-3])*ddx2;
    }
}

// here comes the graphics

class pierceCanvas extends Canvas {
    public int x,y;
    public PIC pierce;
    double last_rho;

    Image backBuffer=null;
    Graphics backGC;
    Dimension backSize;

    public pierceCanvas(PIC pierce)
    {
	super();
	this.pierce = pierce;
	last_rho = 0;
	setVisible(true);
	
    }

    public synchronized void newBackBuffer() {
	Dimension size = getSize();
	backBuffer = createImage(getSize().width, getSize().height);
	backGC = backBuffer.getGraphics();
	backSize = size;
	backGC.setColor(Color.yellow);
	backGC.fillRect(0,0,getSize().width,getSize().height);
    }

    public synchronized void paint(Graphics g) {
	Dimension size = getSize();
	int width = (int)(size.width);
	int height = (int)(size.height);
	if (backBuffer != null) {
	    if (!backSize.equals(size)) {
		newBackBuffer();
	    }
	    backGC.setColor(Color.blue);
	    backGC.fillRect(0,0,width,height);
	    backGC.setColor(getForeground());
	    backGC.setColor(Color.yellow);
	    String text = "alpha = " + (double)Math.round(pierce.alpha*1000)/1000.0;
	    backGC.drawString(text,0,10);	    
	    for (int i = 0; i < pierce.pl.size(); i++) {
		Particle pli = (Particle)pierce.pl.elementAt(i);
		int x = (int)(pli.x*width);
		int y = (int)(height*(0.5 - pli.v/4.));
		backGC.fillOval(x,y,2,2);
	    }
	    int cl_maxsize = width/10;
	    while(pierce.cl.size() > cl_maxsize)
		pierce.cl.removeElementAt(0);
	    backGC.setColor(Color.red);
	    int y0 = (int)(height*0.8);
	    for(int i = cl_maxsize-1; i >= 0; i--){
		Double dq = (Double)pierce.cl.elementAt(i);
		int x = (cl_maxsize-i-1)*10;
		int y = (int)(height*0.8+dq.doubleValue()*1e4);
		backGC.drawLine(x,y0,x,y);
	    }
	    g.drawImage(backBuffer, 0, 0, this);
	} else {
	    newBackBuffer();
	}
	
     }
    
}


public class Pierce extends Applet
    implements Runnable, AdjustmentListener, ItemListener, MouseListener, ActionListener
{
    boolean painted = true;
    PIC pierce;

    Thread killme = null;
    int sizex;
    int sizey;
    Choice control_choice;
    Scrollbar alpha_slider;
    Button restart;
    pierceCanvas canvas;
    boolean threadSuspended = false; //added by kwalrath

    public synchronized void init() {
	sizex = getSize().width;
	sizey = getSize().height;
	pierce = new PIC();
	resize(sizex,sizey);

	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints c = new GridBagConstraints();
	setLayout(gridbag);
	c.weightx = 0.5;
	c.fill = GridBagConstraints.HORIZONTAL;
	restart = new Button("Restart");
	restart.addActionListener(this);
	gridbag.setConstraints(restart, c);
	add(restart);
	c.weightx = 1;
	control_choice = new Choice();
	control_choice.addItemListener(this);
	control_choice.addItem("keine Chaoskontrolle");
	control_choice.addItem("lineare Chaoskontrolle");
	control_choice.addItem("nichtlineare Chaoskontrolle");
	gridbag.setConstraints(control_choice, c);
	add(control_choice);
	c.weightx = 1;
	add(new Label("Pierce-Parameter"));
	alpha_slider = new Scrollbar(Scrollbar.HORIZONTAL, 255, 1, 0, 255);
	alpha_slider.addAdjustmentListener(this);
	c.gridwidth = GridBagConstraints.REMAINDER; //end row
	gridbag.setConstraints(alpha_slider, c);
	add(alpha_slider);
	canvas = new pierceCanvas(pierce);
	canvas.addMouseListener(this);
	c.fill = GridBagConstraints.BOTH;
	c.weighty = 1.0;
	gridbag.setConstraints(canvas, c);
	add(canvas);
    }

    public void start() {
	if(killme == null) {
	    killme = new Thread(this);
	    killme.start();
	}
    }

    public void stop() {
	killme = null;
    }

    public void run() {
	while (killme != null) {
	    try {
		pierce.integrate();
		
		Thread.sleep(2);
	    } 
	    catch (InterruptedException e){}
	    repaint();
	}
	killme = null;
    }
    public synchronized void update(Graphics g) {
	paintAll(g);
	canvas.paint(canvas.getGraphics());
   
    }
    public synchronized void actionPerformed(ActionEvent event)
    {
	canvas.newBackBuffer();
	pierce.restart = true;
    }
    
    public void itemStateChanged(ItemEvent evt) {
	if (control_choice.getSelectedItem().equals("keine Chaoskontrolle")) {
	    pierce.setControl(0);
	}
	else if (control_choice.getSelectedItem().equals("lineare Chaoskontrolle")){
	    pierce.setControl(1);
	}
	else if (control_choice.getSelectedItem().equals("nichtlineare Chaoskontrolle")){
	    pierce.setControl(2);
	}
    }
    
    public void adjustmentValueChanged(AdjustmentEvent evt) {
	double alpha = 3.0+alpha_slider.getValue()*7.0/255.0;
	pierce.setAlpha(alpha);
    }
    
    public void mouseClicked(MouseEvent evt) {
    }
    public void mousePressed(MouseEvent evt) {
	canvas.newBackBuffer();
    }
    public void mouseReleased(MouseEvent evt) { }
    public void mouseEntered(MouseEvent evt) { }
    public void mouseExited(MouseEvent evt) { }
}
