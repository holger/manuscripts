#include <string>
#include <sstream>

std::string intToString(int s1)
{
  std::ostringstream temp;
  temp << s1;
  return temp.str();
}

template<class T>
std::string toString(T s1)
{
  std::ostringstream temp;
  temp << s1;
  return temp.str();
}

class Position
{

public:
  Position(double xx, double yy, double zz) : x(xx), y(yy), z(zz) {}

  double x;
  double y;
  double z;
};

// cannot be member function of Particle! Why? operator<< can only be member
// function of class ostream because it modifies the stream object (std::ostream&).
// In Matrix a; a *= 2 the *= operator can only be of the class Matrix.
// However, we cannot change std::ostream!
std::ostream& operator<<(std::ostream &of, Position p) {
  of << "(" << p.x << "," << p.y << "," << p.z << ")";
  return of;
}
