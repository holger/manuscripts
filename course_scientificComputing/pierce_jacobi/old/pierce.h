#ifndef pierce_h
#define pierce_h

#include <list>

class Parameter {
public:
  double dt,dx;
  double dtout;
  int mx;
  double numberOfParticles;
  double particleDensity;
  double particleCharge;
  double alpha;
	      
  Parameter() {
    dt = 0.01;
    dtout = 0.01;
    mx = 100;
    dx = 1./(mx-1);
    numberOfParticles = 200;
    particleDensity = 1/numberOfParticles;
    particleCharge = 1/numberOfParticles;
    alpha = 10.0;
  }
  
  void setAlpha(double alpha){
    this->alpha = alpha;
  }
};

class PIC : public Parameter {
public: 
  double* phi;
  double* E;
  double* rho;
  std::list<Particle> particleList;
  //  Vector cl;
  double time,tinj;
  int ic_out;
  int ic_dtout;
  double charge;
  int control;
  bool restart;
		 
  PIC() {
    ic_out = 1;
    ic_dtout = (int)(dtout/dt+0.5);
    restart = false;
    control = 0;
    init();
  }

  void setControl(int control)
  {
    this->control = control;
  }
    
  void init() {
    charge = 0;
    phi = new double[mx];
    E   = new double[mx];
    rho = new double[mx];
    //    cl = new Vector();

    time = 0;

    double deltaXParticles = 1/numberOfParticles;
    for (int i = 0; i < numberOfParticles; i++) {
      particleList.push_back(Particle(particleCharge,i*deltaXParticles));
      charge += particleCharge;
    }
  }

  void eliminate() {
    for(std::list<Particle>::iterator particleIt = particleList.begin(); particleIt != particleList.end(); particleIt++) {
      if(particleIt->x < 0 || particleIt->x >= 1.) {
	charge -= particleIt->qc;
	particleList.erase(particleIt);
	--particleIt;
      }
    }
  }
  
  void integrate() {
    if(restart){
      restart = false;
      init();
    }
    
    while (true) {
      double dq, q;
      
      singlestep();

      time += dt;
      
      int particleFluxPerDt = dt*1*particleDensity;

      for(int i=0;i<particleFluxPerDt;i++) {
	switch(control){
	case 1:
	  dq = -0.02*(charge+1);
	  break;
	case 2:
	  dq = -0.038*(charge+1) + 0.3*(charge+1)*(charge+1);
	  break;
	default:
	  dq = 0.;
	}
	q = -1*particleCharge + dq;
	particleList.push_back(Particle(q));
	//	cl.addElement(new Double(dq));
	charge += q;
      }
      
      if (ic_out >= ic_dtout) {
	ic_out = 1;
	break;
      } else {
	ic_out++;
      }
    }
  }

  void move_x(double dt) {
    for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
      pIt->move_x(dt);
    }
  }

  void move_v(double dt) {
    for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
      pIt->move_v(dt);
    }
  }

  void weight_rho() {
    for(int i = 0; i < mx; i++) rho[i] = 0;
    
    for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
      int i = (int)(pIt->x/dx);
      rho[i] += pIt->qc*((i+1)*dx - pIt->x)/dx/dx;
      rho[i+1] += pIt->qc*(pIt->x - i*dx)/dx/dx;
    }
  }

    void force() {

      for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
	int i = (int)(pIt->x/dx);
	    pIt->F = - ((i+1)*dx - pIt->x)/dx*E[i] - (pIt->x - i*dx)/dx*E[i+1];
	}
    }

    void singlestep() {
	move_x(dt/2.);
	eliminate();
	weight_rho();
	poisson();
	force();
	move_v(dt);
	move_x(dt/2);
    }

    void poisson() {
	double* f = new double[mx];
	double* a = new double[mx];
	double* b = new double[mx];
	double alpha2dx2 = alpha*alpha*dx*dx;
	double ddx2 = 1./2./dx;

	int i;
	for (i = 0; i < mx; i++) {
	    f[i] = - alpha2dx2*(1 + rho[i]);
	}
 
	// determination of series a and b
	a[0] = b[0] = 0.;
	for (i = 1; i < mx; i++) {
	    a[i] = 1./(2. - a[i-1]);
	    b[i] = a[i]*(b[i-1] - f[i]);
	}
 
	// back substitution
	phi[mx-1] = 0.;
	for (i = mx-2; i >= 0; i--)
	    phi[i] = a[i]*phi[i+1] + b[i];

	// calculation of E
	E[0] = - (-3.*phi[0] + 4.*phi[1] - phi[2])*ddx2;
	for (i = 1; i < mx-1; i++) {
	    E[i] = - (phi[i+1] - phi[i-1])*ddx2;
	}
	E[mx-1] = - (3.*phi[mx-1] - 4.*phi[mx-2] + phi[mx-3])*ddx2;
    }
};

#endif
