#ifndef particle_h
#define particle_h

class Particle {
  
public:

  Particle() {
    x = 0;
    v = 1.0;
    F = 0;
    qc = 0;
  }

  Particle(double q) {
    x = 0;
    v = 1;
    F = 0;
    qc = q;
  }
  
  Particle(double q, double xx) {
    x = xx;
    v = 1.0;
    F = 0;
    qc = q;
  }

  double x;
  double v;
  double F;
  double qc;
  
  void move_x(double dt) {
    x += dt*v;
  }
  
  void move_v(double dt) {
    v += dt*F;
  }
};

#endif
