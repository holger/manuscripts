#ifndef pierce_h
#define pierce_h

#include "smallLib/toString.h"

#include "../../gnuplot-cpp/gnuplot_i.hpp"
//#include "../gnuplot-cpp/new/gnuplot-cpp-read-only/gnuplot_i.hpp"
#include <list>
#include <iostream>
#include <fstream>
#include <cmath>

class Parameter {
public:
  double dt,dx;
  int mx;
  double numberOfParticles;
  double particleDensity;
  double particleCharge;
  double alpha;
	      
  Parameter() {
    dt = 0.01;
    mx = 100;
    dx = 1./(mx-1);
    numberOfParticles = 200;
    particleDensity = numberOfParticles;
    particleCharge = -1/numberOfParticles;
    alpha = 10.0;
  }
  
  void setAlpha(double alpha){
    this->alpha = alpha;
  }
};

class Pic : public Parameter {
public: 
  double* phi;
  double* E;
  double* rho;
  std::list<Particle> particleList;
  //  Vector cl;
  double time;
  double particleOutFlux;
  int step;
  double charge;
  int control;
  bool restart;
  Gnuplot g1;
		 
  Pic() {
    std::cerr << "Pic::Pic()\n";

    restart = false;
    control = 0;
    init();
  }

  void setControl(int control)
  {
    this->control = control;
  }
    
  void init() {
    std::cerr << "Pic::init\n";

    charge = 0;
    phi = new double[mx];
    E   = new double[mx];
    rho = new double[mx];
    //    cl = new Vector();

    time = 0;
    step = 0;
    particleOutFlux = 0;

    double deltaXParticles = 1/numberOfParticles;
    for (int i = 0; i < numberOfParticles; i++) {
      particleList.push_back(Particle(particleCharge,i*deltaXParticles));
      charge += particleCharge;
    }
  }

  void eliminate() {
    for(std::list<Particle>::iterator particleIt = particleList.begin(); particleIt != particleList.end(); particleIt++) {
      if(particleIt->x < 0 || particleIt->x >= 1.) {
	charge -= particleIt->qc;
	particleList.erase(particleIt);
	--particleIt;
      }
    }
  }
  
  void integrate() {
    if(restart){
      restart = false;
      init();
    }
    
    int counter = 0;
    //    while (counter < 2000) {
    while (true) {
      double dq, q;

      write();

      singlestep();



      time += dt;
      ++step;

      particleOutFlux += 1*dt*particleDensity;
      //      std::cout << "particleOutFlux = " << particleOutFlux << std::endl;
      
      while(particleOutFlux > 1) {
	switch(control){
	case 1:
	  dq = -0.02*(charge+1);
	  break;
	case 2:
	  dq = -0.038*(charge+1) + 0.3*(charge+1)*(charge+1);
	  break;
	default:
	  dq = 0.;
	}
	q = 1*particleCharge + dq;
	particleList.push_back(Particle(q));
	//	cl.addElement(new Double(dq));
	charge += q;
	
	--particleOutFlux;
	
	//	std::cout << "charge = " << charge << std::endl;
      }
      ++counter;
    }
  }
    
  void write() {
    std::cerr << "Pic::write for step = " << step << std::endl;

    if(step % 2 == 0) {
      // std::string filename = toString("particles_") + toString(0) + toString(".txt");
      // std::ofstream out(filename.c_str());
      
      // int counter = 1;
      // for(std::list<Particle>::const_iterator pIt=particleList.begin(); pIt != particleList.end(); pIt++)
      // 	out << counter++ << "\t" << pIt->qc << "\t" << pIt->x << "\t" << pIt->v << std::endl;
      
      // out.close();
      
      // filename = toString("rho_") + toString(step) + toString(".txt");
      // out.open(filename.c_str());
      // for(int i=0;i<mx;i++)
      // 	out << i << "\t" << rho[i] << "\t" << E[i] << std::endl;

      // out.close();
    
    
    int particleNumber = particleList.size();
    std::vector<double> x(particleNumber);
    std::vector<double> y(particleNumber);
    int pCounter = 0;
    for(std::list<Particle>::const_iterator pIt=particleList.begin(); pIt != particleList.end(); pIt++) {
      x[pCounter] = pIt->x;
      y[pCounter] = pIt->v;
      ++pCounter;
    }
    
    g1.reset_plot();
    g1.set_style("points").set_xlabel("x").set_ylabel("v").set_xrange(0,1).set_yrange(-2.5,2.5).plot_xy(x,y,"particles");

    std::cout << std::endl << "Press ENTER to continue..." << std::endl;
    
    std::cin.clear();
    std::cin.ignore(std::cin.rdbuf()->in_avail());
    std::cin.get();
    }


  }
    

  void move_x(double dt) {
    for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
      pIt->move_x(dt);
    }
  }

  void move_v(double dt) {
    for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
      pIt->move_v(dt);
    }
  }

  void weight_rho() {
    for(int i = 0; i < mx; i++) rho[i] = 0;
    
    for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
      int i = int(pIt->x/dx);
      rho[i] += pIt->qc*((i+1)*dx - pIt->x)/dx/dx;
      rho[i+1] += pIt->qc*(pIt->x - i*dx)/dx/dx;
    }
  }

  void force() {
    //      std::cout << "Pic::force()\n";
      for(std::list<Particle>::iterator pIt = particleList.begin(); pIt != particleList.end(); pIt++) {
      int i = (int)(pIt->x/dx);
      pIt->F = - ((i+1)*dx - pIt->x)/dx*E[i] - (pIt->x - i*dx)/dx*E[i+1];
    }
    }
  
  void singlestep() {
    //      std::cout << "Pic::singlestep()\n";
      move_x(dt/2.);

      eliminate();
      weight_rho();
      poisson();
      write();
      force();
      move_v(dt);

      move_x(dt/2);
    }
  
  void poisson() {
      double* f = new double[mx];
      double* a = new double[mx];
      double* b = new double[mx];
      
      std::cout << "alpha = " << alpha << std::endl;
      double alpha2dx2 = alpha*alpha*dx*dx;
      for (int i = 0; i < mx; i++) {
	f[i] = - alpha2dx2*(1 + rho[i]);
	//	f[i] = 0;
      }

      double* phiNew = new double[mx];
      // phi[0] = 0;
      // phi[mx-1] = 1;

      phiNew[0] = 0;
      phiNew[mx-1] = 0;
      
      double deltaT = 2./(4/(dx*dx));;
      double difference = 1;
      
      while(difference > 1e-12) {
	for(int i=1;i<mx-1;i++) {
	  phiNew[i] = 0.5*(phi[i+1] + phi[i-1]) - f[i]/2.;
	  //	  std::cout << i << "\t" << f[i] << "\t" << phiNew[i] << "\t" << dx << std::endl;
	}
	difference = 0;
	for(int i=1;i<mx-1;i++) 
	  if(fabs(phi[i] - phiNew[i]) > difference)
	    difference = fabs(phi[i] - phiNew[i]);

	for(int i=1;i<mx-1;i++) 
	  phi[i] = phiNew[i];
	
	//	std::cout << "difference = " << difference << std::endl;
      }
      
      delete [] phi;
      // for(int i=0;i<mx;i++)
      // 	std::cout << i << "\t" << phi[i] << std::endl;
      


      double ddx2 = 1./2./dx;
      // calculation of E
      E[0] = - (-3.*phi[0] + 4.*phi[1] - phi[2])*ddx2;
      for (int i = 1; i < mx-1; i++) {
      E[i] = - (phi[i+1] - phi[i-1])*ddx2;
    }
      E[mx-1] = - (3.*phi[mx-1] - 4.*phi[mx-2] + phi[mx-3])*ddx2;
      // for(int i = 1; i < mx; i++) {
      // 	std::cerr << "f[" << i << "] = " << f[i] << "\t" << E[i] << "\t" << rho[i] << "\t" << phi[i] << std::endl;
      // }
  }
};


#endif
