#include <iostream>
#include <cmath>
#include <cstring>
#include "../vector/vector.h"
#include "poisson.h"

int main(int argc, char** argv)
{
  std::cout << "jacobi 1d test starts ...\n";

  int mx = 1000;
  Vector<double> phi(mx);
  Vector<double> rho(mx);
  
  for(int i=0;i<mx;i++) {
    phi[i] = 0.;
    rho[i] = 0.;
  }

  rho[mx-1] = 1;
  
  int counter;
  counter = poisson(phi,rho,0,1);
  
  for(int i=0;i<mx;i++)
    std::cerr << i << "\t" << phi[i] << std::endl;

  std::cout << "counter = " << counter << std::endl;
}
