#ifndef poisson_h
#define poisson_h

template<typename T, class Vector>
int poisson(Vector& phi, const Vector& rho, T valLeft, T valRight)
{
  // create temporary vector
  Vector phiNew(phi.size());

  // boundary conditions
  phiNew[phiNew.size()-1] = valRight;
  phiNew[0] = valLeft;
  
  int counter = 0;
  double difference = 1;
  while(difference > 1e-12) {
    // one jacobi iteration
    for(int i=1;i<phi.size()-1;i++) 
      phiNew[i] = 0.5*(phi[i+1] + phi[i-1]);// - rho[i]/2.;

    // compute absolute value of the difference phiNew - phi
    difference = 0;
    for(int i=0;i<phi.size();i++) 
      if(fabs(phi[i] - phiNew[i]) > difference)
	difference = fabs(phi[i] - phiNew[i]);
    
    phi = phiNew;
    counter++;
  }

  return counter;
}

#endif
