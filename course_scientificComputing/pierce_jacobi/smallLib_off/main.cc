#include "toString.h"

#include <iostream>
#include <cstdio>

int main(int argc, char** argv)
{
  // --------------------------------------------------------------
  
  // often one wants to write something like
  // std::string filename = "particle-" + number + ".txt";
  // However, + operation only for strings!

  std::cerr << "converting int to string\n";

  int number = 123;
  
  // c-way:
  char str[128];  
  sprintf(str,"%d",number);
  // Caution: 1) Length safety: size of str must be sufficiently large !!! Buffer overflow! (write behind str in memory)
  //          2) Type safecty: specifier must be correct !!! %s = pointer to character! (write somewhere in memory)
  std::cout << "number = " << str << std::endl;
  
  // c++-way:
  std::cout << "number = " << intToString(number) << std::endl;

  // with templates
  std::cout << "number = " << toString(number) << std::endl;

  Position position(1,2,3);
  std::cout << "position = " << position << std::endl;

  // write code for
  // std::string filename = toString(number,particle);
  // and
  // std::string filename = toString(number,particle,".txt");

  // --------------------------------------------------------------


}
