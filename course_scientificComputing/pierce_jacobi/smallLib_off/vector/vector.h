#ifndef vector_class_h
#define vector_class_h

#include <cmath>
#include <cstring>
#include <cassert>

template<typename T>
class Vector
{
public:
  Vector();
  Vector(int n);
  Vector(const Vector& vector);
  ~Vector();  

  int size();

  T operator[](int n) const;
  T& operator[](int n);
  Vector& operator=(const Vector& vector);
  template<typename TYPE>  Vector& operator=(TYPE value);
  
private:
  T* data_;
  int size_;
};

// definitions:

template<typename T>
Vector<T>::Vector()
{
  data_ = NULL;
  size = 0;
}

template<typename T>
Vector<T>::Vector(int n)
{
  size_ = n;
  data_ = new T[n];
}

template<typename T>
Vector<T>::Vector(const Vector& vector)
{
  size_ = vector.size_;
  data_ = new T[size_];

  this->operator=(vector);
}

template<typename T>
Vector<T>::~Vector()
{
  delete [] data_;
}

template<typename T>
inline int Vector<T>::size()
{
  return size_;
}

template<typename T>
inline T Vector<T>::operator[](int i) const
{
  //  assert(i < size_);
  return data_[i];
}

template<typename T>
inline T& Vector<T>::operator[](int i)
{
  //  assert(i < size_);
  return data_[i];
}

template<typename T>
Vector<T>& Vector<T>::operator=(const Vector<T>& vector)
{
  std::memcpy(data_,vector.data_,size_*sizeof(T));

  return *this;
}

template<typename T>
template<typename TYPE>  
Vector<T>& Vector<T>::operator=(TYPE value)
{
  // use something like memset?
  for(int i=0;i<size_;i++)
    data_[i] = value;
}

#endif
