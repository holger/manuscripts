\documentclass[a4paper,12pt]{letter}
\usepackage{german,amsmath,ifthen}
\usepackage{ucs} 
\usepackage{bm}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{float}
\usepackage{amssymb}
\usepackage[utf8x]{inputenc}

\pagestyle{empty}
\setlength{\textwidth}{16.5cm}
\setlength{\textheight}{26cm}
\setlength{\parindent}{0mm}

\setlength{\topmargin}{-1cm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}

\setlength{\marginparwidth}{0mm}
\setlength{\marginparpush}{0mm}
\setlength{\marginparsep}{0mm}
\setlength{\oddsidemargin}{0mm}

\begin{document}

Université de Nice - Sophia Antipolis \\
Faculté des Sciences \\[2cm]

\begin{center}
{{\bf \large Contrôle Mécanique II }\\ {\bf partie
    mécanique du solide }\\[0.5cm] 18 juin 2014} \\[1cm] \hrule {\bf
  \large Documents, calculatrices et portables interdits \\ Durée :
  Une heure}\\[0.7cm] \hrule
\end{center}

\vspace{1cm}

Un cerceau (cercle) de rayon $r$ roule sans glisser dans un cerceau
immobile de rayon $R$ (voir figure en dessous). La force
gravitationelle constante $\vec{g}$ agit sur le petit cerceau.

\begin{enumerate}
\item Propriétés du disque (8 points) :
  \begin{enumerate}
    \item Combien de degrées de liberté a ce système.
  \item Calculer la masse $M$ et le centre de masse $\vec G$ du
    cerceau. Trouver les éléments $\mathbb{I}^G_{zz}$,
    $\mathbb{I}^G_{xx}$ et $\mathbb{I}^G_{yy}$ du tenseur d'inertie
    $\mathbb{I}^G$ du cerceau par rapport à son centre de masse $\vec
    G$.
  \item Calculer la vitesse $\vec v_c$ du centre $\vec x_c$ du cerceau
    en fonction de $\dot \theta$ dans les coordonnées $x,y$.
  \item La condition que le petit cerceau ne glisse pas sur le grand
    cerceau donne une relation entre $\dot \theta$ et $\dot
    \phi$. Calculer la constante $C$ dans $\dot \phi=C\, \dot \phi)$.
  \end{enumerate}
\item L'énergie du disque (12 points) :
  \begin{enumerate}
  \item Ecrire l'énergie cinétique $T$ du cerceau en fonction de
    $\dot \theta$.
  \item Trouver l'énergie potentielle $U$ du cerceau en fonction de
    $\theta$. Assumer que cette énergie vaut $U=0$ tout en bas.
  \item Quelle est la vitesse de translation $\dot \vec x_c$ du
    cerceau tout en bas si il lacher à $\theta=\pi/2$ et vitesse zero
    ?
  \item Trouver l'équation du mouvement du système qui relie $\ddot
    \theta$ et $\theta$ en utilisant la conservation d'énergie totale.
  \item Trouver la fréquence avec laquelle le cerceau oscille si
    $\theta$ est petit.
  \end{enumerate}
\end{enumerate}

\newpage
\begin{wrapfigure}{r}{10cm}
  \includegraphics[scale=0.9]{figure2.pdf}
\end{wrapfigure}

\hrule
\begin{center}
{\bf \large Formulaire } 
\end{center}

\begin{itemize}
\item La masse : $M=\int \rho \,d^3r$
\item Le centre de masse : $\vec G=\int \rho \, \vec r\,d^3r$
\item Le tenseur d'inertie : $\mathbb{I}^G_{ij}=\int \rho \,
  [\parallel \vec{r} \parallel^2\delta_{ij}-r_i\,r_j] \,d^3r$
\item Composition des vitesses :
  $\vec{v}_B=\vec{v}_A+\vec{\omega}\wedge\overrightarrow{AB}$
\item L'énergie cinétique : $T=T_T+T_R=1/2\,M\,\parallel
  \vec{v}_G\parallel^2+ 1/2\,\vec{\omega} \cdot \mathbb{I}^G \cdot
  \vec{\omega}$
\end{itemize}

\hrule
\vspace*{2cm}
\end{document}
