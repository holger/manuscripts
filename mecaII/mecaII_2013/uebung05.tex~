\documentclass[a4paper,12pt]{letter}
\usepackage{german,amsmath,ifthen}
\usepackage{ucs} 
\usepackage{bm}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{float}
\usepackage[utf8x]{inputenc}

\pagestyle{empty}
\setlength{\textwidth}{16cm}
\setlength{\textheight}{26cm}
\setlength{\parindent}{0mm}

\setlength{\topmargin}{-1cm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}

\setlength{\marginparwidth}{0mm}
\setlength{\marginparpush}{0mm}
\setlength{\marginparsep}{0mm}
\setlength{\oddsidemargin}{0mm}

\begin{document}

{\bf TD 4} \hfill {\bf Mécanique II 2013}\\[3mm]

\begin{center}
{\bf \large Mécanique du solide}
\hrule
\end{center}

\vspace{1cm}

{\bf 1: Le moments cinétique des solides} \\[.3cm] Considerer une
plaque homogène, de masse $m$ ayant la forme d'un triangle rectangle
autour de $O'H$ (voir figure). La plaque tourne avec la vitesse
angulaire $\vec{\omega}=\omega \hat{z'}$.

\begin{enumerate}
  \item Calculer la résultante cinétique $P$ de la barre.
  \item Établir la matrice d'inertie $I$ du solide par rapport à un
    référentiel approprié du solide.
  \item Calculer le moment cinétique avec l'origin $O'$.
  \item Calculer l'énergie cinétique de la plaque.
\end{enumerate}
\begin{wrapfigure}{r}{5cm}
  \includegraphics[scale=0.5]{crochet.pdf}
\end{wrapfigure}
\end{document}
