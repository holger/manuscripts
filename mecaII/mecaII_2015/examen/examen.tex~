\documentclass[a4paper,12pt]{letter}
\usepackage{amsmath,ifthen}
\usepackage{ucs} 
\usepackage{bm}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{float}
\usepackage{amssymb}
\usepackage[utf8x]{inputenc}

\pagestyle{empty}
\setlength{\textwidth}{17.2cm}
\setlength{\textheight}{28cm}
\setlength{\parindent}{0mm}

\setlength{\topmargin}{-1.5cm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}

\setlength{\marginparwidth}{0mm}
\setlength{\marginparpush}{0mm}
\setlength{\marginparsep}{0mm}
\setlength{\oddsidemargin}{0mm}

\begin{document}

Université de Nice - Sophia Antipolis \\
Faculté des Sciences \\[1.5cm]

\begin{center}
{{\bf \large Contrôle Mécanique II }\\
 {\bf partie mécanique du solide }\\[0.5cm]
02 mai 2016} \\[1cm]
\hrule
{\bf \large Documents, calculatrices et portables interdits \\
  Durée totale : trois heures}\\[0.7cm]
\hrule
\end{center}

\vspace{0.7cm}

On étudie un rectangle homogène de densité surfasique $\rho$ avec les
longueurs des côtés $a$ et $b$ ($b > a$) dans le repère du
laboratoire $(0',\hat{x}',\hat{y}',\hat{z}')$. (voir figure en
dessous). Le rectangle peut tourner dans le plan
$(O',\hat{x}',\hat{y}')$ autour du point $O'$. Une accélération
constante gravitationnelle $-g\,\hat{y}'$ agit sur le corps.

\begin{enumerate}
\item Propriétés du rectangle (6 points) :
  \begin{enumerate}
    \item Calculer sa masse $M$. Calculer son centre de masse $G$ dans
      le repère du corps $(O',\hat{x},\hat{y})$ avec pour origine le
      point $O'$ (qui coïncide avec l'origine $O$) en fonction de $a$
      et $b$.
    \item Calculer l'élément $zz$ du tenseur d'inertie par rapport à
      l'origine $O$ ($\mathbb{I}^0_{zz}$) et par rapport au centre de
      masse $G$ ($\mathbb{I}^G_{zz}$) en fonction de $M$, $a$ et $b$.
    \item Calculer la vitesse du centre de masse $G$ en fonction de
      l'angle $\alpha$ dans le \emph{repère du corps}
      $(O',\hat{x},\hat{y})$. Montrer $\vec{v}_G = -\frac{\dot
        \alpha}{2}\,(b \hat{x} + a\hat{y})$. Recalculer la vitesse du
      centre de masse $G$ en fonction de l'angle $\alpha$ dans le
      \emph{repère du laboratoire} $(O',\hat{x}',\hat{y}')$. Montrer
      $\vec{v}_G = -\frac{\dot \alpha}{2}\,\left[(a\cos\alpha
        +b\sin\alpha)\,\hat{x}' + (a\sin\alpha
        -b\cos\alpha)\,\hat{y}'\right]$
  \end{enumerate}
\item L'énergie du rectangle et équations de mouvement (8 points) :
  \begin{enumerate}
  \item Calculer l'énergie cinétique $T$ du rectangle en fonction de
    $\dot \alpha$ par deux approches différentes : Par une rotation
    pure autour de $O'$ et par une translation de $G$ plus une
    rotation du rectangle autour de $G$.
  \item Trouver l'énergie potentielle $U$ du rectangle en fonction de
    $\alpha$, $a$ et $b$. Montrer que $U(\alpha) = \frac{M\, g}{2}
    [a(\cos\alpha -1) + b\sin\alpha]$ en supposant que l'énergie
    potentielle vaut zéro pour $\alpha = 0$.
  \item Montrer que $U$ est maximal pour $tan\alpha=b/a$. Qu'est ce
    que cette maximum implique pour la stabilité du rectangle ?
  \item Trouver l'équation du mouvement du système qui relie $\ddot
    \alpha$ et $\alpha$ en utilisant la conservation d'énergie
    totale.
  \end{enumerate}
\item Les forces sur le rectangle (6 points)
  \begin{enumerate} 
  \item Dessiner les forces qui agissent sur le
    rectangle. Exprimer-les dans le repère du laboratoire
    $(0',\hat{x}',\hat{y}',\hat{z}')$.
  \item Ecrire l'équation d'évolution pour la résultante cinétique
    $\vec{P}$ dans le repère du laboratoire
    $(0',\hat{x}',\hat{y}',\hat{z}')$.
  \item Ecrire l'équation d'évolution pour le moment cinétique
    $\vec{L}$ dans le repère du laboratoire
    $(0',\hat{x}',\hat{y}',\hat{z}')$.
  \end{enumerate}
\end{enumerate}

\newpage
\begin{wrapfigure}{r}{9cm}
  \includegraphics[scale=1.3]{rectangle.pdf}
\end{wrapfigure}

\hrule
\begin{center}
{\bf \large Formulaire } 
\end{center}

\begin{itemize}
\item La masse : $M=\int \rho \,d^3r$
\item Le centre de masse : $\vec{G}=1/M\,\int \rho \, \vec{r} \,d^3r$
\item Le tenseur d'inertie : $\mathbb{I}_{ij}=\int \rho\, [\parallel
  \vec{r} \parallel^2\delta_{ij}-r_i\,r_j] \,d^3r$
\item Composition des vitesses :
  $\vec{v}_B=\vec{v}_A+\vec{\omega}\wedge\overrightarrow{AB}$
\item L'énergie cinétique : $T=T_T+T_R=1/2\,M\,\parallel
  \vec{v}_G\parallel^2+ 1/2\,\vec{\omega} \cdot \mathbb{I}^G \cdot
  \vec{\omega}$
\item La résultante cinétique : $\vec{P} = M \vec{v}_G$
\item Le moment cinétique par rapport au centre de masse :
  $\vec{L}=\mathbb{I}^G \cdot \vec{\omega}$
\end{itemize}

\hrule
\vspace*{2cm}
\end{document}
