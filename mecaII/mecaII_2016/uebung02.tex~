\documentclass[a4paper,12pt]{letter}
\usepackage{amsmath}
\usepackage{ucs} 
\usepackage{bm}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{float}
\usepackage[utf8x]{inputenc}

\pagestyle{empty}
\setlength{\textwidth}{16cm}
\setlength{\textheight}{26cm}
\setlength{\parindent}{0mm}

\setlength{\topmargin}{-1cm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}

\setlength{\marginparwidth}{0mm}
\setlength{\marginparpush}{0mm}
\setlength{\marginparsep}{0mm}
\setlength{\oddsidemargin}{0mm}

\begin{document}

{\bf TD 2} \hfill {\bf Mécanique II 2016/2017}\\[3mm]

\begin{center}
{\bf \large Mécanique du solide}
\hrule
\end{center}

\vspace{1cm}

{\bf 1: Une meule}\\[-0.3cm]

Un disque de rayon $R$ est assujeti à rouler sur un plan horizontal
$x'O'y'$, de manière à ce que son axe $Ox$ rencontre l'axe vertical
$O'z'$ en un point fixe $H$ tel que $O'H=R$ et $PH=2R$.
\begin{wrapfigure}{r}{5cm}
  \includegraphics[scale=0.4]{disque.jpg}
\end{wrapfigure}
\begin{enumerate}
\item Déterminer le nombre de dégrée de liberté et trouver autant de
  parametres adaptés pour décrire la position du disque.
\item Déterminer le vecteur de rotation instantanée $\vec \omega$ de ce
  mouvement.
\item Ecrire la condition pour un roulement sans glissement.
\item Que devient cette condition si le plan $x'O'y'$ tourne
  lui-même autour de $O'z'$ avec une vitesse angulaire $\Omega$?
\end{enumerate}

{\bf 2: La masse et le centre de masse des corps} \\[.3cm] Calculer la
masse et la position du centre de masse des corps homogènes suivants:
\begin{enumerate}
\item Barre de langueur $a$
\item Demi disque plein de rayon $R$.
\item Sphère creuse de rayon $R$.
\item Cône plein de hauteur $h$ de rayon $R$.
\end{enumerate}

\end{document}
