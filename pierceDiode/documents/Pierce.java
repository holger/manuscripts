import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import java.awt.Dimension;
import java.util.*;
import java.io.*;

// PIC-simulation of Pierce-diode

class Particle {
  double x;
  double v;
  double F;
  double qc;

  Particle() {
    x = 0;
    v = 1;
    F = 0;
    qc = 0;
  }

  Particle(double q) {
    x = 0;
    v = 0.99;
    F = 0;
    qc = q;
  }

  Particle(double q, double xx) {
    x = xx;
    v = 1;
    F = 0;
    qc = q;
  }

  void move_x(double dt) {
    x += dt*v;
  }

  void move_v(double dt) {
    v += dt*F;
  }

}

class Parameter {
  double dt,dx;
  double dtout;
  int mx;
  double T;
  double alpha;

  public Parameter() {
    dt = 0.001;
    dtout = 0.01;
    mx = 100;
    dx = 1./(mx-1);
    T = 0.005;
    alpha = 10;
  }
}

class PIC extends Parameter {
  double phi[];
  double E[];
  double rho[];
  Vector pl;
  double time,tinj;
  int ic_out;
  int ic_dtout;

  public PIC() {
    phi = new double [mx];
    E   = new double [mx];
    rho = new double [mx];
    pl = new Vector((int)(1.5/T));

    init();
    ic_out = 1;
    ic_dtout = (int)(dtout/dt+0.5);
  }

  void init() {
    time = 0;
    for (double x = 1-T; x > -T/2; x -= T) {
      	pl.addElement(new Particle(-1*T,x));
    }
  }

  void eliminate() {
    for (int pi = 0; pi < pl.size();) {
      Particle pli = (Particle)pl.elementAt(pi);
      if (pli.x < 0 || pli.x >= 1.) pl.removeElementAt(pi);
      else pi++;
    }
  }

  void integrate() {
    while (true) {
      singlestep();
      time += dt;

      tinj += dt;
      if (tinj > T) {
	pl.addElement(new Particle(-1*T));
	tinj -= T;
	while (tinj > T) tinj -= T;
      }
	
      if (ic_out >= ic_dtout) {
	ic_out = 1;
	break;
      } else {
	ic_out++;
      }
    }
  }

  void move_x(double dt) {
    for (int i = 0; i < pl.size(); i++) {
      ((Particle)pl.elementAt(i)).move_x(dt);
    }
  }

  void move_v(double dt) {
    for (int i = 0; i < pl.size(); i++) {
      ((Particle)pl.elementAt(i)).move_v(dt);
    }
  }

  void weight_rho() {
    for (int i = 0; i < mx; i++) rho[i] = 0;

    for (int pi = 0; pi < pl.size(); pi++) {
      Particle pli = (Particle)pl.elementAt(pi);
      int i = (int)(pli.x/dx);
      rho[i] += pli.qc*((i+1)*dx - pli.x)/dx/dx;
      rho[i+1] += pli.qc*(pli.x - i*dx)/dx/dx;
    }
  }

  void force() {
    for (int pi = 0; pi < pl.size(); pi++) {
      Particle pli = (Particle)pl.elementAt(pi);
      int i = (int)(pli.x/dx);
      ((Particle)pl.elementAt(pi)).F 
	= - ((i+1)*dx - pli.x)/dx*E[i] - (pli.x - i*dx)/dx*E[i+1];
    }
  }

  void singlestep() {
    move_x(dt/2.);
    eliminate();
    weight_rho();
    poisson();
    force();
    move_v(dt);
    move_x(dt/2);
  }

  void poisson() {
    double f[] = new double [mx];
    double a[] = new double [mx];
    double b[] = new double [mx];
    double alpha2dx2 = alpha*alpha*dx*dx;
    double ddx2 = 1./2./dx;

    int i;
    for (i = 0; i < mx; i++) {
      f[i] = - alpha2dx2*(1 + rho[i]);
    }
 
    // determination of series a and b
    a[0] = b[0] = 0.;
    for (i = 1; i < mx; i++) {
      a[i] = 1./(2. - a[i-1]);
      b[i] = a[i]*(b[i-1] - f[i]);
    }
 
    // back substitution
    phi[mx-1] = 0.;
    for (i = mx-2; i >= 0; i--)
      phi[i] = a[i]*phi[i+1] + b[i];

    // calculation of E
    E[0] = - (-3.*phi[0] + 4.*phi[1] - phi[2])*ddx2;
    for (i = 1; i < mx-1; i++) {
      E[i] = - (phi[i+1] - phi[i-1])*ddx2;
    }
    E[mx-1] = - (3.*phi[mx-1] - 4.*phi[mx-2] + phi[mx-3])*ddx2;
  }
}

// here comes the graphics

public class Pierce extends Applet implements Runnable {

  Image backBuffer;
  Graphics backGC;
  Dimension backSize;
  boolean painted = true;

  PIC pierce;

  Thread killme = null;
  int sizex;
  int sizey;
 
  boolean threadSuspended = false; //added by kwalrath

  private synchronized void newBackBuffer() {
    backBuffer = createImage(size().width, size().height);
    backGC = backBuffer.getGraphics();
    backSize = size();
  }

    public void init() {
    sizex = 640;
    sizey = 480;
    pierce = new PIC();
    resize(sizex,sizey);

    newBackBuffer();
}

  public void start() {
    if(killme == null) {
      killme = new Thread(this);
      killme.start();
    }
  }

  public void stop() {
    killme = null;
  }

  public void run() {
    while (killme != null) {
      try {
	pierce.integrate();

	Thread.sleep(10);
      } 
      catch (InterruptedException e){}
      repaint();
    }
    killme = null;
  }

  public void update(Graphics g) {
    if (backBuffer == null)
      g.clearRect(0, 0, size().width, size().height);
    paint(g);
  }


  public void paint(Graphics g) {
    
    if (backBuffer != null) {
      if (!backSize.equals(size()))
	newBackBuffer();
//       backGC.setColor(getBackground());
      backGC.setColor(Color.blue);
      backGC.fillRect(0,0,size().width,size().height);
      backGC.setColor(getForeground());
      backGC.setColor(Color.yellow);

      for (int i = 0; i < pierce.pl.size(); i++) {
	Particle pli = (Particle)pierce.pl.elementAt(i);
	int x = (int)(pli.x*size().width);
	int y = (int)(size().height*(0.5 - pli.v/4.));
	backGC.fillOval(x,y,3,3);
      }
      g.drawImage(backBuffer, 0, 0, this);

    } else {
    }
  }
}
